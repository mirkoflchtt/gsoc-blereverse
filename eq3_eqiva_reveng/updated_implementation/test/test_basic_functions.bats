#!/usr/bin/env bats

setup() {
    . ./basic_functions.sh
}

@test "convert temperature in hexadecimal base" {
    run calculate_temp 18
    [ "$output" == "24" ]
    [ "$status" -eq 0 ]

    run calculate_temp 21.4
    [ "$output" == "2b" ]
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run calculate_temp
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: calculate_temp <temperature>" ]

    run calculate_temp 0
    [ "$status" -ne 0 ]

    run calculate_temp -21
    [ "$status" -ne 0 ]
}

@test "convert temperature+128 in hexadecimal base" {
    run calculate_temp_128 18
    [ "$output" == "A4" ]
    [ "$status" -eq 0 ]

    run calculate_temp_128 21.4
    [ "$output" == "AB" ]
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run calculate_temp_128
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: calculate_temp_128 <temperature>" ]

    run calculate_temp_128 0
    [ "$status" -ne 0 ]

    run calculate_temp_128 -21
    [ "$status" -ne 0 ]
}

@test "parsing of holiday data in notifications" {
    #24/02/2018 15:30
    run parse_holiday_params 18 12 1F 02
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run parse_holiday_params 18 12
    [ "$output" = "Usage: parse_holiday_params <byte7> <byte8> <byte9> <byte10>" ]
    [ "$status" -ne 0 ]

    #33/02/2018 15:30 fails
    run parse_holiday_params 21 12 1F 02
    [ "$status" -ne 0 ]

    #24/13/2018 15:00 fails
    run parse_holiday_params 21 12 1E 0D
    [ "$status" -ne 0 ]

    #24/02/2256 15:00 fails
    run parse_holiday_params 18 100 1E 02
    [ "$status" -ne 0 ]

    #24/02/2018 25:00 fails
    run parse_holiday_params 18 12 32 02
    [ "$status" -ne 0 ]
}

@test "MAC address validation" {
    run validate_mac E8:D1:1B:BC:19:5C
    [ "$output" -eq 0 ]
    [ "$status" -eq 0 ]

    run validate_mac E8:D1:1B:BC:19:
    [ "$output" -eq -1 ]
    [ "$status" -eq 0 ]

    run validate_mac E8:D1:1B:HH:19:5C
    [ "$output" -eq -1 ]
    [ "$status" -eq 0 ]

    #lack of address causes an error
    run validate_mac
    [ "$output" = "Usage: validate_mac <MAC_address>" ]
    [ "$status" -ne 0 ]
}

@test "conversion from days to numbers" {
    run get_day_number Wednesday
    [ "$output" == "4" ]
    [ "$status" -eq 0 ]

    run get_day_number sun
    [ "$output" == "1" ]
    [ "$status" -eq 0 ]

    #lack of day name causes an error
    run get_day_number
    [ "$output" = "Usage: get_day_number <day_name>" ]
    [ "$status" -ne 0 ]

    run get_day_number 2
    [ "$status" -ne 0 ]

    run get_day_number june
    [ "$status" -ne 0 ]

    run get_day_number feb02
    [ "$status" -ne 0 ]
}

@test "search valves by name" {
    run search_by_name study2 valves.txt
    [ "$output" == "9C:B6:D0:16:E2:3B" ]
    [ "$status" -eq 0 ]

    run search_by_name KITCHEN2 valves.txt
    [ "$output" == "3A:B2:66:3C:FF:4B" ]
    [ "$status" -eq 0 ]

    run search_by_name bedROOM valves.txt
    [ "$output" == "00:11:22:33:44:55" ]
    [ "$status" -eq 0 ]

    run search_by_name study1 valves.txt
    [ "$output" == "-1" ]
    [ "$status" -eq 0 ]

    run search_by_name garage valves.txt
    [ "$output" == "-1" ]
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run search_by_name valves.txt
    [ "$output" = "Usage: search_by_name <valve_name> <file_name>" ]
    [ "$status" -ne 0 ]
}

@test "notification parsing" {
    #status notification
    run parse_return_value 02 01 0C XX XX 28
    [ "$status" -eq 0 ]

    #status notification in holiday mode
    run parse_return_value 02 01 2A XX XX 24 0E 10 25 0C
    [ "$status" -eq 0 ]

    #profile set notification
    run parse_return_value 02 02 03
    [ "$status" -eq 0 ]

    #profile request notification
    run parse_return_value 21 02 22 24 2A 36 22 66 2A 8A 22 90 22 90 22 90
    [ "$status" -eq 0 ]

    #profile request with day out of range
    run parse_return_value 21 FF 22 24 2A 36 22 66 2A 8A 22 90 22 90 22 90
    [ "$status" -ne 0 ]

    #unknown notification
    run parse_return_value 02 05 XX XX
    [ "$status" -ne 0 ]

    #unknown notification
    run parse_return_value 01 XX
    [ "$status" -ne 0 ]
}

#note: at the end of this test Bluetooth is disabled
@test "bluetooth status checker" {
    rf_out=$( rfkill list )

    while read line
    do
        #find bluetooth adapters indices in rfkill list
        if [[ "$line" =~ [B|b]luetooth ]]; then
            index[${#index[@]}]=$( echo $line | cut -d ":" -f 1 )
        fi
    done <<< "$rf_out"

    #if no line contains "bluetooth" the test fails
    [ "${#index[@]}" -gt 0 ]

    #activate bluetooth on each adapter
    for adapter in "${index[@]}"; do
        rfkill unblock $adapter
    done

    sleep 2
    run check_bt_status
    [ "$status" -eq 0 ]

    #deactivate bluetooth on each adapter
    for adapter in "${index[@]}"; do
        rfkill block $adapter
    done

    run check_bt_status
    [ "$status" -ne 0 ]
}
