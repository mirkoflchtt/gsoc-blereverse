#!/bin/bash

#USAGE:
#	./set_all_temp.sh <temperature>
#	changes the temperature of ALL the valves listed in $VALVE_FILE

. ./valve_commands.sh

if [ -z $1 ]; then
	printf "Usage: ./set_all_temp.sh <temperature>\n"
	exit
fi

while read -r line || [[ -n "$line" ]]; do
	address=$(echo $line | cut -d "/" -f 2)
	set_temperature $address $1
done < "$VALVE_FILE"
