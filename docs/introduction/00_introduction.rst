Introduction
============

.. (atrent) metterei una ulteriore intro per identificare il contesto, i.e., spiegare che esistono in commercio "oggetti" IoT (basati su BLE) che vengono controllati mediante app o altro sw per PC, ma di cui non viene divulgato il protocollo applicativo...
.. .. [CHIUSO] (sergioalberti) inserita

For some years now the world of IoT (*Internet of Things*) has experienced a strong increase in the production of BLE
(*Bluetooth Low Energy*) devices. This type of "smart devices" changes the way we interact with the world, but is often
controlled through smartphone apps or software whose application protocol is not disclosed.

This guide deals with the activity related to the reverse-engineering of the protocol used
in the communication with a BLE device.

It tries to be as general as
possible, however it takes as a test/reference device the `Eqiva <http://www.eq-3.com/products/eqiva.html>`_
radiator valves produced by the EQ3 company and the related Android application `CalorBT <https://play.google.com/store/apps/details?id=de.eq3.ble.android>`_ .
Between 2015 and 2016, `Dr. Andrea Trentini <http://atrent.it/doku.php>`_ contacted the company more than once
in order to obtain documentation related to the protocol, but the company did not want to provide details. EQ3
also specified that there is no GNU/Linux software to interact with the valves.

.. (atrent) equiva o eqiva?
.. .. [CHIUSO] (sergioalberti) corretto

.. figure:: ../images/eqiva_valve.*
    :alt: EQ3 Eqiva valve on a radiator
    :align: center

    EQ3 Eqiva valve on a radiator

The objectives are:

* to reverse engineer the protocol used to communicate with the BLE device
* to show how to communicate with the device using the BlueZ stack

The result translates into the possibility to integrate these devices into free home automation
systems or other external projects. In the specific case of the radiator valves, this guide has
led to the creation of a series of shell script functions to manage every "device controlling" aspect.

.. include:: 01_what_valves_are.inc

.. include:: 02_setup_without_radiator.inc
