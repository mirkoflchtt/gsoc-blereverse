.. _bluez_stack:

-----------
Bluez Stack
-----------

`BlueZ <http://www.bluez.org/>`_ is the offical Linux Bluetooth protocol stack and is part of the Linux kernel
since version 2.4.6 was released [4]_. It provides the necessary modules to manage *both classic and low energy*
Bluetooth devices [5]_. Along with these modules, there is a series of command-line tools (*bluez-tools*) that interact
with the main core.

Debian provides all the necessary packages in its repositories. The installation can be performed through the
``apt`` package manager::

    $ sudo apt install bluez bluez-tools

Many tools are available. Below we describe the use of those needed to send a command to a *BLE* device and to
search for its MAC address.

.. _hcitool_section:

Hcitool
========

As specified in its manual [6]_, ``hcitool`` is used to configure Bluetooth connections and send some special
command to Bluetooth devices. It allows, among other things, to send
`HCI <https://en.wikipedia.org/wiki/List_of_Bluetooth_protocols#Host_Controller_Interface_(HCI)>`_ commands,
make connections and manage the authentication phases.

Furthermore, it is able to scan for both BLE and non-BLE devices. This allows us to identify the **MAC address**
of the device we want to work on. However, it's not possible to search both types of devices simultaneously:

* to start a classic scan, use the command::

    $ sudo hcitool scan

* to start a BLE scan, use the command::

    $ sudo hcitool lescan

The following image shows the result of a BLE scan. Each line contains the MAC address of the identified device
followed by its name (if readable).

.. _hcitool_lescan:
.. figure:: ../images/hcitool_lescan.*
    :alt: scan for BLE devices
    :align: center

    Scan for BLE devices

.. note::

    The ``scan`` and ``lescan`` commands ignore advertising packages received from devices they already know about.
    To prevent those packages from being ignored (in some cases it may be necessary) use the ``--duplicate`` parameter.

.. _hcidump_section:

Hcidump
=======

*Hcidump* is a tool that can read the contents of the
`HCI <https://en.wikipedia.org/wiki/List_of_Bluetooth_protocols#Host_Controller_Interface_(HCI)>`_ packets sent
and received by any Bluetooth device.

Despite the BlueZ stack integrates this tool (*since version 5*), it requires to be installed separately. As always,
in Debian this can be done using the ``apt`` package manager::

    $ sudo apt install bluez-hcidump

Among other things, we will use this software to **read the contents of the advertising packages** during the scan phase.
To do this:

1. start a *scan* through :ref:`hcitool <hcitool_section>`::

    $ sudo hcitool lescan --duplicate

2. while *hcitool* is running, start ``hcidump``::

    $ sudo hcidump --raw

The ``--raw`` parameter allows to obtain data in the "original" format. Other parameters (``--hex``, ``--ascii``) can
be used to get them in other formats. Refer to the manual [8]_ for these details.

.. _hcidump_screen:
.. figure:: ../images/hcidump_screen.png
    :alt: hcidump running while scanning
    :align: center

    Hcidump (*right*) running while scanning (*left*)

The :ref:`image above <hcidump_screen>` shows the execution of the previous commands. The terminal on the
right lists the intercepted HCI packets. They are all advertising packages, because the first bytes are ``04 3E``.
As indicated by the Bluetooth specifications, ``04`` stands for *HCI Event* while ``3E`` means *LE Advertising Report*.

An example of the use of ``hcitool`` and ``hcidump`` is given in the
:ref:`Laica PS7200L Scripts <laica_ps7200l_script>` section.

.. _gatttool_section:

Gatttool
========

*Gatttool* is designed for Bluetooth Low Energy. It therfore works exploiting the concept of
:ref:`GATT <gatt_section>` and its ATT protocol, which is adopted only by BLE devices. Basically, it allows
you to connect to a device, discover its characteristics, write/read attributes and receive notifications.
Optionally, it can also be used in *interactive mode* through a `CLI <https://en.wikipedia.org/wiki/Command-line_interface>`_.

Possible uses are described in the manual [7]_ and through the command ``gatttool --help``. Below we show how to
write a value on a characteristic, which is what we want to do in order to send commands to BLE devices.

To this end, you need:

    * the **MAC address** of the target device
    * the :ref:`Handle <char_handle>` that identifies the characteristic.

Given this data, the following command sends the value represented by ``<value>`` to the device::

    $ gatttool -b <mac_address> --char-write-req -a <handle> -n <value> --listen

.. note::

    The ``--listen`` parameter requires ``gatttool`` to wait for a notification. However, this puts the process
    in a waiting state until it is *manually* terminated.

The :ref:`following image <gatttool_write>` shows the output obtained from the command execution. As you can see, the characteristic has
been written correctly and information about the received notification is shown.

.. _gatttool_write:
.. figure:: ../images/gatttool.*
    :alt: gatttool characteristic write
    :align: center

    Gatttool Characteristic Write

-----------------------------

.. [4] `BlueZ - Common Questions <http://www.bluez.org/faq/common/>`_
.. [5] `Ubuntu Bluez Documentation <https://docs.ubuntu.com/core/en/stacks/bluetooth/bluez/docs/>`_
.. [6] `hcitool manpage <https://linux.die.net/man/1/hcitool>`_
.. [7] `gatttool manpage <http://manpages.org/gatttool>`_
.. [8] `hcidump manpage <https://linux.die.net/man/8/hcidump>`_
