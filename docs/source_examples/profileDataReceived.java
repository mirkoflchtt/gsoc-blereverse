public void profileDataReceived(DayOfWeek dayOfWeek, byte[] profileData) {
    int i;
    Log.d(getClass().getSimpleName(), "Received Day: " + dayOfWeek.name() + " data-length: " + profileData.length);
    this.receivedDays.add(dayOfWeek);
    ProfileDay receivedProfileDay = new ProfileDay();
    receivedProfileDay.setDayOfWeek(dayOfWeek);
    List<ProfileDataPair> dataPairs = new ArrayList();

    for (i = 0; i < profileData.length; i += 2) {
        int time = (profileData[i + 1] & 255) * 10;
        dataPairs.add(new ProfileDataPair(((double) profileData[i]) / 2.0d, time));
        if (time == 1440) {
            break;
        }
    }

    double baseTemperature = getBaseTemperature(dataPairs);
    receivedProfileDay.setBaseTemperature(baseTemperature);
    List<Period> periods = new ArrayList();

    for (i = 0; i < dataPairs.size(); i++) {
        ProfileDataPair pair = (ProfileDataPair) dataPairs.get(i);
        if (pair.temperature != baseTemperature) {
            Period currentPeriod = new Period();
            if (i > 0) {
                currentPeriod.setStarttimeAsMinutesOfDay(((ProfileDataPair) dataPairs.get(i - 1)).time);
            } else {
                currentPeriod.setStarttimeAsMinutesOfDay(0);
            }
            currentPeriod.setEndtimeAsMinuteOfDay(pair.time);
            currentPeriod.setTemperature(pair.temperature);
            if (periods.size() < 3) {
                periods.add(currentPeriod);
            } else {
                Log.d(getClass().getSimpleName(), "The profile has to much entries, cut them off time: " + pair.time + " temperature: " + pair.temperature);
            }
        }
    }

    receivedProfileDay.setHeatingPeriods(periods);
    this.profile.getProfileDays().put(dayOfWeek, receivedProfileDay);
    Log.d(getClass().getSimpleName(), "Received Day: " + dayOfWeek.name() + " base temperature: " + baseTemperature + " Periods: " + periods.size());

    if (this.receivedDays.size() == DayOfWeek.values().length) {
        stop();
        this.listener.profileReceived(this.profile);
    }

    if (dayOfWeek.equals(this.lastRequestedDay)) {
        DayOfWeek[] weekDays = DayOfWeek.values();
        boolean sendNext = false;
        for (i = 0; i < weekDays.length; i++) {
            if (sendNext) {
                startRequest(weekDays[i]);
                return;
            }
            if (weekDays[i].equals(this.lastRequestedDay)) {
                sendNext = true;
            }
        }
    }
}
