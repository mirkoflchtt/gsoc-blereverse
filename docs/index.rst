Reverse Engineering BLE Devices
===============================

The following documentation is intended as a guide to reverse engineering of BLE (*Bluetooth Low Energy*)
devices. The idea is to provide information about BLE, how to identify the protocol used by the
devices and how to create shell scripts to communicate with them.

To do this, the guide is based on examples applied to devices currently on the market. As explained in the
:ref:`Contributions <contributions_sec>` section, this document would like to be an evolving project,
in which to gather information on reverse engineering techniques and to make available works already done in
this area.

.. (atrent) mettere un'esortazione a contributi anche in termini linguistici?
.. .. [CHIUSO] (sergioalberti) inserita a fondo pagina

Notes (GSoC 2018)
-----------------

This guide comes from a project of `GSoC 2018 <https://wiki.debian.org/SummerOfCode2018/Projects/RadiatorThermovalveReverseEngineering>`_
and takes as a starting point a work done on radiator valves. These systems have become increasingly important in recent years,
especially in some countries where they have been made mandatory by law. This led to the production of various models programmable
using a smartphone application coupled with the BLE protocol. At the moment all the products on the market use proprietary communication
protocols to exchange essential data with the application, making it difficult to integrate this devices into external open-source projects.
For this reason the University of Milan has successfully reverse-engineered a protocol and
`released <http://sl-lab.it/dokuwiki/doku.php/tesi:reveng-termovalvole>`_ the necessary code to use it with a GPL license.
An English translation of the code can be found
`here <https://gitlab.com/sergioalberti/gsoc-blereverse/tree/master/eq3_eqiva_reveng/reference_implementation>`_.

The project aims to use what has already been produced to:

* write a reverse-engineering guide for BLE devices as general as possible
* design a mechanical device to test the valves without a radiator
* port the library to a more modern language in an attempt to integrate it into projects such as `openhab <http://www.openhab.org/>`_ or `home-assistant <https://www.home-assistant.io/>`_ and create a Debian package

:doc:`Here <deliverables_link>` **is available a detailed description of the deliverables and the time schedule and** :doc:`here <diary_link>` **is a brief weekly report.**

Contents
--------

.. toctree::
    :maxdepth: 2
    :numbered:

    Introduction <introduction/00_introduction.rst>
    Reverse Engineering The Protocol <protocol_reveng/00_protocol_reveng>
    Protocol Description <protocol_description/00_protocol_description>
    Script Creation <script_creation/00_script_creation>
    Contributions <contributions/00_contributions>
