## Reverse Engineering BLE Devices

Radiator valves have become increasingly important in recent years, especially in some countries where they have been made mandatory by law. This led to the production of various models programmable using a smartphone application coupled with the BLE (Bluetooth Low Energy) protocol. At the moment all the products on the market use proprietary communication protocols to exchange essential data with the application, making it difficult to integrate this devices into external open-source projects. For this reason the University of Milan has successfully reverse-engineered a protocol and [released](http://sl-lab.it/dokuwiki/doku.php/tesi:reveng-termovalvole) the necessary code to use it with a GPL license.

The project aims to use what has already been produced to:
- write a reverse-engineering guide for BLE devices as general as possible
- design a mechanical device to test the valves without a radiator
- port the library to a more modern language in an attempt to integrate it into projects such as [openhab](http://www.openhab.org/) or [home-assistant](https://www.home-assistant.io/) and create a Debian package

**A compiled and updated version of the guide is available [here](http://reverse-engineering-ble-devices.readthedocs.io/en/latest/)**

____________________________

This work is the result of a project of the Google Summer of Code 2018, within the Debian "organization".   

References:
* [Project on Debian wiki](https://wiki.debian.org/SummerOfCode2018/Projects/RadiatorThermovalveReverseEngineering)
* [Student page on Debian wiki](https://wiki.debian.org/SergioAlberti)
* [Weekly report diary](https://gitlab.com/sergioalberti/gsoc-blereverse/blob/master/GSoC-2018/journal.rst)
