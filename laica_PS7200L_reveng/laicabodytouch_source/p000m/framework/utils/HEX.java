package p000m.framework.utils;

import android.support.v4.view.MotionEventCompat;

public class HEX {
    private static final char[] DIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String toHex(byte[] data) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            buffer.append(String.format("%02x", new Object[]{Byte.valueOf(data[i])}));
        }
        return buffer.toString();
    }

    public static byte[] toByte(String hexData) {
        byte[] bArr = null;
        if (hexData != null) {
            int len = hexData.length();
            if (len % 2 != 1) {
                int dataLength = len / 2;
                bArr = new byte[dataLength];
                for (int i = 0; i < dataLength; i++) {
                    bArr[i] = (byte) Integer.parseInt(hexData.substring(i * 2, (i * 2) + 2), 16);
                }
            }
        }
        return bArr;
    }

    public static String encodeHexString(byte[] data) {
        return new String(HEX.encodeHex(data));
    }

    public static byte[] decodeHexString(String hexData) {
        return HEX.decodeHex(hexData.toCharArray());
    }

    public static byte[] decodeHex(char[] data) {
        int len = data.length;
        if ((len & 1) != 0) {
            throw new RuntimeException("Odd number of characters.");
        }
        byte[] out = new byte[(len >> 1)];
        int i = 0;
        int j = 0;
        while (j < len) {
            j++;
            j++;
            out[i] = (byte) (((HEX.toDigit(data[j], j) << 4) | HEX.toDigit(data[j], j)) & MotionEventCompat.ACTION_MASK);
            i++;
        }
        return out;
    }

    protected static int toDigit(char ch, int index) {
        int digit = Character.digit(ch, 16);
        if (digit != -1) {
            return digit;
        }
        throw new RuntimeException("Illegal hexadecimal charcter " + ch + " at index " + index);
    }

    public static char[] encodeHex(byte[] data) {
        int l = data.length;
        char[] out = new char[(l << 1)];
        int j = 0;
        for (int i = 0; i < l; i++) {
            int i2 = j + 1;
            out[j] = DIGITS[(data[i] & 240) >>> 4];
            j = i2 + 1;
            out[i2] = DIGITS[data[i] & 15];
        }
        return out;
    }

    public byte[] decode(byte[] array) {
        return HEX.decodeHex(new String(array).toCharArray());
    }

    public byte[] decode(Object object) {
        try {
            return HEX.decodeHex(object instanceof String ? ((String) object).toCharArray() : (char[]) object);
        } catch (ClassCastException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public byte[] encode(byte[] array) {
        return new String(HEX.encodeHex(array)).getBytes();
    }

    public char[] encode(Object object) {
        try {
            return HEX.encodeHex(object instanceof String ? ((String) object).getBytes() : (byte[]) object);
        } catch (ClassCastException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
