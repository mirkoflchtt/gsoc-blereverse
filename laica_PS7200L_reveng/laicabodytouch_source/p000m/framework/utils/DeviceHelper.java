package p000m.framework.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.v4.os.EnvironmentCompat;
import android.support.v4.widget.ViewDragHelper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.whb.loease.bodytouch.C0181R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class DeviceHelper {
    private static DeviceHelper deviceHelper;
    private Context context;

    public DeviceHelper(Context context) {
        this.context = context.getApplicationContext();
    }

    public static DeviceHelper getInstance(Context c) {
        if (deviceHelper == null) {
            deviceHelper = new DeviceHelper(c);
        }
        return deviceHelper;
    }

    public boolean isRooted() {
        return false;
    }

    public String getMacAddress() {
        WifiManager wifi = (WifiManager) this.context.getSystemService("wifi");
        if (wifi == null) {
            return null;
        }
        WifiInfo info = wifi.getConnectionInfo();
        if (info == null) {
            return null;
        }
        String mac = info.getMacAddress();
        if (mac == null) {
            mac = null;
        }
        return mac;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getFactory() {
        return Build.MANUFACTURER;
    }

    public String getDeviceId() {
        TelephonyManager phone = (TelephonyManager) this.context.getSystemService("phone");
        if (phone == null) {
            return null;
        }
        String deviceId = phone.getDeviceId();
        String backId = "";
        if (deviceId != null) {
            backId = new String(deviceId).replace(WeightUnitHelper.Kg, "");
        }
        if ((deviceId != null && backId.length() > 0) || VERSION.SDK_INT < 9) {
            return deviceId;
        }
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            return (String) c.getMethod("get", new Class[]{String.class, String.class}).invoke(c, new Object[]{"ro.serialno", EnvironmentCompat.MEDIA_UNKNOWN});
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    public String getDeviceData() {
        return Base64AES(getModel() + "|" + getOSVersion() + "|" + getFactory() + "|" + getCarrier() + "|" + getScreenSize(), getDeviceKey().substring(0, 16));
    }

    public String Base64AES(String msg, String key) {
        String result = null;
        try {
            result = Base64.encodeToString(Data.AES128Encode(key, msg), 0);
            if (result.contains("\n")) {
                result = result.replace("\n", "");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getOSVersion() {
        return String.valueOf(VERSION.SDK_INT);
    }

    public String getScreenSize() {
        DisplayMetrics dm2 = this.context.getResources().getDisplayMetrics();
        if (this.context.getResources().getConfiguration().orientation == 1) {
            return dm2.widthPixels + "x" + dm2.heightPixels;
        }
        return dm2.heightPixels + "x" + dm2.widthPixels;
    }

    public String getCarrier() {
        String operator = ((TelephonyManager) this.context.getSystemService("phone")).getSimOperator();
        if (TextUtils.isEmpty(operator)) {
            return "-1";
        }
        return operator;
    }

    public String getNetworkType() {
        ConnectivityManager conn = (ConnectivityManager) this.context.getSystemService("connectivity");
        if (conn == null) {
            return null;
        }
        NetworkInfo network = conn.getActiveNetworkInfo();
        if (network == null || !network.isAvailable()) {
            return null;
        }
        int type = network.getType();
        if (1 == type) {
            return "wifi";
        }
        if (type != 0) {
            return null;
        }
        String proxy = Proxy.getDefaultHost();
        String wap = "";
        if (proxy != null && proxy.length() > 0) {
            wap = " wap";
        }
        return new StringBuilder(String.valueOf(isFastMobileNetwork() ? "3G" : "2G")).append(wap).toString();
    }

    public int getPlatformCode() {
        return 1;
    }

    private boolean isFastMobileNetwork() {
        TelephonyManager phone = (TelephonyManager) this.context.getSystemService("phone");
        if (phone == null) {
            return false;
        }
        switch (phone.getNetworkType()) {
            case 0:
            case 1:
            case 2:
            case 4:
            case 7:
            case C0181R.styleable.SwipeListView_swipeDrawableUnchecked /*11*/:
                return false;
            case 3:
                return true;
            case 5:
                return true;
            case 6:
                return true;
            case 8:
                return true;
            case 9:
                return true;
            case 10:
                return true;
            case 12:
                return true;
            case 13:
                return true;
            case 14:
                return true;
            case ViewDragHelper.EDGE_ALL /*15*/:
                return true;
            default:
                return false;
        }
    }

    public JSONArray getRunningApp() {
        JSONArray appNmes = new JSONArray();
        ActivityManager am = (ActivityManager) this.context.getSystemService("activity");
        if (am != null) {
            List<RunningAppProcessInfo> apps = am.getRunningAppProcesses();
            if (apps != null) {
                for (RunningAppProcessInfo app : apps) {
                    appNmes.put(app.processName);
                }
            }
        }
        return appNmes;
    }

    public String getRunningAppStr() throws JSONException {
        JSONArray apps = getRunningApp();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < apps.length(); i++) {
            if (i > 0) {
                sb.append(',');
            }
            sb.append(String.valueOf(apps.get(i)));
        }
        return sb.toString();
    }

    public String getDeviceKey() {
        try {
            String mac = getMacAddress();
            String udid = getDeviceId();
            return Data.byteToHex(Data.SHA1(new StringBuilder(String.valueOf(mac)).append(":").append(udid).append(":").append(getModel()).toString()));
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    public String getPackageName() {
        return this.context.getPackageName();
    }

    public String getAppName() {
        String appName = this.context.getApplicationInfo().name;
        if (appName != null) {
            return appName;
        }
        int appLbl = this.context.getApplicationInfo().labelRes;
        if (appLbl > 0) {
            appName = this.context.getString(appLbl);
        }
        return appName;
    }

    public int getAppVersion() {
        int i = 0;
        try {
            return this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode;
        } catch (Throwable t) {
            t.printStackTrace();
            return i;
        }
    }

    public String getAppVersionName() {
        try {
            return this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName;
        } catch (Throwable t) {
            t.printStackTrace();
            return "1.0";
        }
    }

    public ArrayList<HashMap<String, String>> getInstalledApp(boolean includeSystemApp) {
        ArrayList<HashMap<String, String>> apps = new ArrayList();
        try {
            PackageManager pm = this.context.getPackageManager();
            for (PackageInfo pi : pm.getInstalledPackages(0)) {
                if (includeSystemApp || !isSystemApp(pi)) {
                    HashMap<String, String> app = new HashMap();
                    app.put("pkg", pi.packageName);
                    app.put("name", pi.applicationInfo.loadLabel(pm).toString());
                    app.put("version", pi.versionName);
                    apps.add(app);
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return apps;
    }

    private boolean isSystemApp(PackageInfo pi) {
        boolean isSysApp;
        if ((pi.applicationInfo.flags & 1) == 1) {
            isSysApp = true;
        } else {
            isSysApp = false;
        }
        boolean isSysUpd;
        if ((pi.applicationInfo.flags & 128) == 1) {
            isSysUpd = true;
        } else {
            isSysUpd = false;
        }
        if (isSysApp || isSysUpd) {
            return true;
        }
        return false;
    }

    public String getNetworkOperator() {
        return ((TelephonyManager) this.context.getSystemService("phone")).getNetworkOperator();
    }

    public String getTopTaskPackageName() {
        try {
            return ((RunningTaskInfo) ((ActivityManager) this.context.getSystemService("activity")).getRunningTasks(1).get(0)).topActivity.getPackageName();
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    public boolean getSdcardState() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public String getSdcardPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }
}
