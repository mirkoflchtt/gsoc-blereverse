package p000m.framework.ui.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class PullToRefreshView extends RelativeLayout {
    private static final long MIN_REF_TIME = 1000;
    private PullToRefreshAdatper adapter;
    private View bodyView;
    private float downY;
    private int headerHeight;
    private View headerView;
    private boolean pullingLock;
    private long refreshTime;
    private boolean requesting;
    private Runnable stopAct;
    private int top;

    class C01911 implements Runnable {
        C01911() {
        }

        public void run() {
            PullToRefreshView.this.reversePulling();
            PullToRefreshView.this.stopRequest();
        }
    }

    public PullToRefreshView(Context context) {
        super(context);
        init();
    }

    public PullToRefreshView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PullToRefreshView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.stopAct = new C01911();
    }

    public void setAdapter(PullToRefreshAdatper adapter) {
        this.adapter = adapter;
        removeAllViews();
        this.bodyView = (View) adapter.getBodyView();
        LayoutParams lpBody = new LayoutParams(-2, -2);
        lpBody.addRule(9, -1);
        lpBody.addRule(11, -1);
        lpBody.addRule(10, -1);
        addView(this.bodyView, lpBody);
        this.headerView = adapter.getHeaderView();
        this.headerView.measure(0, 0);
        this.headerHeight = this.headerView.getMeasuredHeight();
        LayoutParams lpHead = new LayoutParams(-2, this.headerHeight);
        lpHead.addRule(9, -1);
        lpHead.addRule(11, -1);
        lpHead.addRule(10, -1);
        lpHead.topMargin = -this.headerHeight;
        addView(this.headerView, lpHead);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.downY = ev.getY();
                break;
            case 1:
            case 3:
                if (!this.requesting) {
                    if (this.top <= this.headerHeight) {
                        if (this.top != 0) {
                            reversePulling();
                            if (this.adapter != null) {
                                this.adapter.onPullDown(0);
                                break;
                            }
                        }
                    }
                    this.top = this.headerHeight;
                    scrollTo(0, -this.top);
                    if (this.adapter != null) {
                        this.adapter.onPullDown(100);
                    }
                    performRequest();
                    ev = getCancelEvent(ev);
                    break;
                }
                this.top = this.headerHeight;
                scrollTo(0, -this.top);
                break;
                break;
            case 2:
                float curY = ev.getY();
                if (this.requesting || canPull()) {
                    this.top = (int) (((float) this.top) + ((curY - this.downY) / 2.0f));
                    if (this.top > 0) {
                        scrollTo(0, -this.top);
                        if (!(this.requesting || this.adapter == null)) {
                            this.adapter.onPullDown((this.top * 100) / this.headerHeight);
                        }
                        ev = getCancelEvent(ev);
                    } else {
                        this.top = 0;
                        scrollTo(0, 0);
                    }
                }
                this.downY = curY;
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    private MotionEvent getCancelEvent(MotionEvent ev) {
        return MotionEvent.obtain(ev.getDownTime(), ev.getEventTime(), 3, ev.getX(), ev.getY(), ev.getMetaState());
    }

    private void performRequest() {
        this.refreshTime = System.currentTimeMillis();
        this.requesting = true;
        if (this.adapter != null) {
            this.adapter.onRequest();
        }
    }

    private void stopRequest() {
        this.requesting = false;
    }

    public void performPulling(boolean request) {
        this.top = this.headerHeight;
        scrollTo(0, -this.top);
        if (request) {
            performRequest();
        }
    }

    private void reversePulling() {
        this.top = 0;
        scrollTo(0, 0);
        if (this.adapter != null) {
            this.adapter.onReversed();
        }
    }

    public void stopPulling() {
        long delta = System.currentTimeMillis() - this.refreshTime;
        if (delta < MIN_REF_TIME) {
            postDelayed(this.stopAct, MIN_REF_TIME - delta);
        } else {
            post(this.stopAct);
        }
    }

    public void lockPulling() {
        this.pullingLock = true;
    }

    public void releaseLock() {
        this.pullingLock = false;
    }

    private boolean canPull() {
        return !this.pullingLock && this.adapter.isPullReady();
    }
}
