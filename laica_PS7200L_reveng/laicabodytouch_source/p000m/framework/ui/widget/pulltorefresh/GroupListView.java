package p000m.framework.ui.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GroupListView extends ListView {

    public static abstract class GroupListAdapter {
        private BaseAdapter adapter;

        public abstract Object getChild(int i, int i2);

        public abstract View getChildView(int i, int i2, View view, ViewGroup viewGroup);

        public abstract int getChildrenCount(int i);

        public abstract int getGroupCount();

        public abstract String getGroupTitle(int i);

        public abstract View getGroupTitleView(int i, View view, ViewGroup viewGroup);

        private void setInnerAdapter(BaseAdapter adapter) {
            this.adapter = adapter;
        }

        public void notifyDataSetChanged() {
            this.adapter.notifyDataSetChanged();
        }
    }

    private static class ItemHolder {
        public View child;
        public LinearLayout llItem;
        public View title;

        private ItemHolder() {
        }
    }

    public GroupListView(Context context) {
        super(context);
    }

    public GroupListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GroupListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setAdapter(final GroupListAdapter adapter) {
        BaseAdapter innerAdapter = new BaseAdapter() {
            public View getView(int position, View convertView, ViewGroup parent) {
                int[] indexPair = getItemIndex(position);
                int groupIndex = indexPair[0];
                int itemIndex = indexPair[1];
                ItemHolder holder;
                if (convertView == null) {
                    holder = new ItemHolder();
                    holder.llItem = new LinearLayout(parent.getContext());
                    holder.llItem.setOrientation(1);
                    holder.llItem.setTag(holder);
                    if (groupIndex > -1) {
                        if (itemIndex == -1) {
                            holder.title = adapter.getGroupTitleView(groupIndex, holder.title, holder.llItem);
                            holder.llItem.addView(holder.title);
                        } else if (itemIndex > -1) {
                            holder.child = adapter.getChildView(groupIndex, itemIndex, holder.child, holder.llItem);
                            holder.llItem.addView(holder.child);
                        }
                    }
                    return holder.llItem;
                }
                holder = (ItemHolder) convertView.getTag();
                if (groupIndex <= -1) {
                    return convertView;
                }
                if (itemIndex == -1) {
                    holder.title = adapter.getGroupTitleView(groupIndex, holder.title, holder.llItem);
                    if (holder.child == null) {
                        return convertView;
                    }
                    holder.llItem.removeView(holder.child);
                    return convertView;
                } else if (itemIndex <= -1) {
                    return convertView;
                } else {
                    holder.child = adapter.getChildView(groupIndex, itemIndex, holder.child, holder.llItem);
                    if (holder.title == null) {
                        return convertView;
                    }
                    holder.llItem.removeView(holder.title);
                    return convertView;
                }
            }

            public long getItemId(int position) {
                return (long) position;
            }

            public Object getItem(int position) {
                int[] indexPair = getItemIndex(position);
                int groupIndex = indexPair[0];
                int itemIndex = indexPair[1];
                if (groupIndex > -1) {
                    if (itemIndex == -1) {
                        return adapter.getGroupTitle(itemIndex);
                    }
                    if (itemIndex > -1) {
                        return adapter.getChild(groupIndex, itemIndex);
                    }
                }
                return null;
            }

            private int[] getItemIndex(int position) {
                int[] indexPair = new int[]{-1, -2};
                int count = 0;
                int size = adapter.getGroupCount();
                for (int gi = 0; gi < size; gi++) {
                    int groupSize = adapter.getChildrenCount(gi) + 1;
                    if (count + groupSize > position) {
                        indexPair[0] = gi;
                        indexPair[1] = (position - count) - 1;
                        break;
                    }
                    count += groupSize;
                }
                return indexPair;
            }

            public int getCount() {
                int count = 0;
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    count += adapter.getChildrenCount(i) + 1;
                }
                return count;
            }
        };
        adapter.setInnerAdapter(innerAdapter);
        super.setAdapter(innerAdapter);
    }
}
