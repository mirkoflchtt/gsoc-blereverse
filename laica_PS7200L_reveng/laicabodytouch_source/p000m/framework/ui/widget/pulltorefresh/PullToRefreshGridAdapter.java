package p000m.framework.ui.widget.pulltorefresh;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.GridView;

public abstract class PullToRefreshGridAdapter extends PullToRefreshBaseListAdapter {
    private ListInnerAdapter adapter;
    private boolean fling;
    private ScrollableGridView gridView = new ScrollableGridView(getContext());
    private OnListStopScrollListener osListener;

    class C01881 implements OnScrollListener {
        private int firstVisibleItem;
        private int visibleItemCount;

        C01881() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            PullToRefreshGridAdapter.this.fling = scrollState == 2;
            if (scrollState != 0) {
                return;
            }
            if (PullToRefreshGridAdapter.this.osListener != null) {
                PullToRefreshGridAdapter.this.osListener.onListStopScrolling(this.firstVisibleItem, this.visibleItemCount);
            } else if (PullToRefreshGridAdapter.this.adapter != null) {
                PullToRefreshGridAdapter.this.adapter.notifyDataSetChanged();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            PullToRefreshGridAdapter.this.onScroll(PullToRefreshGridAdapter.this.gridView, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    public PullToRefreshGridAdapter(PullToRefreshView view) {
        super(view);
        this.gridView.setOnScrollListener(new C01881());
        this.adapter = new ListInnerAdapter(this);
        this.gridView.setAdapter(this.adapter);
    }

    public Scrollable getBodyView() {
        return this.gridView;
    }

    public boolean isPullReady() {
        return this.gridView.isReadyToPull();
    }

    public GridView getGridView() {
        return this.gridView;
    }

    public boolean isFling() {
        return this.fling;
    }

    public void onScroll(Scrollable scrollable, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.adapter.notifyDataSetChanged();
    }
}
