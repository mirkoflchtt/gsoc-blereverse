package p000m.framework.ui.widget.pulltorefresh;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ListInnerAdapter extends BaseAdapter {
    private PullToRefreshBaseListAdapter adapter;

    public ListInnerAdapter(PullToRefreshBaseListAdapter adapter) {
        this.adapter = adapter;
    }

    public int getCount() {
        return this.adapter.getCount();
    }

    public Object getItem(int position) {
        return this.adapter.getItem(position);
    }

    public long getItemId(int position) {
        return this.adapter.getItemId(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return this.adapter.getView(position, convertView, parent);
    }
}
