package p000m.framework.ui.widget.pulltorefresh;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import p000m.framework.ui.widget.pulltorefresh.GroupListView.GroupListAdapter;

public abstract class PullToRefreshGroupListAdapter extends PullToRefreshAdatper {
    private GroupListAdapter adapter;
    private boolean fling;
    private ScrollableGroupListView listView = new ScrollableGroupListView(getContext());
    private OnListStopScrollListener osListener;

    class C01891 implements OnScrollListener {
        private int firstVisibleItem;
        private int visibleItemCount;

        C01891() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            PullToRefreshGroupListAdapter.this.fling = scrollState == 2;
            if (scrollState != 0) {
                return;
            }
            if (PullToRefreshGroupListAdapter.this.osListener != null) {
                PullToRefreshGroupListAdapter.this.osListener.onListStopScrolling(this.firstVisibleItem, this.visibleItemCount);
            } else if (PullToRefreshGroupListAdapter.this.adapter != null) {
                PullToRefreshGroupListAdapter.this.adapter.notifyDataSetChanged();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            PullToRefreshGroupListAdapter.this.onScroll(PullToRefreshGroupListAdapter.this.listView, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    class C02822 extends GroupListAdapter {
        C02822() {
        }

        public View getGroupTitleView(int groupPosition, View convertView, ViewGroup parent) {
            return PullToRefreshGroupListAdapter.this.getGroupTitleView(groupPosition, convertView, parent);
        }

        public String getGroupTitle(int position) {
            return PullToRefreshGroupListAdapter.this.getGroupTitle(position);
        }

        public int getGroupCount() {
            return PullToRefreshGroupListAdapter.this.getGroupCount();
        }

        public int getChildrenCount(int groupPosition) {
            return PullToRefreshGroupListAdapter.this.getChildrenCount(groupPosition);
        }

        public View getChildView(int groupPosition, int childPosition, View convertView, ViewGroup parent) {
            return PullToRefreshGroupListAdapter.this.getChildView(groupPosition, childPosition, convertView, parent);
        }

        public Object getChild(int groupPosition, int childPosition) {
            return PullToRefreshGroupListAdapter.this.getChild(groupPosition, childPosition);
        }
    }

    public abstract Object getChild(int i, int i2);

    public abstract View getChildView(int i, int i2, View view, ViewGroup viewGroup);

    public abstract int getChildrenCount(int i);

    public abstract int getGroupCount();

    public abstract String getGroupTitle(int i);

    public abstract View getGroupTitleView(int i, View view, ViewGroup viewGroup);

    public PullToRefreshGroupListAdapter(PullToRefreshView view) {
        super(view);
        this.listView.setOnScrollListener(new C01891());
        this.adapter = new C02822();
        this.listView.setAdapter(this.adapter);
    }

    public Scrollable getBodyView() {
        return this.listView;
    }

    public boolean isPullReady() {
        return this.listView.isReadyToPull();
    }

    public GroupListView getListView() {
        return this.listView;
    }

    public boolean isFling() {
        return this.fling;
    }

    public void onScroll(Scrollable scrollable, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.adapter.notifyDataSetChanged();
    }
}
