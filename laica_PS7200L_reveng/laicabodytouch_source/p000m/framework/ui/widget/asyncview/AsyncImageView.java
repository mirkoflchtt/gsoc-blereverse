package p000m.framework.ui.widget.asyncview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import java.util.Random;
import p000m.framework.utils.UIHandler;
import p000m.framework.utils.Utils;

public class AsyncImageView extends ImageView implements AsyncView, BitmapCallback, Callback {
    public static final int DEFAULT_TRANSPARENT = 17170445;
    private static Bitmap DEFAULT_TRANSPARENT_BITMAP = null;
    private static final int MSG_IMG_GOT = 1;
    private static String cacheDir;
    private static final Random rnd = new Random();
    private int defaultRes;
    private OnImageGotListener onImageGotListener;
    private String url;

    public AsyncImageView(Context context) {
        super(context);
        init(context);
    }

    public AsyncImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AsyncImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        UIHandler.prepare();
        if (TextUtils.isEmpty(cacheDir)) {
            cacheDir = Utils.getCachePath(getContext(), "images");
        }
        BitmapProcessor.prepare(cacheDir);
        setOnImageGotListener(SimpleOnImageGotListener.INSTANCE);
    }

    public void setOnImageGotListener(OnImageGotListener l) {
        this.onImageGotListener = l;
    }

    public String getUrl() {
        return this.url;
    }

    public void execute(String url) {
        execute(url, 0);
    }

    public void execute(String url, int defaultRes) {
        this.url = url;
        this.defaultRes = defaultRes;
        if (Utils.isNullOrEmpty(url)) {
            setImageResource(defaultRes);
            return;
        }
        Bitmap bm = BitmapProcessor.getBitmapFromCache(url);
        if (bm == null || bm.isRecycled()) {
            if (defaultRes > 0) {
                setImageBitmap(getDefaultBitmap(defaultRes));
            }
            BitmapProcessor.process(url, this);
            return;
        }
        setImageBitmap(bm);
    }

    private Bitmap getDefaultBitmap(int defaultRes) {
        switch (defaultRes) {
            case DEFAULT_TRANSPARENT /*17170445*/:
                if (DEFAULT_TRANSPARENT_BITMAP == null) {
                    DEFAULT_TRANSPARENT_BITMAP = BitmapFactory.decodeResource(getResources(), DEFAULT_TRANSPARENT);
                }
                return DEFAULT_TRANSPARENT_BITMAP;
            default:
                return BitmapFactory.decodeResource(getResources(), defaultRes);
        }
    }

    public void onImageGot(String url, Bitmap bm) {
        Bitmap shownImage = bm;
        if (this.onImageGotListener != null) {
            shownImage = this.onImageGotListener.onImageGot(this, shownImage, url);
        }
        Message msg = new Message();
        msg.what = 1;
        msg.obj = new Object[]{url, shownImage};
        UIHandler.sendMessageDelayed(msg, (long) rnd.nextInt(300), this);
    }

    public boolean handleMessage(Message msg) {
        if (msg.what == 1) {
            Object url = ((Object[]) msg.obj)[0];
            Object bm = ((Object[]) msg.obj)[1];
            if (bm == null || url == null || !url.equals(this.url)) {
                setImageResource(this.defaultRes);
            } else {
                setImageBitmap((Bitmap) bm);
            }
        }
        return false;
    }
}
