package p000m.framework.ui.widget.asyncview;

import android.graphics.Bitmap;

public class SimpleOnImageGotListener implements OnImageGotListener {
    public static final SimpleOnImageGotListener INSTANCE = new SimpleOnImageGotListener();

    private SimpleOnImageGotListener() {
    }

    public Bitmap onImageGot(AsyncView view, Bitmap image, String url) {
        return (url == null || url.trim().length() <= 0 || !url.equals(view.getUrl())) ? null : image;
    }
}
