package p000m.framework.ui.widget.asyncview;

public interface AsyncView {
    void execute(String str, int i);

    String getUrl();
}
