package p000m.framework.ui.widget.asyncview;

import android.graphics.Bitmap;

public interface OnImageGotListener {
    Bitmap onImageGot(AsyncView asyncView, Bitmap bitmap, String str);
}
