package p000m.framework.ui.widget.slidingmenu;

import java.util.ArrayList;
import java.util.Iterator;

final class SlidingMenuGroup {
    int id;
    private ArrayList<SlidingMenuItem> items = new ArrayList();
    String text;

    SlidingMenuGroup() {
    }

    void setItem(SlidingMenuItem item) {
        if (item != null) {
            SlidingMenuItem itemTmp = findItemById(item.id);
            item.group = this.id;
            if (itemTmp == null) {
                this.items.add(item);
                return;
            }
            int index = this.items.indexOf(itemTmp);
            this.items.remove(index);
            this.items.add(index, item);
        }
    }

    SlidingMenuItem findItemById(int id) {
        if (this.items == null) {
            return null;
        }
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            SlidingMenuItem item = (SlidingMenuItem) it.next();
            if (item != null && item.id == id) {
                return item;
            }
        }
        return null;
    }

    int getCount() {
        return this.items == null ? 0 : this.items.size();
    }

    SlidingMenuItem getItem(int position) {
        return (SlidingMenuItem) this.items.get(position);
    }
}
