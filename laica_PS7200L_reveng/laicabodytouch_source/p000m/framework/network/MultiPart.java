package p000m.framework.network;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class MultiPart extends HTTPPart {
    private ArrayList<HTTPPart> parts = new ArrayList();

    public MultiPart append(HTTPPart part) throws Throwable {
        this.parts.add(part);
        return this;
    }

    protected InputStream getInputStream() throws Throwable {
        MultiPartInputStream mpis = new MultiPartInputStream();
        Iterator it = this.parts.iterator();
        while (it.hasNext()) {
            mpis.addInputStream(((HTTPPart) it.next()).getInputStream());
        }
        return mpis;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator it = this.parts.iterator();
        while (it.hasNext()) {
            sb.append(((HTTPPart) it.next()).toString());
        }
        return sb.toString();
    }

    protected long length() throws Throwable {
        long length = 0;
        Iterator it = this.parts.iterator();
        while (it.hasNext()) {
            length += ((HTTPPart) it.next()).length();
        }
        return length;
    }
}
