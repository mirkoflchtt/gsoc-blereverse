package p000m.framework.network;

import java.io.InputStream;

public interface ResponseCallback {
    void onResponse(InputStream inputStream);
}
