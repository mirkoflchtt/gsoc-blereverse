package org.androidannotations.api.rest;

import org.springframework.web.client.RestTemplate;

public interface RestClientSupport {
    RestTemplate getRestTemplate();

    void setRestTemplate(RestTemplate restTemplate);
}
