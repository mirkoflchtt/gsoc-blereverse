package org.androidannotations.api.rest;

import org.springframework.core.NestedRuntimeException;

public interface RestErrorHandler {
    void onRestClientExceptionThrown(NestedRuntimeException nestedRuntimeException);
}
