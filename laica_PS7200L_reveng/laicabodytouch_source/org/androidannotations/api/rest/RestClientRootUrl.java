package org.androidannotations.api.rest;

public interface RestClientRootUrl {
    String getRootUrl();

    void setRootUrl(String str);
}
