package org.androidannotations.api;

import android.os.Build.VERSION;

public class SdkVersionHelper {

    private static class HelperInternal {
        private HelperInternal() {
        }

        private static int getSdkIntInternal() {
            return VERSION.SDK_INT;
        }
    }

    public static int getSdkInt() {
        if (VERSION.RELEASE.startsWith("1.5")) {
            return 3;
        }
        return HelperInternal.getSdkIntInternal();
    }
}
