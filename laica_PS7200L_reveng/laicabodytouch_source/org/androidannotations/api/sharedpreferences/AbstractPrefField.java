package org.androidannotations.api.sharedpreferences;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public abstract class AbstractPrefField {
    protected final String key;
    protected final SharedPreferences sharedPreferences;

    public AbstractPrefField(SharedPreferences sharedPreferences, String key) {
        this.sharedPreferences = sharedPreferences;
        this.key = key;
    }

    public final boolean exists() {
        return this.sharedPreferences.contains(this.key);
    }

    public String key() {
        return this.key;
    }

    public final void remove() {
        apply(edit().remove(this.key));
    }

    protected Editor edit() {
        return this.sharedPreferences.edit();
    }

    protected final void apply(Editor editor) {
        SharedPreferencesCompat.apply(editor);
    }
}
