package com.whb.loease.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import com.vc.cloudbalance.common.AppService;
import com.vc.cloudbalance.common.ApplicationMamager;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.CrashHandler;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.widget.View_Themes_;
import java.util.Locale;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;

@EActivity(2130903058)
@NoTitle
public class AppConfig_NullActivity extends Activity {
    private boolean isload = false;
    @ViewById
    LinearLayout llTop;
    private Context mContext;

    class C01411 implements OnGlobalLayoutListener {

        class C01401 implements OnDismissListener {
            C01401() {
            }

            public void onDismiss() {
                AppConfig_NullActivity.this.startActivity(MemberInfoActivity_.intent(AppConfig_NullActivity.this.mContext).get());
                AppConfig_NullActivity.this.isload = false;
            }
        }

        C01411() {
        }

        public void onGlobalLayout() {
            if (!AppConfig_NullActivity.this.isload) {
                AppConfig_NullActivity.this.isload = true;
                PopupWindow mPopupWindow = new PopupWindow(-1, -1);
                mPopupWindow.setContentView(View_Themes_.build(AppConfig_NullActivity.this.mContext, mPopupWindow));
                mPopupWindow.showAsDropDown(AppConfig_NullActivity.this.llTop);
                mPopupWindow.setOnDismissListener(new C01401());
            }
        }
    }

    protected void onResume() {
        init();
        System.gc();
        super.onResume();
    }

    void init() {
        this.mContext = this;
        CrashHandler.getInstance().init(getApplicationContext());
        AppService.actionStart(this);
        getWindow().addFlags(67108864);
        ApplicationMamager.getInstance().addActivity(this);
        initLanguage();
        if (new AppConfigDAL(this).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING).isEmpty()) {
            new AppConfigDAL(this).insert(Constants.SQL_KEY_BALANCETYPE_STRING, WeightUnitHelper.Kg);
        }
        if (new AppConfigDAL(this).select(Constants.THEME_COLOR_STRING).isEmpty()) {
            this.llTop.getViewTreeObserver().addOnGlobalLayoutListener(new C01411());
        } else {
            startActivity(MainActivity_.intent((Context) this).get());
        }
    }

    private void initLanguage() {
        SharedPreferences sharedPreferences = getSharedPreferences("AppConfigDAL", 0);
        Editor editor = sharedPreferences.edit();
        int LanguageVal = sharedPreferences.getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
        Locale locale = getResources().getConfiguration().locale;
        String systemLanguage = Locale.getDefault().getLanguage();
        String[] availibleLanguages = Common.getLanguageShortCodes();
        if (LanguageVal == 0) {
            boolean isConfig = false;
            int i = 0;
            while (i < availibleLanguages.length) {
                if (systemLanguage.endsWith("zh")) {
                    if (locale.getCountry().equals("CN")) {
                        editor.putInt(Constants.SQL_KEY_LANGUAGE_STRING, 1);
                        LanguageVal = 1;
                    } else {
                        editor.putInt(Constants.SQL_KEY_LANGUAGE_STRING, 2);
                        LanguageVal = 2;
                    }
                    editor.commit();
                    isConfig = true;
                } else if (systemLanguage.endsWith(availibleLanguages[i])) {
                    editor.putInt(Constants.SQL_KEY_LANGUAGE_STRING, i + 1);
                    editor.commit();
                    LanguageVal = i + 1;
                    isConfig = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!isConfig) {
                editor.putInt(Constants.SQL_KEY_LANGUAGE_STRING, 8);
                editor.commit();
                LanguageVal = 8;
            }
        }
        try {
            Resources resources = getResources();
            Configuration config = resources.getConfiguration();
            DisplayMetrics dm = resources.getDisplayMetrics();
            if (LanguageVal == 1 || LanguageVal == 2) {
                config.locale = new Locale("zh", availibleLanguages[LanguageVal - 1]);
            } else {
                config.locale = new Locale(availibleLanguages[LanguageVal - 1]);
            }
            resources.updateConfiguration(config, dm);
            Locale.setDefault(config.locale);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
