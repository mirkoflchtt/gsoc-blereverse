package com.whb.loease.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.vc.cloudbalance.common.ApplicationMamager;
import com.vc.cloudbalance.common.BaseActivity;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.DatePickerDialogCustom;
import com.vc.cloudbalance.common.DialogHelper;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.ActionMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.ActionDAL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.dialog.ListViewWindow;
import com.vc.dialog.ListViewWindow.OnSelectListener;
import com.vc.util.FileHelper;
import com.vc.util.ImageUtil;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity
public class MemberInfoActivity extends BaseActivity {
    private static final int CAMERA_WITH_DATA = 1001;
    private static final File PHOTO_DIR = new File(new StringBuilder(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath())).append("/DCIM/Camera").toString());
    private static final int PHOTO_PICKED_WITH_DATA = 1002;
    private static final int PHOTO_REQUEST_CUT = 1003;
    private String Gender = "1";
    @Extra("EXTRA_KEY_ID_STRING")
    String MemberId;
    private String ModelType = WeightUnitHelper.Kg;
    Bitmap bitmap;
    @ViewById
    Button btnDelete;
    @ViewById
    CircleImageView circleImageView;
    @ViewById
    EditText etAge;
    @ViewById
    EditText etHeight;
    @ViewById
    EditText etName;
    @ViewById
    EditText etSex;
    @ViewById
    ImageView imgMember;
    byte[] imgStream;
    boolean isLoad = false;
    @ViewById
    LinearLayout llImgHead;
    public File mCurrentPhotoFile;
    MemberMDL member;
    ListViewWindow mode_listViewWindow;
    private OnSelectListener onPicSelectListener = new C02771();
    @ViewById
    RadioButton rbGenderFeman;
    @ViewById
    RadioButton rbGenderMan;
    @ViewById
    RadioGroup rbgGender;
    ListViewWindow sex_listViewWindow;
    ListViewWindow takePic_listViewWindow;
    @ViewById
    TextView tvSave;
    @ViewById
    TextView tvTitle;

    class C01582 implements OnTouchListener {
        C01582() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                MemberInfoActivity.this.showDate();
            }
            return true;
        }
    }

    class C01593 implements OnTouchListener {
        C01593() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                MemberInfoActivity.this.showHeight();
            }
            return true;
        }
    }

    class C01604 implements OnCheckedChangeListener {
        C01604() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == C0181R.id.rbGenderFeman) {
                MemberInfoActivity.this.Gender = WeightUnitHelper.Kg;
                MemberInfoActivity.this.rbGenderFeman.setBackgroundColor(Common.getThemeColor(MemberInfoActivity.this.mContext));
                MemberInfoActivity.this.rbGenderMan.setBackground(null);
            } else if (checkedId == C0181R.id.rbGenderMan) {
                MemberInfoActivity.this.Gender = "1";
                MemberInfoActivity.this.rbGenderMan.setBackgroundColor(Common.getThemeColor(MemberInfoActivity.this.mContext));
                MemberInfoActivity.this.rbGenderFeman.setBackground(null);
            }
        }
    }

    class C01615 implements OnGlobalLayoutListener {
        C01615() {
        }

        public void onGlobalLayout() {
            if (!MemberInfoActivity.this.isLoad) {
                LayoutParams lp = (LayoutParams) MemberInfoActivity.this.imgMember.getLayoutParams();
                lp.width = MemberInfoActivity.this.imgMember.getWidth();
                lp.height = MemberInfoActivity.this.imgMember.getHeight();
                MemberInfoActivity.this.imgMember.setLayoutParams(lp);
                LayoutParams lp2 = (LayoutParams) MemberInfoActivity.this.circleImageView.getLayoutParams();
                lp2.width = MemberInfoActivity.this.imgMember.getWidth();
                lp2.height = MemberInfoActivity.this.imgMember.getHeight();
                MemberInfoActivity.this.circleImageView.setLayoutParams(lp2);
                MemberInfoActivity.this.isLoad = true;
                if (TextUtils.isEmpty(MemberInfoActivity.this.MemberId)) {
                    MemberInfoActivity.this.btnDelete.setVisibility(4);
                    return;
                }
                MemberInfoActivity.this.member = new MemberDAL(MemberInfoActivity.this.mContext).SelectByClientId(MemberInfoActivity.this.MemberId);
                MemberInfoActivity.this.rbgGender.setEnabled(false);
                MemberInfoActivity.this.btnDelete.setVisibility(0);
                MemberInfoActivity.this.setData();
            }
        }
    }

    class C01658 implements OnClickListener {
        C01658() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (TextUtils.isEmpty(MemberInfoActivity.this.member.getMemberid())) {
                new MemberDAL(MemberInfoActivity.this.mContext).DelByClientId(MemberInfoActivity.this.member.getClientid());
            } else {
                new ActionDAL(MemberInfoActivity.this.mContext).Insert(new ActionMDL(UUID.randomUUID().toString(), ActionMDL.DelMember, MemberInfoActivity.this.member.getMemberid()));
                new MemberDAL(MemberInfoActivity.this.mContext).DelByMemberId(MemberInfoActivity.this.member.getMemberid());
                Common.DelMemberThread(MemberInfoActivity.this.member, MemberInfoActivity.this.mContext);
            }
            new AppConfigDAL(MemberInfoActivity.this.mContext).insert(Constants.SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING, "");
            MemberInfoActivity.this.finish();
        }
    }

    class C01669 implements OnClickListener {
        C01669() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C02771 implements OnSelectListener {
        C02771() {
        }

        public void onSelect(DialogInterface dialog, int which) {
            if (which == 0) {
                MemberInfoActivity.this.doTakePhoto();
            } else if (which == 1) {
                MemberInfoActivity.this.doPickPhotoFromGallery();
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initThemeView();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        int i = newConfig.orientation;
        super.onConfigurationChanged(newConfig);
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                setContentView(C0181R.layout.activity_member_info);
                return;
            case 2:
                setContentView(C0181R.layout.activity_member_info_greentheme);
                return;
            case 4:
                setContentView(C0181R.layout.activity_member_info_greytheme);
                return;
            default:
                setContentView(C0181R.layout.activity_member_info_bluetheme);
                return;
        }
    }

    protected void onDestroy() {
        if (this.bitmap != null) {
            this.circleImageView.setImageBitmap(null);
            this.bitmap.recycle();
        }
        this.circleImageView = null;
        this.bitmap = null;
        super.onDestroy();
    }

    private void initLanguage() {
        int LanguageVal = getSharedPreferences("AppConfigDAL", 0).getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
        if (LanguageVal != 0) {
            try {
                Resources resources = getResources();
                Configuration config = resources.getConfiguration();
                DisplayMetrics dm = resources.getDisplayMetrics();
                if (LanguageVal == 17) {
                    this.tvSave.setTextSize(17.0f);
                }
                if (LanguageVal == 1 || LanguageVal == 2) {
                    config.locale = new Locale("zh", Common.getLanguageShortCodes()[LanguageVal - 1]);
                } else {
                    config.locale = new Locale(Common.getLanguageShortCodes()[LanguageVal - 1]);
                }
                resources.updateConfiguration(config, dm);
                Locale.setDefault(config.locale);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @AfterViews
    void init() {
        this.llImgHead.setFocusable(true);
        this.llImgHead.setFocusableInTouchMode(true);
        String birthdayDate = new StringBuilder(String.valueOf(Calendar.getInstance().get(1) - 25)).append("-").append(1).append("-").append(1).toString();
        this.etAge.setText(new StringBuilder(String.valueOf(25)).toString());
        this.etAge.setTag(birthdayDate);
        initLanguage();
        this.etAge.setOnTouchListener(new C01582());
        this.etHeight.setOnTouchListener(new C01593());
        this.rbgGender.setOnCheckedChangeListener(new C01604());
        this.imgMember.getViewTreeObserver().addOnGlobalLayoutListener(new C01615());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        initLanguage();
        if (resultCode != 0) {
            switch (requestCode) {
                case CAMERA_WITH_DATA /*1001*/:
                    if (data != null) {
                        if (data.hasExtra("data")) {
                            this.bitmap = (Bitmap) data.getParcelableExtra("data");
                            this.bitmap = ImageUtil.compBitmap(this.bitmap);
                            this.imgStream = ImageUtil.Bitmap2Bytes(this.bitmap);
                            setImageBitmap(this.bitmap);
                            return;
                        }
                        return;
                    }
                    startPhotoZoom(Uri.fromFile(this.mCurrentPhotoFile), 300);
                    return;
                case PHOTO_PICKED_WITH_DATA /*1002*/:
                    if (data != null) {
                        Uri uri = data.getData();
                        String img_path = "";
                        boolean isHasClass = true;
                        try {
                            Class.forName("android.provider.DocumentsContract");
                        } catch (ClassNotFoundException e) {
                            isHasClass = false;
                        }
                        if (isHasClass && DocumentsContract.isDocumentUri(this, uri)) {
                            String id = DocumentsContract.getDocumentId(uri).split(":")[1];
                            String[] column = new String[]{"_data"};
                            Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI, column, "_id=?", new String[]{id}, null);
                            int columnIndex = cursor.getColumnIndex(column[0]);
                            if (cursor.moveToFirst()) {
                                img_path = cursor.getString(columnIndex);
                            }
                            cursor.close();
                        } else {
                            Cursor actualimagecursor = managedQuery(uri, new String[]{"_data"}, null, null, null);
                            int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow("_data");
                            actualimagecursor.moveToFirst();
                            img_path = actualimagecursor.getString(actual_image_column_index);
                        }
                        if (img_path != null) {
                            if (!img_path.equals("")) {
                                startPhotoZoom(Uri.fromFile(new File(img_path)), 300);
                                return;
                            }
                        }
                        try {
                            startPhotoZoom(uri, 300);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    return;
                case PHOTO_REQUEST_CUT /*1003*/:
                    this.bitmap = (Bitmap) data.getExtras().getParcelable("data");
                    this.bitmap = ImageUtil.compBitmap(this.bitmap);
                    this.imgStream = ImageUtil.Bitmap2Bytes(this.bitmap);
                    setImageBitmap(this.bitmap);
                    return;
                default:
                    return;
            }
        }
    }

    private void startPhotoZoom(Uri uri, int size) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", size);
        intent.putExtra("outputY", size);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    public void showHeight() {
        final NumberPicker np = new NumberPicker(this.mContext);
        np.setMaxValue(230);
        np.setMinValue(10);
        int val = ObjectHelper.Convert2Int(this.etHeight.getText().toString());
        if (val <= 0) {
            val = 160;
        }
        np.setValue(val);
        new Builder(this.mContext).setTitle(new StringBuilder(String.valueOf(this.mContext.getString(C0181R.string.height))).append("(cm)").toString()).setView(np).setPositiveButton(this.mContext.getString(C0181R.string.confirm), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int height = ObjectHelper.Convert2Int(Integer.valueOf(np.getValue()));
                if (height < 10 || height > 230) {
                    MemberInfoActivity.this.showHeight();
                    return;
                }
                MemberInfoActivity.this.etHeight.setText(new StringBuilder(String.valueOf(np.getValue())).toString());
                dialog.dismiss();
            }
        }).setNegativeButton(this.mContext.getString(C0181R.string.cancle), null).show();
    }

    public void setData() {
        this.etName.setText(this.member.getMembername());
        this.etAge.setTag(this.member.getBirthday());
        if (!TextUtils.isEmpty(this.member.getBirthday())) {
            int age = Common.GetAgeByBirthday(this.member.getBirthday());
            if (age <= 0) {
                this.etAge.setText((0 - ObjectHelper.GetReciprocalDays(this.member.getBirthday())) + getString(C0181R.string.day));
            } else {
                this.etAge.setText(new StringBuilder(String.valueOf(age)).toString());
            }
        }
        this.etHeight.setText(this.member.getHeight());
        this.Gender = this.member.getSex();
        if (this.Gender.equals(WeightUnitHelper.Kg)) {
            this.rbGenderFeman.setChecked(true);
            this.rbGenderFeman.setBackgroundColor(Common.getThemeColor(this.mContext));
            this.rbGenderMan.setBackground(null);
        } else if (this.Gender.equals("1")) {
            this.rbGenderMan.setChecked(true);
            this.rbGenderMan.setBackgroundColor(Common.getThemeColor(this.mContext));
            this.rbGenderFeman.setBackground(null);
        }
        if (this.member.getClientImg() != null && this.member.getClientImg().length > 0) {
            this.imgStream = this.member.getClientImg();
            this.bitmap = ImageUtil.decodeSampledBitmapFromByte(this.imgStream, this.imgMember.getWidth(), this.imgMember.getHeight());
            if (this.bitmap != null) {
                setImageBitmap(this.bitmap);
            }
        }
        if (this.member.getIconfile() != null && !this.member.getIconfile().equals("")) {
            loadImage(this.member.getIconfile());
        }
    }

    private void loadImage(final String urlString) {
        if (urlString.startsWith("http")) {
            new Thread() {

                class C01631 implements Runnable {
                    C01631() {
                    }

                    public void run() {
                        MemberInfoActivity.this.setImageBitmap(MemberInfoActivity.this.bitmap);
                    }
                }

                public void run() {
                    try {
                        HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
                        conn.setDoInput(true);
                        conn.connect();
                        InputStream inputStream = conn.getInputStream();
                        MemberInfoActivity.this.bitmap = BitmapFactory.decodeStream(inputStream);
                        MemberInfoActivity.this.bitmap = ImageUtil.compBitmap(MemberInfoActivity.this.bitmap);
                        MemberInfoActivity.this.imgStream = ImageUtil.Bitmap2Bytes(MemberInfoActivity.this.bitmap);
                        MemberInfoActivity.this.member.setClientImg(MemberInfoActivity.this.imgStream);
                        new MemberDAL(MemberInfoActivity.this.mContext).UpdateImage(MemberInfoActivity.this.member);
                        MemberInfoActivity.this.runOnUiThread(new C01631());
                    } catch (Exception e) {
                    }
                    super.run();
                }
            }.start();
            return;
        }
        this.imgMember.setImageURI(Uri.parse(urlString));
        this.imgMember.setVisibility(8);
        this.circleImageView.setVisibility(0);
        this.circleImageView.setImageURI(Uri.parse(urlString));
        this.imgStream = FileHelper.encodeByteFile(urlString);
    }

    @Click({2131492988})
    void Action() {
        if (this.member != null) {
            String aString = this.mContext.getString(C0181R.string.confirmDeleMember);
            DialogHelper.showComfrimDialog(this, null, this.mContext.getString(C0181R.string.confirmDeleMember), this.mContext.getString(C0181R.string.confirm), new C01658(), this.mContext.getString(C0181R.string.cancle), new C01669());
        }
    }

    boolean Ver() {
        if (!TextUtils.isEmpty(this.etName.getText().toString())) {
            return true;
        }
        DialogHelper.showTost(this.mContext, this.mContext.getString(C0181R.string.fillName));
        return false;
    }

    @Click({2131492964})
    void save() {
        if (Ver()) {
            doSave();
        }
    }

    private void doSave() {
        boolean state;
        String Name = this.etName.getText().toString();
        String Birthday = (String) this.etAge.getTag();
        String Height = this.etHeight.getText().toString();
        if (this.member == null) {
            this.member = new MemberMDL();
            this.member.setUpload(0);
        }
        this.member.setClientImg(this.imgStream);
        if (GetApp().getUser() == null) {
            this.member.setUserid("");
        } else {
            this.member.setUserid(GetApp().getUser().getUserid());
        }
        this.member.setMembername(Name);
        this.member.setBirthday(Birthday);
        this.member.setHeight(Height);
        this.member.setSex(this.Gender);
        this.member.setModeltype(this.ModelType);
        if (TextUtils.isEmpty(this.member.getClientid())) {
            this.member.setClientid(UUID.randomUUID().toString());
            state = new MemberDAL(this.mContext).Insert(this.member);
            if (state) {
                new ActionDAL(this.mContext).Insert(new ActionMDL(UUID.randomUUID().toString(), ActionMDL.AddMember, this.member.getClientid()));
                Common.AddMemberThread(this.mContext);
            }
        } else {
            state = new MemberDAL(this.mContext).Update(this.member);
            if (state) {
                new ActionDAL(this.mContext).Insert(new ActionMDL(UUID.randomUUID().toString(), ActionMDL.UpdateMember, this.member.getClientid()));
                Common.UpdateMemberThread(this.member, this.mContext);
            }
        }
        if (state) {
            ApplicationMamager.getInstance().goMainActivity(this.mContext);
            finish();
        }
    }

    @Click({2131492966})
    void selectMemberImage() {
        this.takePic_listViewWindow = new ListViewWindow(this.mContext, this.onPicSelectListener, new String[]{this.mContext.getString(C0181R.string.camera), this.mContext.getString(C0181R.string.photoLibrary)}, null);
        this.takePic_listViewWindow.show();
    }

    @Click({2131492882})
    void selectMemberImage2() {
        this.takePic_listViewWindow = new ListViewWindow(this.mContext, this.onPicSelectListener, new String[]{this.mContext.getString(C0181R.string.camera), this.mContext.getString(C0181R.string.photoLibrary)}, null);
        this.takePic_listViewWindow.show();
    }

    protected void doPickPhotoFromGallery() {
        try {
            ((Activity) this.mContext).startActivityForResult(getPhotoPickIntent(), PHOTO_PICKED_WITH_DATA);
        } catch (ActivityNotFoundException e) {
        }
    }

    public Intent getPhotoPickIntent() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT", null);
        intent.setType("image/*");
        intent.putExtra("return-data", false);
        return intent;
    }

    protected void doTakePhoto() {
        try {
            PHOTO_DIR.mkdirs();
            this.mCurrentPhotoFile = new File(PHOTO_DIR, getPhotoFileName());
            if (!this.mCurrentPhotoFile.exists()) {
                try {
                    this.mCurrentPhotoFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ((Activity) this.mContext).startActivityForResult(getTakePickIntent(this.mCurrentPhotoFile), CAMERA_WITH_DATA);
        } catch (ActivityNotFoundException e2) {
        }
    }

    private String getPhotoFileName() {
        return new StringBuilder(String.valueOf(new SimpleDateFormat("'IMG'_yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())))).append(".png").toString();
    }

    public Intent getTakePickIntent(File f) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE", null);
        intent.putExtra("output", Uri.fromFile(this.mCurrentPhotoFile));
        intent.putExtra("return-data", true);
        return intent;
    }

    public void setImageUrl(String url) {
        this.bitmap = BitmapFactory.decodeFile(url);
        this.bitmap = ImageUtil.compBitmap(this.bitmap);
        this.imgStream = ImageUtil.Bitmap2Bytes(this.bitmap);
        setImageBitmap(this.bitmap);
    }

    public void setCamImageUrl(String url) {
        this.bitmap = BitmapFactory.decodeFile(url);
        try {
            int rotate = 0;
            switch (new ExifInterface(url).getAttributeInt("Orientation", 0)) {
                case 3:
                    rotate = 180;
                    break;
                case 6:
                    rotate = 90;
                    break;
                case 8:
                    rotate = 270;
                    break;
            }
            Matrix m = new Matrix();
            m.setRotate((float) rotate);
            this.bitmap = Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), m, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.bitmap = ImageUtil.compBitmap(this.bitmap);
        this.imgStream = ImageUtil.Bitmap2Bytes(this.bitmap);
        setImageBitmap(this.bitmap);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.imgMember.setVisibility(8);
        this.circleImageView.setVisibility(0);
        this.circleImageView.setImageBitmap(bitmap);
    }

    private void showDate() {
        Dialog dialog = new Dialog(this.mContext);
        Calendar c = Calendar.getInstance();
        String text = (String) this.etAge.getTag();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(1);
        int month = cal.get(2);
        int day = cal.get(5);
        if (!TextUtils.isEmpty(text)) {
            Date date = ObjectHelper.Convert2Date(text, "yyyy-MM-dd");
            cal.setTime(date);
            if (date != null) {
                year = cal.get(1);
                month = cal.get(2);
                day = cal.get(5);
            }
        }
        new DatePickerDialogCustom(this.mContext, new OnDateSetListener() {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
                String nowmonth;
                String nowdayOfMonth;
                if (month + 1 < 10) {
                    nowmonth = new StringBuilder(WeightUnitHelper.Kg).append(month + 1).toString();
                } else {
                    nowmonth = new StringBuilder(String.valueOf(month + 1)).toString();
                }
                if (dayOfMonth < 10) {
                    nowdayOfMonth = new StringBuilder(WeightUnitHelper.Kg).append(dayOfMonth).toString();
                } else {
                    nowdayOfMonth = new StringBuilder(String.valueOf(dayOfMonth)).toString();
                }
                String selectDateTime = new StringBuilder(String.valueOf(year)).append("-").append(nowmonth).append("-").append(nowdayOfMonth).toString();
                MemberInfoActivity.this.etAge.setTag(selectDateTime);
                int age = Common.GetAgeByBirthday(selectDateTime);
                if (age <= 0) {
                    MemberInfoActivity.this.etAge.setText((0 - ObjectHelper.GetReciprocalDays(selectDateTime)) + MemberInfoActivity.this.mContext.getString(C0181R.string.day));
                    return;
                }
                MemberInfoActivity.this.etAge.setText(new StringBuilder(String.valueOf(age)).toString());
            }
        }, year, month, day).show();
    }
}
