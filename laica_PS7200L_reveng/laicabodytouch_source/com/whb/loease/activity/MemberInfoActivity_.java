package com.whb.loease.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import de.hdodenhof.circleimageview.CircleImageView;
import org.androidannotations.api.builder.ActivityIntentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class MemberInfoActivity_ extends MemberInfoActivity implements HasViews, OnViewChangedListener {
    public static final String MEMBER_ID_EXTRA = "EXTRA_KEY_ID_STRING";
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01671 implements OnClickListener {
        C01671() {
        }

        public void onClick(View view) {
            MemberInfoActivity_.this.goBack();
        }
    }

    class C01682 implements OnClickListener {
        C01682() {
        }

        public void onClick(View view) {
            MemberInfoActivity_.this.Action();
        }
    }

    class C01693 implements OnClickListener {
        C01693() {
        }

        public void onClick(View view) {
            MemberInfoActivity_.this.selectMemberImage();
        }
    }

    class C01704 implements OnClickListener {
        C01704() {
        }

        public void onClick(View view) {
            MemberInfoActivity_.this.save();
        }
    }

    class C01715 implements OnClickListener {
        C01715() {
        }

        public void onClick(View view) {
            MemberInfoActivity_.this.selectMemberImage2();
        }
    }

    public static class IntentBuilder_ extends ActivityIntentBuilder<IntentBuilder_> {
        private Fragment fragmentSupport_;
        private android.app.Fragment fragment_;

        public IntentBuilder_(Context context) {
            super(context, MemberInfoActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            super(fragment.getActivity(), MemberInfoActivity_.class);
            this.fragment_ = fragment;
        }

        public IntentBuilder_(Fragment fragment) {
            super(fragment.getActivity(), MemberInfoActivity_.class);
            this.fragmentSupport_ = fragment;
        }

        public void startForResult(int requestCode) {
            if (this.fragmentSupport_ != null) {
                this.fragmentSupport_.startActivityForResult(this.intent, requestCode);
            } else if (this.fragment_ != null) {
                this.fragment_.startActivityForResult(this.intent, requestCode);
            } else {
                super.startForResult(requestCode);
            }
        }

        public IntentBuilder_ MemberId(String MemberId) {
            return (IntentBuilder_) super.extra("EXTRA_KEY_ID_STRING", MemberId);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        requestWindowFeature(1);
        injectExtras_();
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static IntentBuilder_ intent(Context context) {
        return new IntentBuilder_(context);
    }

    public static IntentBuilder_ intent(android.app.Fragment fragment) {
        return new IntentBuilder_(fragment);
    }

    public static IntentBuilder_ intent(Fragment supportFragment) {
        return new IntentBuilder_(supportFragment);
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.btnBack = (Button) hasViews.findViewById(C0181R.id.btnBack);
        this.etSex = (EditText) hasViews.findViewById(C0181R.id.etSex);
        this.rbGenderFeman = (RadioButton) hasViews.findViewById(C0181R.id.rbGenderFeman);
        this.circleImageView = (CircleImageView) hasViews.findViewById(C0181R.id.circleImageView);
        this.etHeight = (EditText) hasViews.findViewById(C0181R.id.etHeight);
        this.tvSave = (TextView) hasViews.findViewById(C0181R.id.tvSave);
        this.llImgHead = (LinearLayout) hasViews.findViewById(C0181R.id.llImgHead);
        this.tvTitle = this.tvTitle;
        this.btnDelete = (Button) hasViews.findViewById(C0181R.id.btnDelete);
        this.etName = (EditText) hasViews.findViewById(C0181R.id.etName);
        this.rbGenderMan = (RadioButton) hasViews.findViewById(C0181R.id.rbGenderMan);
        this.rbgGender = (RadioGroup) hasViews.findViewById(C0181R.id.rbgGender);
        this.etAge = (EditText) hasViews.findViewById(C0181R.id.etAge);
        this.imgMember = (ImageView) hasViews.findViewById(C0181R.id.imgMember);
        if (this.btnBack != null) {
            this.btnBack.setOnClickListener(new C01671());
        }
        if (this.btnDelete != null) {
            this.btnDelete.setOnClickListener(new C01682());
        }
        if (this.imgMember != null) {
            this.imgMember.setOnClickListener(new C01693());
        }
        if (this.tvSave != null) {
            this.tvSave.setOnClickListener(new C01704());
        }
        if (this.circleImageView != null) {
            this.circleImageView.setOnClickListener(new C01715());
        }
        InitBase();
        init();
    }

    private void injectExtras_() {
        Bundle extras_ = getIntent().getExtras();
        if (extras_ != null && extras_.containsKey("EXTRA_KEY_ID_STRING")) {
            this.MemberId = extras_.getString("EXTRA_KEY_ID_STRING");
        }
    }

    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
        injectExtras_();
    }
}
