package com.whb.loease.activity;

import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;
import com.vc.cloudbalance.adapter.BasePageAdapter;
import com.vc.cloudbalance.common.BaseActivity;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.cloudbalance.widget.View_DataChart_Adult;
import com.vc.cloudbalance.widget.View_DataChart_Adult_;
import com.vc.cloudbalance.widget.View_DataList_Adult;
import com.vc.cloudbalance.widget.View_DataList_Adult_;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(2130903040)
public class DataViewActivity extends BaseActivity {
    @Extra("EXTRA_KEY_ID_STRING")
    String ClientId;
    int ViewIndex = 0;
    BasePageAdapter adapter;
    View_DataChart_Adult adultChartView;
    View_DataList_Adult adultListView;
    Animation leftInAnimation;
    Animation leftOutAnimation;
    @ViewById
    View lineViewLeft;
    @ViewById
    View lineViewRight;
    Context mContext;
    private GestureDetector mGestureDetector;
    private MemberMDL member;
    OnGestureListener onGestureListener = new C01421();
    private OnParentViewChangeListener onParentViewChangeListener;
    Animation rightInAnimation;
    Animation rightOutAnimation;
    @ViewById
    ViewFlipper viewFlipper;
    ViewFlipper viewFlipperParent;

    class C01421 implements OnGestureListener {
        C01421() {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() - e2.getX() > 120.0f) {
                DataViewActivity.this.showNextPage();
                return true;
            } else if (e1.getX() - e2.getX() >= -200.0f) {
                return false;
            } else {
                if (DataViewActivity.this.ViewIndex == 0) {
                    if (DataViewActivity.this.onParentViewChangeListener != null) {
                        DataViewActivity.this.onParentViewChangeListener.onPreviou();
                    }
                    return false;
                }
                DataViewActivity.this.showPreviousPage();
                return true;
            }
        }

        public boolean onDown(MotionEvent e) {
            return false;
        }
    }

    class C01432 implements OnTouchListener {
        C01432() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            DataViewActivity.this.mGestureDetector.onTouchEvent(event);
            return true;
        }
    }

    public interface OnParentViewChangeListener {
        void onNext();

        void onPreviou();
    }

    public void setParentViewFlipper(ViewFlipper v) {
        this.viewFlipperParent = v;
    }

    public void setOnParentViewChangeListener(OnParentViewChangeListener o) {
        this.onParentViewChangeListener = o;
    }

    public void setMember(MemberMDL mdl) {
        this.member = mdl;
    }

    public void loadListData() {
        if (this.adultListView != null) {
            this.adultListView.loadData();
        }
    }

    public void removeListData() {
        if (this.adultListView != null) {
            this.adultListView.removeData();
        }
    }

    public void refreshChart() {
        if (this.adultChartView != null) {
            this.adultChartView.refreshChart();
        }
    }

    private void initThemeView() {
        this.lineViewRight.setBackgroundColor(getResources().getColor(C0181R.color.textcolor_grey));
        this.lineViewLeft.setBackgroundColor(Common.getThemeColor(this));
        switch (Common.getThemeType()) {
            case 1:
                this.btnBack.setBackground(getResources().getDrawable(C0181R.drawable.btn_back));
                return;
            case 2:
                this.btnBack.setBackground(getResources().getDrawable(C0181R.drawable.btn_back_green));
                return;
            case 4:
                this.btnBack.setBackground(getResources().getDrawable(C0181R.drawable.btn_back_black));
                return;
            default:
                this.btnBack.setBackground(getResources().getDrawable(C0181R.drawable.btn_back_blue));
                return;
        }
    }

    @AfterViews
    void init() {
        initThemeView();
        this.member = new MemberDAL(this).SelectByClientId(this.ClientId);
        if (this.member != null) {
            this.mGestureDetector = new GestureDetector(this.onGestureListener);
            this.leftInAnimation = AnimationUtils.loadAnimation(this, C0181R.anim.in_left);
            this.leftOutAnimation = AnimationUtils.loadAnimation(this, C0181R.anim.out_left);
            this.rightInAnimation = AnimationUtils.loadAnimation(this, C0181R.anim.in_right);
            this.rightOutAnimation = AnimationUtils.loadAnimation(this, C0181R.anim.out_right);
            this.adultChartView = View_DataChart_Adult_.build((Context) this, this.member);
            this.adultListView = View_DataList_Adult_.build(this, this.member, this);
            this.adultListView.setGestureDetector(this.mGestureDetector);
            this.viewFlipper.addView(this.adultChartView);
            this.viewFlipper.addView(this.adultListView);
            this.viewFlipper.setOnTouchListener(new C01432());
            initLanguage();
        }
    }

    public void initLanguage() {
        if (getSharedPreferences("AppConfigDAL", 0).getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0) == 17) {
            this.adultChartView.reloadLayoutSize();
        }
        System.out.printf("tag", new Object[]{String.valueOf(LanguageVal)});
    }

    void showNextPage() {
        if (this.ViewIndex != 1) {
            this.ViewIndex = 1;
            this.tvTitle.setText(getString(C0181R.string.List));
            this.lineViewRight.setBackgroundColor(Common.getThemeColor(this));
            this.lineViewLeft.setBackgroundColor(getResources().getColor(C0181R.color.textcolor_grey));
            this.viewFlipper.setInAnimation(this.leftInAnimation);
            this.viewFlipper.setOutAnimation(this.leftOutAnimation);
            this.viewFlipper.showNext();
            loadListData();
        }
    }

    void showPreviousPage() {
        if (this.ViewIndex != 0) {
            this.ViewIndex = 0;
            this.tvTitle.setText(getString(C0181R.string.Chart));
            this.lineViewRight.setBackgroundColor(getResources().getColor(C0181R.color.textcolor_grey));
            this.lineViewLeft.setBackgroundColor(Common.getThemeColor(this));
            this.viewFlipper.setInAnimation(this.rightInAnimation);
            this.viewFlipper.setOutAnimation(this.rightOutAnimation);
            this.viewFlipper.showPrevious();
        }
    }

    public void prePager() {
        if (this.ViewIndex != 0) {
            showPreviousPage();
        } else if (this.onParentViewChangeListener != null) {
            this.onParentViewChangeListener.onPreviou();
        }
    }
}
