package com.yohealth.api.btscale;

import android.app.AlertDialog.Builder;
import android.content.Context;

public class DialogHelper {
    public static void showMsgDialog(Context mContext, String msg) {
        new Builder(mContext).setTitle("提示").setMessage(msg).setPositiveButton("确定", null).show();
    }
}
