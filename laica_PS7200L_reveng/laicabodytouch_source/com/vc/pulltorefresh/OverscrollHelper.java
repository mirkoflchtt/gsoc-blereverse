package com.vc.pulltorefresh;

import android.annotation.TargetApi;
import android.view.View;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;

@TargetApi(9)
final class OverscrollHelper {
    static final float DEFAULT_OVERSCROLL_SCALE = 1.0f;
    static final String LOG_TAG = "OverscrollHelper";

    OverscrollHelper() {
    }

    static void overScrollBy(PullToRefreshBase<?> view, int deltaY, int scrollY, boolean isTouchEvent) {
        overScrollBy(view, deltaY, scrollY, 0, isTouchEvent);
    }

    static void overScrollBy(PullToRefreshBase<?> view, int deltaY, int scrollY, int scrollRange, boolean isTouchEvent) {
        overScrollBy(view, deltaY, scrollY, scrollRange, 0, DEFAULT_OVERSCROLL_SCALE, isTouchEvent);
    }

    static void overScrollBy(PullToRefreshBase<?> view, int deltaY, int scrollY, int scrollRange, int fuzzyThreshold, float scaleFactor, boolean isTouchEvent) {
        if (view.isPullToRefreshOverScrollEnabled()) {
            Mode mode = view.getMode();
            if (mode != Mode.DISABLED && !isTouchEvent) {
                int newY = deltaY + scrollY;
                if (newY < 0 - fuzzyThreshold) {
                    if (mode.canPullDown()) {
                        view.setHeaderScroll((int) (((float) (view.getScrollY() + newY)) * scaleFactor));
                    }
                } else if (newY > scrollRange + fuzzyThreshold) {
                    if (mode.canPullUp()) {
                        view.setHeaderScroll((int) (((float) ((view.getScrollY() + newY) - scrollRange)) * scaleFactor));
                    }
                } else if (Math.abs(newY) <= fuzzyThreshold || Math.abs(newY - scrollRange) <= fuzzyThreshold) {
                    view.smoothScrollTo(0, 325);
                }
            }
        }
    }

    static boolean isAndroidOverScrollEnabled(View view) {
        return view.getOverScrollMode() != 2;
    }
}
