package com.vc.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileHelper {
    public static String encodeBase64File(String path) {
        try {
            File file = new File(path);
            byte[] buffer = new byte[(((int) file.length()) + 100)];
            return Base64.encodeToString(buffer, 0, new FileInputStream(file).read(buffer), 0);
        } catch (Exception e) {
            return null;
        }
    }

    private static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) {
            baos.reset();
            image.compress(CompressFormat.JPEG, options, baos);
            options -= 10;
        }
        return BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, null);
    }

    private static Bitmap getimage(String srcPath) {
        Options newOpts = new Options();
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        int be = 1;
        if (w > h && ((float) w) > 480.0f) {
            be = (int) (((float) newOpts.outWidth) / 480.0f);
        } else if (w < h && ((float) h) > 800.0f) {
            be = (int) (((float) newOpts.outHeight) / 800.0f);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        return compressImage(BitmapFactory.decodeFile(srcPath, newOpts));
    }

    public static String encodeBase64FileScale(String path) {
        try {
            return bitmapToBase64(getimage(path));
        } catch (Exception e) {
            return null;
        }
    }

    public static String bitmapToBase64(Bitmap bitmap) {
        IOException e;
        Throwable th;
        String result = null;
        ByteArrayOutputStream baos = null;
        if (bitmap != null) {
            try {
                ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                try {
                    bitmap.compress(CompressFormat.JPEG, 100, baos2);
                    baos2.flush();
                    baos2.close();
                    result = Base64.encodeToString(baos2.toByteArray(), 0);
                    baos = baos2;
                } catch (IOException e2) {
                    e = e2;
                    baos = baos2;
                    try {
                        e.printStackTrace();
                        if (baos != null) {
                            try {
                                baos.flush();
                                baos.close();
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                        }
                        return result;
                    } catch (Throwable th2) {
                        th = th2;
                        if (baos != null) {
                            try {
                                baos.flush();
                                baos.close();
                            } catch (IOException e32) {
                                e32.printStackTrace();
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    baos = baos2;
                    if (baos != null) {
                        baos.flush();
                        baos.close();
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e32 = e4;
                e32.printStackTrace();
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
                return result;
            }
        }
        if (baos != null) {
            try {
                baos.flush();
                baos.close();
            } catch (IOException e322) {
                e322.printStackTrace();
            }
        }
        return result;
    }

    public static byte[] InputStreamToByte(InputStream is) throws IOException {
        ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
        while (true) {
            int ch = is.read();
            if (ch == -1) {
                byte[] imgdata = bytestream.toByteArray();
                bytestream.close();
                return imgdata;
            }
            bytestream.write(ch);
        }
    }

    public static byte[] encodeByteFile(String path) {
        try {
            File file = new File(path);
            FileInputStream stream = new FileInputStream(file);
            byte[] buffer = new byte[(((int) file.length()) + 100)];
            int length = stream.read(buffer);
            stream.close();
            return buffer;
        } catch (Exception e) {
            return null;
        }
    }

    public static String BytetoStreamString(byte[] bytes) {
        try {
            return Base64.encodeToString(bytes, 0, bytes.length, 0);
        } catch (Exception e) {
            return "";
        }
    }
}
