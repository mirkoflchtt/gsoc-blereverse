package com.vc.cloudbalance.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import com.whb.loease.activity.MainActivity_;
import java.util.LinkedList;
import java.util.List;

public class ApplicationMamager {
    private static ApplicationMamager instance;
    private List<Activity> mList = new LinkedList();

    private ApplicationMamager() {
    }

    public static synchronized ApplicationMamager getInstance() {
        ApplicationMamager applicationMamager;
        synchronized (ApplicationMamager.class) {
            if (instance == null) {
                instance = new ApplicationMamager();
            }
            applicationMamager = instance;
        }
        return applicationMamager;
    }

    public void addActivity(Activity activity) {
        this.mList.add(activity);
    }

    public void exit() {
        try {
            if (this.mList != null) {
                for (Activity activity : this.mList) {
                    if (activity != null) {
                        activity.finish();
                    }
                }
                Process.killProcess(Process.myPid());
                System.exit(0);
            }
        } catch (Exception e) {
        }
    }

    public void goMainActivity(Context mContext) {
        Intent intent = MainActivity_.intent(mContext).get();
        intent.setFlags(67108864);
        mContext.startActivity(intent);
    }

    public void clearActivity() {
        int i = 0;
        while (i < this.mList.size()) {
            String a = ((Activity) this.mList.get(i)).getLocalClassName().toLowerCase();
            if (!(this.mList.get(i) == null || ((Activity) this.mList.get(i)).getLocalClassName().toLowerCase().equals("mainactivity_"))) {
                ((Activity) this.mList.get(i)).finish();
            }
            i++;
        }
        System.gc();
    }

    public void quit() {
        for (int i = this.mList.size() - 1; i > 0; i--) {
            if (this.mList.get(i) != null) {
                ((Activity) this.mList.get(i)).finish();
                this.mList.remove(i);
            }
        }
    }
}
