package com.vc.cloudbalance.common;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.widget.DatePicker;
import com.whb.loease.bodytouch.C0181R;

public class DatePickerDialogCustom extends DatePickerDialog {
    public DatePickerDialogCustom(Context context, OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, listener, year, monthOfYear, dayOfMonth);
        setButton(-1, getContext().getString(C0181R.string.confirm), this);
        setButton(-2, null, this);
        getDatePicker().setCalendarViewShown(false);
        setTitle(new StringBuilder(String.valueOf(year)).append("-").append(monthOfYear + 1).append("-").append(dayOfMonth).toString());
    }

    public void onDateChanged(DatePicker view, int year, int month, int day) {
        super.onDateChanged(view, year, month, day);
        setTitle(new StringBuilder(String.valueOf(year)).append("-").append(month + 1).append("-").append(day).toString());
    }
}
