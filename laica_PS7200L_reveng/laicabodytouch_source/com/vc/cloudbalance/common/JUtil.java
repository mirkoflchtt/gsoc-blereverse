package com.vc.cloudbalance.common;

import android.util.Log;
import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JUtil {
    public static boolean GetJsonStatu(JSONObject jsonObject) {
        try {
            if (jsonObject.getString("status").equalsIgnoreCase("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean GetJsonStatu(JSONArray jsonArray) {
        try {
            if (jsonArray.getJSONObject(1).getJSONObject("status").getString("mark").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static String GetErrorString(JSONObject jsonObject) {
        try {
            return jsonObject.getString("msg");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String GetNormalString(JSONObject jsonObject, String key) {
        try {
            return jsonObject.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String GetJsonString(String val) {
        return val.toLowerCase().equals("null") ? "" : val;
    }

    public static String GetString(JSONObject jsonObject, String key) {
        try {
            return GetJsonString(jsonObject.getJSONObject("data").getString(key));
        } catch (JSONException e) {
            return "";
        }
    }

    public static String GetString(Map<String, String> map, String key) {
        try {
            return GetJsonString((String) map.get(key));
        } catch (Exception e) {
            return "";
        }
    }

    public static Date GetDateDefault(JSONObject jsonObject, String key, String type, Locale locale) {
        try {
            String dateString = GetString(jsonObject, key);
            if (dateString == null || dateString.trim().equals("") || dateString.trim().equals("null")) {
                return null;
            }
            DateFormat df = new SimpleDateFormat(type, locale);
            Date date = new Date();
            try {
                return df.parse(dateString);
            } catch (ParseException e) {
                Log.e("StringToDate", new StringBuilder(String.valueOf(dateString)).append("    ").append(e).toString());
                e.printStackTrace();
                return date;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public static JSONObject GetJsonObject(JSONObject jsonObject, String key) {
        try {
            return new JSONObject(GetString(jsonObject, key));
        } catch (Exception e) {
            return null;
        }
    }

    public static String object2String(Object obj) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            String serStr = URLEncoder.encode(byteArrayOutputStream.toString("ISO-8859-1"), "UTF-8");
            objectOutputStream.close();
            byteArrayOutputStream.close();
            return serStr;
        } catch (Exception e) {
            return "";
        }
    }

    public static Object getObjectFromString(String serStr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(URLDecoder.decode(serStr, "UTF-8").getBytes("ISO-8859-1"));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            Object object = objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean GetJsonStatu(String json) {
        try {
            if (new JSONObject(json).getString("status").equalsIgnoreCase("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static <T> T fromJson(String json, Type typeOfT) {
        try {
            return fromJson(new JSONObject(json), typeOfT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T fromJson1(JSONObject json, Type typeOfT) {
        Gson gson = new Gson();
        String jsonString = "";
        try {
            jsonString = GetString(json, "boardarray");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gson.fromJson(jsonString, typeOfT);
    }

    public static <T> T fromJson(JSONObject json, Type typeOfT) {
        Gson gson = new Gson();
        String jsonString = "";
        try {
            jsonString = json.getString("data");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gson.fromJson(jsonString, typeOfT);
    }

    public static <T> T fromJson(JSONObject json, Class<T> classOfT) {
        Gson gson = new Gson();
        String jsonString = "";
        try {
            jsonString = json.getString("data");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gson.fromJson(jsonString, (Class) classOfT);
    }

    public static String toJson(Object obj) {
        String jsonString = "";
        try {
            jsonString = new Gson().toJson(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return new Gson().fromJson(json, (Class) classOfT);
    }

    public static <T> T simpleFromJson(String json, Type typeOfT) {
        return new Gson().fromJson(json, typeOfT);
    }
}
