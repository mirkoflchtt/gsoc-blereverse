package com.vc.cloudbalance.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CrashHandler implements UncaughtExceptionHandler {
    public static final String TAG = "CrashHandler";
    private static CrashHandler instance;
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    private Map<String, String> infos = new HashMap();
    private Context mContext;
    private UncaughtExceptionHandler mDefaultHandler;

    class C00881 extends Thread {
        C00881() {
        }

        public void run() {
            Looper.prepare();
            Looper.loop();
        }
    }

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        if (instance == null) {
            instance = new CrashHandler();
        }
        return instance;
    }

    public void init(Context context) {
        this.mContext = context;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.mDefaultHandler == null) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Log.e(TAG, "error : ", e);
            }
            ApplicationMamager.getInstance().exit();
            return;
        }
        this.mDefaultHandler.uncaughtException(thread, ex);
    }

    private boolean handleException(Throwable ex) {
        if (ex == null) {
            return false;
        }
        collectDeviceInfo(this.mContext);
        new C00881().start();
        saveCatchInfo2File(ex);
        return true;
    }

    public void collectDeviceInfo(Context ctx) {
        try {
            PackageInfo pi = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 1);
            if (pi != null) {
                String versionName = pi.versionName == null ? "null" : pi.versionName;
                String versionCode = new StringBuilder(String.valueOf(pi.versionCode)).toString();
                this.infos.put("versionName", versionName);
                this.infos.put("versionCode", versionCode);
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, "an error occured when collect package info", e);
        }
        for (Field field : Build.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                this.infos.put(field.getName(), field.get(null).toString());
                Log.d(TAG, field.getName() + " : " + field.get(null));
            } catch (Exception e2) {
                Log.e(TAG, "an error occured when collect crash info", e2);
            }
        }
    }

    private String saveCatchInfo2File(Throwable ex) {
        StringBuffer sb = new StringBuffer();
        for (Entry<String, String> entry : this.infos.entrySet()) {
            String value = (String) entry.getValue();
            sb.append(new StringBuilder(String.valueOf((String) entry.getKey())).append("=").append(value).append("\n").toString());
        }
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        printWriter.close();
        sb.append(writer.toString());
        try {
            String fileName = "crash-" + this.formatter.format(new Date()) + "-" + System.currentTimeMillis() + ".log";
            if (!Environment.getExternalStorageState().equals("mounted")) {
                return fileName;
            }
            String path = Environment.getExternalStorageDirectory() + "/.LOEASE/.bodytouch/log/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(new StringBuilder(String.valueOf(path)).append(fileName).toString());
            fos.write(sb.toString().getBytes());
            sendCrashLog2PM(new StringBuilder(String.valueOf(path)).append(fileName).toString());
            fos.close();
            return fileName;
        } catch (Exception e) {
            Log.e(TAG, "an error occured while writing file...", e);
            return null;
        }
    }

    private void sendCrashLog2PM(String fileName) {
        IOException e;
        FileNotFoundException e2;
        Throwable th;
        if (new File(fileName).exists()) {
            FileInputStream fis = null;
            BufferedReader reader = null;
            try {
                FileInputStream fis2 = new FileInputStream(fileName);
                try {
                    BufferedReader reader2 = new BufferedReader(new InputStreamReader(fis2, "GBK"));
                    while (true) {
                        try {
                            String s = reader2.readLine();
                            if (s == null) {
                                try {
                                    reader2.close();
                                    fis2.close();
                                    reader = reader2;
                                    fis = fis2;
                                    return;
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                    reader = reader2;
                                    fis = fis2;
                                    return;
                                }
                            }
                            Log.i("info", s.toString());
                        } catch (FileNotFoundException e4) {
                            e2 = e4;
                            reader = reader2;
                            fis = fis2;
                        } catch (IOException e5) {
                            e3 = e5;
                            reader = reader2;
                            fis = fis2;
                        } catch (Throwable th2) {
                            th = th2;
                            reader = reader2;
                            fis = fis2;
                        }
                    }
                } catch (FileNotFoundException e6) {
                    e2 = e6;
                    fis = fis2;
                    try {
                        e2.printStackTrace();
                        try {
                            reader.close();
                            fis.close();
                        } catch (IOException e32) {
                            e32.printStackTrace();
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        try {
                            reader.close();
                            fis.close();
                        } catch (IOException e322) {
                            e322.printStackTrace();
                        }
                        throw th;
                    }
                } catch (IOException e7) {
                    e322 = e7;
                    fis = fis2;
                    e322.printStackTrace();
                    try {
                        reader.close();
                        fis.close();
                    } catch (IOException e3222) {
                        e3222.printStackTrace();
                    }
                } catch (Throwable th4) {
                    th = th4;
                    fis = fis2;
                    reader.close();
                    fis.close();
                    throw th;
                }
            } catch (FileNotFoundException e8) {
                e2 = e8;
                e2.printStackTrace();
                reader.close();
                fis.close();
            } catch (IOException e9) {
                e3222 = e9;
                e3222.printStackTrace();
                reader.close();
                fis.close();
            }
        }
    }
}
