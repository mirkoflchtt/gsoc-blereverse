package com.vc.cloudbalance.common;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.gson.reflect.TypeToken;
import com.vc.cloudbalance.model.ActionMDL;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberDataCountMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.ActionDAL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.cloudbalance.webservice.BalanceDataWS;
import com.vc.cloudbalance.webservice.MemberWS;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class Common {

    class C00821 extends Thread {
        private final /* synthetic */ Context val$mContext;

        C00821(Context context) {
            this.val$mContext = context;
        }

        public void run() {
            Common.AddMember(this.val$mContext);
            super.run();
        }
    }

    class C00832 extends Thread {
        private final /* synthetic */ Context val$mContext;
        private final /* synthetic */ MemberMDL val$member;

        C00832(MemberMDL memberMDL, Context context) {
            this.val$member = memberMDL;
            this.val$mContext = context;
        }

        public void run() {
            Common.DelMember(this.val$member, this.val$mContext);
            super.run();
        }
    }

    class C00843 extends Thread {
        private final /* synthetic */ Context val$mContext;
        private final /* synthetic */ MemberMDL val$member;

        C00843(MemberMDL memberMDL, Context context) {
            this.val$member = memberMDL;
            this.val$mContext = context;
        }

        public void run() {
            Common.UpdateMember(this.val$member, this.val$mContext);
            super.run();
        }
    }

    class C00854 extends Thread {
        private final /* synthetic */ Context val$mContext;

        C00854(Context context) {
            this.val$mContext = context;
        }

        public void run() {
            Common.AddBalanceData(this.val$mContext);
            super.run();
        }
    }

    class C00865 extends Thread {
        private final /* synthetic */ Context val$mContext;

        C00865(Context context) {
            this.val$mContext = context;
        }

        public void run() {
            Common.SynBalanceData(this.val$mContext);
            super.run();
        }
    }

    class C00878 extends Thread {
        private final /* synthetic */ Context val$mContext;

        C00878(Context context) {
            this.val$mContext = context;
        }

        public void run() {
            Common.AddBalanceData(this.val$mContext);
            super.run();
        }
    }

    static class mp3Task extends AsyncTask<String, Object, Object> {
        Context mContext;

        public mp3Task(Context context) {
            this.mContext = context;
        }

        protected Object doInBackground(String... params) {
            String filePath = App.packageName;
            String fileName = App.packageName + "/shake_match.mp3";
            File mfile = new File(fileName);
            if (!mfile.exists()) {
                InputStream isInputStream = this.mContext.getResources().openRawResource(C0181R.raw.shake_match);
                try {
                    File file = new File(filePath);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    FileOutputStream fos = new FileOutputStream(fileName);
                    byte[] buffer = new byte[8192];
                    while (true) {
                        int count = isInputStream.read(buffer);
                        if (count <= 0) {
                            break;
                        }
                        fos.write(buffer, 0, count);
                    }
                    fos.close();
                    isInputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return Boolean.valueOf(false);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return Boolean.valueOf(false);
                }
            }
            if (!mfile.exists()) {
                return Boolean.valueOf(true);
            }
            MediaPlayer mp = new MediaPlayer();
            try {
                mp.setDataSource(fileName);
                mp.prepare();
                mp.start();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (SecurityException e4) {
                e4.printStackTrace();
            } catch (IllegalStateException e5) {
                e5.printStackTrace();
            } catch (IOException e6) {
                e6.printStackTrace();
            }
            return null;
        }
    }

    class C02616 extends TypeToken<List<MemberDataCountMDL>> {
        C02616() {
        }
    }

    class C02627 extends TypeToken<List<BalanceDataMDL>> {
        C02627() {
        }
    }

    public static int GetAgeByBirthday(String birthdayStr) {
        Date birthday = ObjectHelper.Convert2Date(birthdayStr, "yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthday)) {
            throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(1);
        int monthNow = cal.get(2) + 1;
        int dayOfMonthNow = cal.get(5);
        cal.setTime(birthday);
        int yearBirth = cal.get(1);
        int monthBirth = cal.get(2) + 1;
        int dayOfMonthBirth = cal.get(5);
        int age = yearNow - yearBirth;
        if (monthNow > monthBirth) {
            return age;
        }
        if (monthNow != monthBirth) {
            return age - 1;
        }
        if (dayOfMonthNow < dayOfMonthBirth) {
            return age - 1;
        }
        return age;
    }

    public static String GetDeviceId(Context mContext) {
        TelephonyManager tm = (TelephonyManager) mContext.getApplicationContext().getSystemService("phone");
        String imei = tm.getDeviceId() == null ? "" : tm.getDeviceId();
        if (imei.equals("")) {
            imei = getMAC();
        }
        if (imei.equals("")) {
            return Secure.getString(mContext.getApplicationContext().getContentResolver(), "android_id");
        }
        return imei;
    }

    public static String getMAC() {
        String macSerial = null;
        String str = "";
        try {
            LineNumberReader input = new LineNumberReader(new InputStreamReader(Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address ").getInputStream()));
            while (str != null) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();
                    break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return macSerial;
    }

    public static String[] GetLanguageList() {
        return new String[]{"中文", "English"};
    }

    public static String[] getLanguageShortCodes() {
        return new String[]{"CN", "TW", "en", "da", "es", "ko", "ja", "it", "pt", "hu", "hr", "fr", "de", "el", "ro", "nl", "vn"};
    }

    public static String[] GetHeightList() {
        List<String> heights = new LinkedList();
        String[] arrayString = new String[heights.size()];
        for (int i = 10; i <= 220; i++) {
            heights.add(new StringBuilder(String.valueOf(i)).toString());
        }
        return (String[]) heights.toArray(arrayString);
    }

    public static void AddMemberThread(Context mContext) {
        new C00821(mContext).start();
    }

    public static boolean AddMember(Context mContext) {
        if (App.getApp(mContext).getUser() != null) {
            MemberDAL memberDal = new MemberDAL(mContext);
            ActionDAL actionDAL = new ActionDAL(mContext);
            List<ActionMDL> actions = actionDAL.SelectByAction(ActionMDL.AddMember);
            if (actions != null && actions.size() > 0) {
                for (ActionMDL actionMDL : actions) {
                    actionDAL.Locked(actionMDL.getId(), 1);
                    MemberMDL memberMDL = memberDal.SelectByClientId(actionMDL.getData());
                    if (memberMDL == null || !TextUtils.isEmpty(memberMDL.getMemberid())) {
                        actionDAL.Del(actionMDL.getId());
                    } else {
                        JSONObject jsonObject = new MemberWS(mContext).insertMember(App.getApp(mContext).getUser().getUserid(), memberMDL);
                        if (JUtil.GetJsonStatu(jsonObject)) {
                            String memberid = JUtil.GetString(jsonObject, "memberid");
                            memberMDL.setMemberid(memberid);
                            jsonObject = new MemberWS(mContext).updateMemberIcon(memberMDL);
                            new MemberDAL(mContext).UpdateUploadByMemberId(memberMDL.getClientid(), memberid);
                            actionDAL.Del(actionMDL.getId());
                        } else {
                            actionDAL.Locked(actionMDL.getId(), 0);
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void DelMemberThread(MemberMDL member, Context mContext) {
        new C00832(member, mContext).start();
    }

    public static boolean DelMember(MemberMDL member, Context mContext) {
        if (!TextUtils.isEmpty(member.getMemberid())) {
            ActionDAL actionDAL = new ActionDAL(mContext);
            ActionMDL actionMDL = actionDAL.Select(ActionMDL.DelMember, member.getMemberid());
            actionDAL.Locked(actionMDL.getId(), 1);
            if (JUtil.GetJsonStatu(new MemberWS(mContext).deleteMember(member.getUserid(), member))) {
                actionDAL.Del(actionMDL.getId());
            } else {
                actionDAL.Locked(actionMDL.getId(), 0);
            }
        }
        return false;
    }

    public static void UpdateMemberThread(MemberMDL member, Context mContext) {
        new C00843(member, mContext).start();
    }

    public static boolean UpdateMember(MemberMDL member, Context mContext) {
        if (!TextUtils.isEmpty(member.getClientid())) {
            ActionDAL actionDAL = new ActionDAL(mContext);
            ActionMDL actionMDL = actionDAL.Select(ActionMDL.UpdateMember, member.getClientid());
            actionDAL.Locked(actionMDL.getId(), 1);
            if (JUtil.GetJsonStatu(new MemberWS(mContext).updateMember(member.getUserid(), member))) {
                if (member.getClientImg() != null && member.getClientImg().length > 0) {
                    JSONObject jsonObject = new MemberWS(mContext).updateMemberIcon(member);
                }
                actionDAL.Del(actionMDL.getId());
            } else {
                actionDAL.Locked(actionMDL.getId(), 0);
            }
        }
        return false;
    }

    public static void AddBalanceDataThread(Context mContext) {
        new C00854(mContext).start();
    }

    public static boolean AddBalanceData(Context mContext) {
        if (App.getApp(mContext).getUser() != null) {
            BalanceDataDAL dal = new BalanceDataDAL(mContext);
            for (BalanceDataMDL mdl : dal.SelectUnUploadData()) {
                if (!(mdl == null || TextUtils.isEmpty(mdl.getMemberid()) || !JUtil.GetJsonStatu(new BalanceDataWS(mContext).insertBalanceData(mdl)))) {
                    dal.UpdateUnloadData(mdl);
                }
            }
        }
        return false;
    }

    public static void SynBalanceDataThread(Context mContext) {
        new C00865(mContext).start();
    }

    public static boolean SynBalanceData(Context mContext) {
        if (App.getApp(mContext).getUser() != null) {
            String userid = App.getApp(mContext).getUser().getUserid();
            List<MemberDataCountMDL> datas = (List) JUtil.fromJson(new BalanceDataWS(mContext).getMamberdataCount(userid), new C02616().getType());
            if (datas != null && datas.size() > 0) {
                BalanceDataDAL dal = new BalanceDataDAL(mContext);
                int i = 0;
                while (i < datas.size()) {
                    MemberMDL member = new MemberDAL(mContext).SelectById(((MemberDataCountMDL) datas.get(i)).getMemberid());
                    if (!(member == null || dal.SelectCountById(member.getMemberid()) == ObjectHelper.Convert2Int(((MemberDataCountMDL) datas.get(i)).getRecordcount()))) {
                        List<BalanceDataMDL> balanceDataMDLs = (List) JUtil.fromJson(new BalanceDataWS(mContext).getMamberdata(userid, member.getMemberid()), new C02627().getType());
                        if (balanceDataMDLs != null) {
                            dal.Insert(member, balanceDataMDLs);
                        }
                    }
                    i++;
                }
            }
        }
        return false;
    }

    public static void DelBalanceDataThread(Context mContext) {
        new C00878(mContext).start();
    }

    public static boolean DelBalanceData(Context mContext) {
        if (App.getApp(mContext).getUser() != null) {
            BalanceDataDAL dal = new BalanceDataDAL(mContext);
            ActionDAL actionDAL = new ActionDAL(mContext);
            List<ActionMDL> actions = actionDAL.SelectByAction(ActionMDL.DelBalanceData);
            if (actions != null && actions.size() > 0) {
                for (ActionMDL actionMDL : actions) {
                    actionDAL.Locked(actionMDL.getId(), 1);
                    BalanceDataMDL mdl = dal.SelectById(actionMDL.getData());
                    if (mdl == null || TextUtils.isEmpty(mdl.getMemberid())) {
                        actionDAL.Del(actionMDL.getId());
                    } else if (JUtil.GetJsonStatu(new BalanceDataWS(mContext).insertBalanceData(mdl))) {
                        actionDAL.Del(actionMDL.getId());
                    } else {
                        actionDAL.Locked(actionMDL.getId(), 0);
                    }
                }
            }
        }
        return false;
    }

    public static boolean checkNetWork(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService("connectivity");
        if (cm == null) {
            return false;
        }
        NetworkInfo[] infos = cm.getAllNetworkInfo();
        if (infos == null) {
            return false;
        }
        for (NetworkInfo ni : infos) {
            if (ni.isConnected()) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkMobileNO(String mobiles) {
        return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$").matcher(mobiles).matches();
    }

    public static boolean checkEmail(String email) {
        return Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$").matcher(email).matches();
    }

    public static String GetBluetoothValIndex(String str, int i) {
        try {
            return str.substring(i * 2, (i + 1) * 2);
        } catch (Exception e) {
            return "";
        }
    }

    public static Typeface GetFont(Context mContext) {
        return Typeface.createFromAsset(mContext.getAssets(), "yohealth_Th.otf");
    }

    public static void playMp3(Context mContext) {
        new mp3Task(mContext).execute(new String[]{""});
    }

    public static int getThemeType() {
        return ObjectHelper.Convert2Int(new AppConfigDAL(App.getInstance()).select(Constants.THEME_COLOR_STRING));
    }

    public static int getThemeColor(Context context) {
        int temp = getThemeType();
        if (temp == 1) {
            try {
                return context.getResources().getColor(C0181R.color.themecolor_orange);
            } catch (Exception e) {
                return 0;
            }
        } else if (temp == 2) {
            return context.getResources().getColor(C0181R.color.themecolor_green);
        } else {
            if (temp == 4) {
                return context.getResources().getColor(C0181R.color.themecolor_grey);
            }
            return context.getResources().getColor(C0181R.color.themecolor_blue);
        }
    }

    public static int getThemeLightColor(Context context) {
        int temp = getThemeType();
        if (temp == 1) {
            try {
                return context.getResources().getColor(C0181R.color.themecolor_lightorange);
            } catch (Exception e) {
                return 0;
            }
        } else if (temp == 2) {
            return context.getResources().getColor(C0181R.color.themecolor_lightgreen);
        } else {
            if (temp == 4) {
                return context.getResources().getColor(C0181R.color.themecolor_lightgrey);
            }
            return context.getResources().getColor(C0181R.color.themecolor_lightblue);
        }
    }
}
