package com.vc.cloudbalance.common;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.vc.cloudbalance.model.ActionMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.ActionDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;

public class AppService extends Service {
    private static final String ACTION_START = (CLIENT_ID + ".START");
    private static final String ACTION_STOP = (CLIENT_ID + ".STOP");
    public static String CLIENT_ID = "loease";
    public static final String TAG = "DemoPushService";
    private boolean isDo = true;

    class C00801 implements Runnable {
        C00801() {
        }

        public void run() {
            while (AppService.this.isDo) {
                try {
                    MemberMDL member;
                    Common.AddMember(AppService.this.getApplicationContext());
                    for (ActionMDL actionMDL : new ActionDAL(AppService.this.getApplicationContext()).SelectByAction(ActionMDL.DelMember)) {
                        member = new MemberDAL(AppService.this.getApplicationContext()).SelectById(actionMDL.getData());
                        if (member != null) {
                            Common.DelMember(member, AppService.this.getApplicationContext());
                        }
                    }
                    for (ActionMDL actionMDL2 : new ActionDAL(AppService.this.getApplicationContext()).SelectByAction(ActionMDL.UpdateMember)) {
                        member = new MemberDAL(AppService.this.getApplicationContext()).SelectById(actionMDL2.getData());
                        if (member != null) {
                            Common.UpdateMember(member, AppService.this.getApplicationContext());
                        }
                    }
                    Common.AddBalanceData(AppService.this.getApplicationContext());
                } catch (Exception e) {
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx, AppService.class);
        i.setAction(ACTION_START);
        ctx.startService(i);
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, AppService.class);
        i.setAction(ACTION_STOP);
        ctx.startService(i);
    }

    public void onCreate() {
        super.onCreate();
        log("Creating service");
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        log("Service started with intent=" + intent);
        if (intent == null) {
            log("onStart() intent is null");
            return;
        }
        log("onStart() intent is null" + intent.getAction());
        if (intent.getAction().equals(ACTION_STOP)) {
            stop();
            stopSelf();
        } else if (intent.getAction().equals(ACTION_START)) {
            start();
        }
    }

    private void log(String message) {
        log(message, null);
    }

    private void log(String message, Throwable e) {
        if (e != null) {
            Log.e(TAG, message, e);
        } else {
            Log.i(TAG, message);
        }
    }

    private synchronized void start() {
        log("Starting service...");
        new Thread(new C00801()).start();
    }

    private synchronized void stop() {
        this.isDo = false;
    }
}
