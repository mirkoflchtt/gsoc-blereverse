package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.DatabaseHelper;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.util.LogUtils;
import java.util.LinkedList;
import java.util.List;

public class MemberDAL {
    Context context;
    SQLiteDatabase mDb = null;
    DatabaseHelper mDbHelper = null;
    String selectPara = "memberid , membername,iconfile,birthday ,height ,waist ,sex ,targetweight ,modeltype ,upload,userid,clientid,clientImg,targetStartTime,targetFinishTime,mood ";

    public MemberDAL(Context c) {
        this.context = c;
        this.mDbHelper = DatabaseHelper.getInstance(c);
        this.mDb = this.mDbHelper.getDatabase();
    }

    public MemberMDL SelectLastMember() {
        try {
            MemberMDL datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from Member ", new String[0]);
                datas = null;
                if (cursor.moveToLast()) {
                    datas = convert(cursor);
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public MemberMDL SelectById(String memberid) {
        try {
            MemberMDL datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from Member  where memberid=? ";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{memberid});
                datas = null;
                if (cursor.moveToNext()) {
                    datas = convert(cursor);
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public MemberMDL SelectByClientId(String clientid) {
        try {
            MemberMDL datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from Member  where clientid=? ";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{clientid});
                datas = null;
                if (cursor.moveToNext()) {
                    datas = convert(cursor);
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MemberMDL> Select(String userid) {
        try {
            List<MemberMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from Member  where userid=? ";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{userid});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    MemberMDL i = convert(cursor);
                    if (i != null) {
                        datas.add(i);
                    }
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MemberMDL> SelectGuest() {
        try {
            List<MemberMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from Member  where userid is null or userid='' ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    MemberMDL i = convert(cursor);
                    if (i != null) {
                        datas.add(i);
                    }
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MemberMDL> SelectUnUpload() {
        try {
            List<MemberMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from Member  where upload=0 ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    MemberMDL i = convert(cursor);
                    if (i != null) {
                        datas.add(i);
                    }
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<MemberMDL> SelectNotAdult() {
        try {
            List<MemberMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from Member  where modeltype=1 or modeltype=2 ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    MemberMDL i = convert(cursor);
                    if (i != null) {
                        datas.add(i);
                    }
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    private MemberMDL convert(Cursor cursor) {
        MemberMDL i = new MemberMDL();
        try {
            i.setMemberid(cursor.getString(0));
            i.setMembername(cursor.getString(1));
            i.setIconfile(cursor.getString(2));
            i.setBirthday(cursor.getString(3));
            i.setHeight(cursor.getString(4));
            i.setWaist(cursor.getString(5));
            i.setSex(cursor.getString(6));
            i.setTargetweight(cursor.getString(7));
            i.setModeltype(cursor.getString(8));
            i.setUpload(cursor.getInt(9));
            i.setUserid(cursor.getString(10));
            i.setClientid(cursor.getString(11));
            i.setClientImg(cursor.getBlob(12));
            i.setTargetStartTime(cursor.getString(13));
            i.setTargetFinishTime(cursor.getString(14));
            i.setMood(cursor.getString(15));
            return i;
        } catch (Exception e) {
            LogUtils.m3e("convert MemberMDL" + e.toString());
            return null;
        }
    }

    public boolean Insert(MemberMDL data) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "insert into Member (" + this.selectPara + ") values (? , ? ,? ,? ,? ,? ,? ,? ,? ,?,?,?,?,?,?,?)";
                try {
                    this.mDb.execSQL(sql, new Object[]{data.getMemberid(), data.getMembername(), data.getIconfile(), data.getBirthday(), data.getHeight(), data.getWaist(), data.getSex(), data.getTargetweight(), data.getModeltype(), Integer.valueOf(data.getUpload()), data.getUserid(), data.getClientid(), data.getClientImg(), data.getTargetStartTime(), data.getTargetFinishTime(), data.getMood()});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean DelByMemberId(String memberid) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "delete from Member where memberid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{memberid});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean DelByClientId(String clientId) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "delete from Member where clientid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{clientId});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean Insert(List<MemberMDL> members) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "insert into Member (" + this.selectPara + ") values (? , ? ,? ,? ,? ,? ,? ,? ,? ,?,?,?,?,?,?,?)";
                try {
                    this.mDb.beginTransaction();
                    this.mDb.execSQL("delete from Member ");
                    for (MemberMDL data : members) {
                        this.mDb.execSQL(sql, new Object[]{data.getMemberid(), data.getMembername(), data.getIconfile(), data.getBirthday(), data.getHeight(), data.getWaist(), data.getSex(), data.getTargetweight(), data.getModeltype(), Integer.valueOf(data.getUpload()), data.getUserid(), data.getClientid(), data.getClientImg(), data.getTargetStartTime(), data.getTargetFinishTime(), data.getMood()});
                    }
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean Update(MemberMDL data) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "update Member set membername=?,iconfile=? , birthday=?, height=?,waist=?,sex=?,targetweight=?,modeltype=?,upload=?,userid=?,clientImg=?,targetStartTime=?,targetFinishTime=?,mood=? where clientid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{data.getMembername(), data.getIconfile(), data.getBirthday(), data.getHeight(), data.getWaist(), data.getSex(), data.getTargetweight(), data.getModeltype(), Integer.valueOf(data.getUpload()), data.getUserid(), data.getClientImg(), data.getTargetStartTime(), data.getTargetFinishTime(), data.getMood(), data.getClientid()});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean UpdateUploadByMemberId(String memberid) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "update Member set  upload=1  where memberid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{memberid});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean UpdateImage(MemberMDL mdl) {
        try {
            synchronized (App.threadDBLock) {
                this.mDb.beginTransaction();
                try {
                    if (!TextUtils.isEmpty(mdl.getClientid())) {
                        this.mDb.execSQL("update Member set  clientImg=? where clientid=?", new Object[]{mdl.getClientImg(), mdl.getClientid()});
                    } else if (!TextUtils.isEmpty(mdl.getMemberid())) {
                        this.mDb.execSQL("update Member set  clientImg=? where memberid=?", new Object[]{mdl.getClientImg(), mdl.getMemberid()});
                    }
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean UpdateUploadByMemberId(String clientid, String memberid) {
        try {
            synchronized (App.threadDBLock) {
                this.mDb.beginTransaction();
                String sql = "update Member set  upload=1,memberid=?  where clientid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{memberid, clientid});
                    this.mDb.execSQL("update BalanceData set memberid=?  where clientmemberid=?", new Object[]{memberid, clientid});
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean UpdateUploadByClientId(String memberid, String clientid) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "update Member set upload=1 ,memberid =? where clientid=?";
                try {
                    this.mDb.execSQL(sql, new Object[]{memberid, clientid});
                } catch (Exception e) {
                    LogUtils.m3e(e.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }
}
