package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.DatabaseHelper;
import com.vc.util.LogUtils;

public class AppConfigDAL {
    SQLiteDatabase mDb = null;
    DatabaseHelper mDbHelper = null;

    public AppConfigDAL(Context c) {
        this.mDbHelper = DatabaseHelper.getInstance(c);
        this.mDb = this.mDbHelper.getDatabase();
    }

    public String select(String key) {
        String result = "";
        try {
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select [Value] from AppConfig where KeyName =?", new String[]{key});
                if (cursor.moveToFirst()) {
                    result = cursor.getString(0);
                }
            }
            return result;
        } catch (Exception e) {
            return "";
        }
    }

    public boolean insert(String key, String val) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "delete from AppConfig where KeyName =?";
                try {
                    this.mDb.execSQL(sql, new Object[]{key});
                    sql = "insert into  AppConfig (KeyName,[Value]) values (?,?)";
                    try {
                        this.mDb.execSQL(sql, new Object[]{key, val});
                    } catch (Exception e) {
                        LogUtils.m3e(e.toString());
                        return false;
                    }
                } catch (Exception e2) {
                    LogUtils.m3e(e2.toString());
                    return false;
                }
            }
            return true;
        } catch (Exception e22) {
            LogUtils.m3e(e22.toString());
            return false;
        }
    }
}
