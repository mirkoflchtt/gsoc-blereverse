package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.model.LanguageMDL;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class LanguageDAL {
    Context context;
    String filePath = (App.packageName + "/language.db");
    SQLiteDatabase mDb = null;
    String pathStr = App.packageName;

    public SQLiteDatabase openDatabase(Context context) {
        System.out.println("filePath:" + this.filePath);
        File jhPath = new File(this.filePath);
        if (jhPath.exists()) {
            return SQLiteDatabase.openOrCreateDatabase(jhPath, null);
        }
        File path = new File(this.pathStr);
        if (!path.exists()) {
            if (path.mkdir()) {
                System.out.println("创建成功");
            } else {
                System.out.println("创建失败");
            }
        }
        try {
            InputStream is = context.getAssets().open("language.db");
            FileOutputStream fos = new FileOutputStream(jhPath);
            byte[] buffer = new byte[1024];
            while (true) {
                int count = is.read(buffer);
                if (count <= 0) {
                    fos.flush();
                    fos.close();
                    is.close();
                    return openDatabase(context);
                }
                fos.write(buffer, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LanguageDAL(Context c) {
        this.context = c;
        this.mDb = openDatabase(this.context);
    }

    public HashMap<String, LanguageMDL> select() {
        String result = "";
        try {
            HashMap<String, LanguageMDL> languages;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select key,lan1,lan2 from language  ", new String[0]);
                languages = new HashMap();
                while (cursor.moveToNext()) {
                    languages.put(cursor.getString(0), new LanguageMDL(cursor.getString(0), cursor.getString(1), cursor.getString(2)));
                }
                cursor.close();
                this.mDb.close();
            }
            return languages;
        } catch (Exception e) {
            if (this.mDb != null) {
                this.mDb.close();
            }
            return null;
        }
    }
}
