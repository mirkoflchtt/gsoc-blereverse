package com.vc.cloudbalance.model;

public class MemberDataCountMDL {
    private String memberid;
    private String recordcount;

    public String getMemberid() {
        return this.memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getRecordcount() {
        return this.recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }
}
