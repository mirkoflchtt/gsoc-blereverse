package com.vc.cloudbalance.model;

public class ModeTypeEnum {
    public static final String Adult = "1";
    public static final String Baby = "3";
    public static final String Children = "2";
    public static final String HighPrecisionDevice = "2";
    public static final String LowPrecisionDevice = "1";
    public static final int ThemeGreen = 2;
    public static final int ThemeGrey = 4;
    public static final int ThemeOrange = 1;
    public static final int Themeblue = 3;
}
