package com.vc.cloudbalance.model;

public class AppVerMDL {
    private String fileid;
    private String fileurl;
    private String name;
    private String verno;

    public String getFileid() {
        return this.fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerno() {
        return this.verno;
    }

    public void setVerno(String verno) {
        this.verno = verno;
    }

    public String getFileurl() {
        return this.fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }
}
