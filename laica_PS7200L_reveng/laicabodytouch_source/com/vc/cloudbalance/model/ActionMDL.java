package com.vc.cloudbalance.model;

public class ActionMDL {
    public static final String AddBalanceData = "AddBalanceData";
    public static final String AddMember = "AddMember";
    public static final String DelBalanceData = "DelBalanceData";
    public static final String DelMember = "DelMember";
    public static final String UpdateMember = "UpdateMember";
    private String action;
    private String data;
    private String id;
    private int locked;

    public int getLocked() {
        return this.locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public ActionMDL(String arg1, String arg2, String arg3) {
        setId(arg1);
        setAction(arg2);
        setData(arg3);
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
