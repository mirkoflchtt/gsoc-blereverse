package com.vc.cloudbalance.model;

public class LanguageMDL {
    private String Val1;
    private String Val2;
    private String key;

    public LanguageMDL(String k, String v1, String v2) {
        this.key = k;
        this.Val1 = v1;
        this.Val2 = v2;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVal1() {
        return this.Val1;
    }

    public void setVal1(String val1) {
        this.Val1 = val1;
    }

    public String getVal2() {
        return this.Val2;
    }

    public void setVal2(String val2) {
        this.Val2 = val2;
    }

    public String GetVal(String l) {
        if (l.equals("1")) {
            return getVal1();
        }
        if (l.equals("2")) {
            return getVal2();
        }
        return getVal1();
    }
}
