package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_Themes_ extends View_Themes implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01231 implements OnClickListener {
        C01231() {
        }

        public void onClick(View view) {
            View_Themes_.this.select4();
        }
    }

    class C01242 implements OnClickListener {
        C01242() {
        }

        public void onClick(View view) {
            View_Themes_.this.confirm();
        }
    }

    class C01253 implements OnClickListener {
        C01253() {
        }

        public void onClick(View view) {
            View_Themes_.this.select2();
        }
    }

    class C01264 implements OnClickListener {
        C01264() {
        }

        public void onClick(View view) {
            View_Themes_.this.select3();
        }
    }

    class C01275 implements OnClickListener {
        C01275() {
        }

        public void onClick(View view) {
            View_Themes_.this.select1();
        }
    }

    public View_Themes_(Context context, PopupWindow popupWindow) {
        super(context, popupWindow);
        init_();
    }

    public View_Themes_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_Themes build(Context context, PopupWindow popupWindow) {
        View_Themes_ instance = new View_Themes_(context, popupWindow);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_theme, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_Themes build(Context context, AttributeSet attrs) {
        View_Themes_ instance = new View_Themes_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.imgOrangeThemeSelected = (ImageView) hasViews.findViewById(C0181R.id.imgOrangeThemeSelected);
        this.imgGreyThemeSelected = (ImageView) hasViews.findViewById(C0181R.id.imgGreyThemeSelected);
        this.btnConfirm = (Button) hasViews.findViewById(C0181R.id.btnConfirm);
        this.imgBlueThemeSelected = (ImageView) hasViews.findViewById(C0181R.id.imgBlueThemeSelected);
        this.llGreenTheme = (LinearLayout) hasViews.findViewById(C0181R.id.llGreenTheme);
        this.imgGreenThemeSelected = (ImageView) hasViews.findViewById(C0181R.id.imgGreenThemeSelected);
        View view = hasViews.findViewById(C0181R.id.llBlueTheme);
        if (view != null) {
            view.setOnClickListener(new C01231());
        }
        if (this.btnConfirm != null) {
            this.btnConfirm.setOnClickListener(new C01242());
        }
        view = hasViews.findViewById(C0181R.id.llGreyTheme);
        if (view != null) {
            view.setOnClickListener(new C01253());
        }
        view = hasViews.findViewById(C0181R.id.llOrangeTheme);
        if (view != null) {
            view.setOnClickListener(new C01264());
        }
        if (this.llGreenTheme != null) {
            this.llGreenTheme.setOnClickListener(new C01275());
        }
        init();
    }
}
