package com.vc.cloudbalance.widget;

import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.DatePickerDialogCustom;
import com.vc.cloudbalance.common.DialogHelper;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.util.ObjectHelper;
import com.whb.loease.activity.TargetViewActivity;
import com.whb.loease.bodytouch.C0181R;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup
public class ViewSetTarget extends LinearLayout {
    DatePickerDialog dialog;
    private boolean isHighPrecision = false;
    Context mContext;
    MemberMDL member;
    private OnFinishListener onFinishListener;
    @ViewById
    TextView tvCurrentWtVal;
    @ViewById
    TextView tvFinishTime;
    @ViewById
    TextView tvStartTime;
    @ViewById
    EditText tvTargetWtVal;
    @ViewById
    TextView tvWtUnit1;
    @ViewById
    TextView tvWtUnit2;

    public interface OnFinishListener {
        void onFinish();
    }

    public ViewSetTarget(Context context, MemberMDL member) {
        super(context);
        this.mContext = context;
        this.member = member;
        initThemeView();
    }

    public ViewSetTarget(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                inflate(getContext(), C0181R.layout.view_settarget, this);
                return;
            case 2:
                inflate(getContext(), C0181R.layout.view_settarget_greentheme, this);
                return;
            case 4:
                inflate(getContext(), C0181R.layout.view_settarget_greytheme, this);
                return;
            default:
                inflate(getContext(), C0181R.layout.view_settarget_bluetheme, this);
                return;
        }
    }

    @Click({2131493100})
    void setStartTime() {
        showDate(WeightUnitHelper.Kg);
    }

    @Click({2131493101})
    void setFinishTime() {
        showDate("1");
    }

    @AfterViews
    void init() {
        if (new AppConfigDAL(this.mContext).select(Constants.DEVICETYPE_STRING).equals("2")) {
            this.isHighPrecision = true;
        }
        setText();
    }

    @Click({2131493034})
    public void saveTarget() {
        String startTime = (String) this.tvStartTime.getText();
        String finishTime = (String) this.tvFinishTime.getText();
        String targetWeight = WeightUnitHelper.convert2KgVal(2, this.tvTargetWtVal.getText().toString());
        if (((TextUtils.isEmpty(startTime) & TextUtils.isEmpty(finishTime)) & TextUtils.isEmpty(targetWeight)) == 0) {
            int startToFinishDays = 0;
            try {
                startToFinishDays = ObjectHelper.daysBetween(startTime, finishTime);
                if (ObjectHelper.GetReciprocalDays(finishTime) < 0) {
                    DialogHelper.showTost(this.mContext, this.mContext.getString(C0181R.string.timeEarlierToday));
                    return;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (startToFinishDays < 0) {
                DialogHelper.showTost(this.mContext, this.mContext.getString(C0181R.string.setTimeErr));
                return;
            }
            this.member.setTargetStartTime(startTime);
            this.member.setTargetFinishTime(finishTime);
            this.member.setTargetweight(targetWeight);
            if (new MemberDAL(this.mContext).Update(this.member)) {
                ((TargetViewActivity) this.mContext).setData();
                this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, targetWeight));
                this.tvTargetWtVal.setSelection(this.tvTargetWtVal.getText().length());
                if (this.onFinishListener != null) {
                    this.onFinishListener.onFinish();
                }
            }
        }
    }

    private void setText() {
        this.tvWtUnit1.setText(WeightUnitHelper.getWeightUnitString());
        this.tvWtUnit2.setText(WeightUnitHelper.getWeightUnitString());
        BalanceDataMDL lastData = new BalanceDataDAL(this.mContext).SelectLastData(this.member.getMemberid(), this.member.getClientid());
        if (lastData != null) {
            this.tvCurrentWtVal.setText(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 2, lastData.getWeight()));
        }
        this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, this.member.getTargetweight()));
        this.tvStartTime.setText(this.member.getTargetStartTime());
        this.tvFinishTime.setText(this.member.getTargetFinishTime());
    }

    private void showDate(final String type) {
        String text = "";
        if (type.equals(WeightUnitHelper.Kg)) {
            text = (String) this.tvStartTime.getText();
        } else if (type.equals("1")) {
            text = (String) this.tvFinishTime.getText();
        }
        Calendar cal = Calendar.getInstance();
        int year = cal.get(1);
        int month = cal.get(2);
        int day = cal.get(5);
        if (!TextUtils.isEmpty(text)) {
            Date date = ObjectHelper.Convert2Date(text, "yyyy-MM-dd");
            cal.setTime(date);
            if (date != null) {
                year = cal.get(1);
                month = cal.get(2);
                day = cal.get(5);
            }
        }
        this.dialog = new DatePickerDialogCustom(this.mContext, new OnDateSetListener() {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
                String nowmonth;
                String nowdayOfMonth;
                if (month + 1 < 10) {
                    nowmonth = new StringBuilder(WeightUnitHelper.Kg).append(month + 1).toString();
                } else {
                    nowmonth = new StringBuilder(String.valueOf(month + 1)).toString();
                }
                if (dayOfMonth < 10) {
                    nowdayOfMonth = new StringBuilder(WeightUnitHelper.Kg).append(dayOfMonth).toString();
                } else {
                    nowdayOfMonth = new StringBuilder(String.valueOf(dayOfMonth)).toString();
                }
                String selectDateTime = new StringBuilder(String.valueOf(year)).append("-").append(nowmonth).append("-").append(nowdayOfMonth).toString();
                if (type.equals(WeightUnitHelper.Kg)) {
                    ViewSetTarget.this.tvStartTime.setText(selectDateTime);
                } else if (type.equals("1")) {
                    ViewSetTarget.this.tvFinishTime.setText(selectDateTime);
                }
            }
        }, year, month, day);
        this.dialog.show();
    }

    private void showTargetWeight() {
        final NumberPicker np = new NumberPicker(this.mContext);
        np.setMaxValue(ObjectHelper.Convert2Int(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 0, Integer.valueOf(180))));
        np.setMinValue(ObjectHelper.Convert2Int(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 0, Integer.valueOf(5))));
        int val = ObjectHelper.Convert2Int(this.tvTargetWtVal.getText().toString());
        if (val <= 0) {
            val = ObjectHelper.Convert2Int(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 0, Integer.valueOf(55)));
        }
        np.setValue(val);
        new Builder(this.mContext).setTitle(new StringBuilder(String.valueOf(this.mContext.getString(C0181R.string.target_weight))).append("(").append(WeightUnitHelper.getWeightUnitString()).append(")").toString()).setView(np).setPositiveButton(this.mContext.getString(C0181R.string.confirm), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ViewSetTarget.this.tvTargetWtVal.setText(new StringBuilder(String.valueOf(np.getValue())).toString());
                ViewSetTarget.this.tvTargetWtVal.setTag(WeightUnitHelper.convert2KgVal(0, Integer.valueOf(np.getValue())));
                dialog.dismiss();
            }
        }).setNegativeButton(this.mContext.getString(C0181R.string.cancle), null).show();
    }

    public void setOnFinishListener(OnFinishListener onFinishListener) {
        this.onFinishListener = onFinishListener;
    }
}
