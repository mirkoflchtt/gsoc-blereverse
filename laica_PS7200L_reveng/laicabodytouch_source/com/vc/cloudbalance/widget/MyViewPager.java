package com.vc.cloudbalance.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {
    private float lastX;
    private boolean slidingLeft;
    private boolean slidingRight;

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyViewPager(Context context) {
        super(context);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                getParent().requestDisallowInterceptTouchEvent(true);
                this.lastX = ev.getX();
                break;
            case 1:
                getParent().requestDisallowInterceptTouchEvent(false);
                this.lastX = ev.getX();
                this.slidingLeft = false;
                this.slidingRight = false;
                break;
            case 2:
                if (getCurrentItem() != 0) {
                    if (getCurrentItem() == getAdapter().getCount() - 1) {
                        if (this.lastX >= ev.getX() && !this.slidingLeft) {
                            getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                        }
                        this.slidingLeft = true;
                        this.lastX = ev.getX();
                        getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    }
                } else if (this.lastX <= ev.getX() && !this.slidingRight) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                } else {
                    this.slidingRight = true;
                    this.lastX = ev.getX();
                    getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                }
                break;
        }
        super.onTouchEvent(ev);
        return true;
    }
}
