package com.vc.cloudbalance.widget;

import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableRow;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.DatePickerDialogCustom;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import java.util.Calendar;
import java.util.Date;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903072)
public class ViewGuestMemberInfo extends LinearLayout {
    private String Gender = "1";
    @ViewById
    Button btnCancel;
    @ViewById
    Button btnConfirm;
    @ViewById
    EditText etAge;
    @ViewById
    EditText etHeight;
    @ViewById
    EditText etSex;
    boolean isLoad = false;
    @ViewById
    View line1;
    @ViewById
    View line2;
    @ViewById
    View line3;
    private Context mContext;
    private OnFinishListener mOnFinishListener;
    private PopupWindow mPopupWindow;
    private MemberMDL member = new MemberMDL();
    @ViewById
    RadioButton rbGenderFeman;
    @ViewById
    RadioButton rbGenderMan;
    @ViewById
    RadioGroup rbgGender;
    @ViewById
    TableRow tbAge;
    @ViewById
    TableRow tbGender;
    @ViewById
    TableRow tbHeight;

    class C00951 implements OnTouchListener {
        C00951() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                ViewGuestMemberInfo.this.showDate();
            }
            return true;
        }
    }

    class C00962 implements OnTouchListener {
        C00962() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                ViewGuestMemberInfo.this.showHeight();
            }
            return true;
        }
    }

    class C00973 implements OnCheckedChangeListener {
        C00973() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == C0181R.id.rbGenderFeman) {
                ViewGuestMemberInfo.this.Gender = WeightUnitHelper.Kg;
                ViewGuestMemberInfo.this.rbGenderFeman.setBackgroundColor(Common.getThemeColor(ViewGuestMemberInfo.this.mContext));
                ViewGuestMemberInfo.this.rbGenderMan.setBackground(null);
            } else if (checkedId == C0181R.id.rbGenderMan) {
                ViewGuestMemberInfo.this.Gender = "1";
                ViewGuestMemberInfo.this.rbGenderMan.setBackgroundColor(Common.getThemeColor(ViewGuestMemberInfo.this.mContext));
                ViewGuestMemberInfo.this.rbGenderFeman.setBackground(null);
            }
        }
    }

    class C00995 implements OnDateSetListener {
        C00995() {
        }

        public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
            String nowmonth;
            String nowdayOfMonth;
            if (month + 1 < 10) {
                nowmonth = new StringBuilder(WeightUnitHelper.Kg).append(month + 1).toString();
            } else {
                nowmonth = new StringBuilder(String.valueOf(month + 1)).toString();
            }
            if (dayOfMonth < 10) {
                nowdayOfMonth = new StringBuilder(WeightUnitHelper.Kg).append(dayOfMonth).toString();
            } else {
                nowdayOfMonth = new StringBuilder(String.valueOf(dayOfMonth)).toString();
            }
            String selectDateTime = new StringBuilder(String.valueOf(year)).append("-").append(nowmonth).append("-").append(nowdayOfMonth).toString();
            ViewGuestMemberInfo.this.etAge.setTag(selectDateTime);
            ViewGuestMemberInfo.this.etAge.setText(new StringBuilder(String.valueOf(Common.GetAgeByBirthday(selectDateTime))).toString());
        }
    }

    public interface OnFinishListener {
        void onConfirm(MemberMDL memberMDL);
    }

    public ViewGuestMemberInfo(Context context, PopupWindow popupWindow, MemberMDL member) {
        super(context);
        this.mContext = context;
        this.mPopupWindow = popupWindow;
        this.member = member;
    }

    public ViewGuestMemberInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    private void initThemeView() {
        this.rbGenderMan.setBackgroundColor(Common.getThemeColor(this.mContext));
        this.line1.setBackgroundColor(Common.getThemeColor(this.mContext));
        this.line2.setBackgroundColor(Common.getThemeColor(this.mContext));
        this.line3.setBackgroundColor(Common.getThemeColor(this.mContext));
        switch (Common.getThemeType()) {
            case 1:
                this.tbAge.setBackgroundResource(C0181R.drawable.rectangle_orangeborder);
                this.tbGender.setBackgroundResource(C0181R.drawable.rectangle_orangeborder);
                this.tbHeight.setBackgroundResource(C0181R.drawable.rectangle_orangeborder);
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_orange_selector);
                this.btnCancel.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_orange_selector);
                this.btnConfirm.setTextColor(C0181R.drawable.colorselector_orange_2_white);
                this.btnCancel.setTextColor(C0181R.drawable.colorselector_orange_2_white);
                return;
            case 2:
                this.tbAge.setBackgroundResource(C0181R.drawable.rectangle_greenborder);
                this.tbGender.setBackgroundResource(C0181R.drawable.rectangle_greenborder);
                this.tbHeight.setBackgroundResource(C0181R.drawable.rectangle_greenborder);
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_green_selector);
                this.btnCancel.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_green_selector);
                this.btnConfirm.setTextColor(C0181R.drawable.colorselector_green_2_white);
                this.btnCancel.setTextColor(C0181R.drawable.colorselector_green_2_white);
                return;
            case 4:
                this.tbAge.setBackgroundResource(C0181R.drawable.rectangle_grayborder);
                this.tbGender.setBackgroundResource(C0181R.drawable.rectangle_grayborder);
                this.tbHeight.setBackgroundResource(C0181R.drawable.rectangle_grayborder);
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_grey_selector);
                this.btnCancel.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_grey_selector);
                this.btnConfirm.setTextColor(C0181R.drawable.colorselector_grey_2_white);
                this.btnCancel.setTextColor(C0181R.drawable.colorselector_grey_2_white);
                return;
            default:
                this.tbAge.setBackgroundResource(C0181R.drawable.rectangle_blueborder);
                this.tbGender.setBackgroundResource(C0181R.drawable.rectangle_blueborder);
                this.tbHeight.setBackgroundResource(C0181R.drawable.rectangle_blueborder);
                this.btnConfirm.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_blue_selector);
                this.btnCancel.setBackgroundResource(C0181R.drawable.btn_bgwhite_2_blue_selector);
                this.btnConfirm.setTextColor(C0181R.drawable.colorselector_blue_2_white);
                this.btnCancel.setTextColor(C0181R.drawable.colorselector_blue_2_white);
                return;
        }
    }

    @AfterViews
    void init() {
        initThemeView();
        setData();
        this.etAge.setOnTouchListener(new C00951());
        this.etHeight.setOnTouchListener(new C00962());
        this.rbgGender.setOnCheckedChangeListener(new C00973());
    }

    public void setData() {
        this.etAge.setTag(this.member.getBirthday());
        if (!TextUtils.isEmpty(this.member.getBirthday())) {
            this.etAge.setText(new StringBuilder(String.valueOf(Common.GetAgeByBirthday(this.member.getBirthday()))).toString());
        }
        this.etHeight.setText(this.member.getHeight());
        this.Gender = this.member.getSex();
        if (this.Gender.equals(WeightUnitHelper.Kg)) {
            this.rbGenderFeman.setChecked(true);
            this.rbGenderFeman.setBackgroundColor(Common.getThemeColor(this.mContext));
            this.rbGenderMan.setBackground(null);
        } else if (this.Gender.equals("1")) {
            this.rbGenderMan.setChecked(true);
            this.rbGenderMan.setBackgroundColor(Common.getThemeColor(this.mContext));
            this.rbGenderFeman.setBackground(null);
        }
    }

    public void showHeight() {
        final NumberPicker np = new NumberPicker(this.mContext);
        np.setMaxValue(230);
        np.setMinValue(10);
        int val = ObjectHelper.Convert2Int(this.etHeight.getText().toString());
        if (val <= 0) {
            val = 160;
        }
        np.setValue(val);
        new Builder(this.mContext).setTitle(new StringBuilder(String.valueOf(this.mContext.getString(C0181R.string.height))).append("(cm)").toString()).setView(np).setPositiveButton(this.mContext.getString(C0181R.string.confirm), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int height = ObjectHelper.Convert2Int(Integer.valueOf(np.getValue()));
                if (height < 10 || height > 230) {
                    ViewGuestMemberInfo.this.showHeight();
                    return;
                }
                ViewGuestMemberInfo.this.etHeight.setText(new StringBuilder(String.valueOf(np.getValue())).toString());
                dialog.dismiss();
            }
        }).setNegativeButton(this.mContext.getString(C0181R.string.cancle), null).show();
    }

    @Click({2131493033})
    void cancel() {
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    @Click({2131493034})
    void save() {
        doSave();
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    private void doSave() {
        String Birthday = (String) this.etAge.getTag();
        String Height = this.etHeight.getText().toString();
        this.member.setGuest(true);
        this.member.setBirthday(Birthday);
        this.member.setHeight(Height);
        this.member.setSex(this.Gender);
        if (this.mOnFinishListener != null) {
            this.mOnFinishListener.onConfirm(this.member);
        }
    }

    private void showDate() {
        Dialog dialog = new Dialog(this.mContext);
        Calendar c = Calendar.getInstance();
        String text = (String) this.etAge.getTag();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(1);
        int month = cal.get(2);
        int day = cal.get(5);
        if (!TextUtils.isEmpty(text)) {
            Date date = ObjectHelper.Convert2Date(text, "yyyy-MM-dd");
            cal.setTime(date);
            if (date != null) {
                year = cal.get(1);
                month = cal.get(2);
                day = cal.get(5);
            }
        }
        new DatePickerDialogCustom(this.mContext, new C00995(), year, month, day).show();
    }

    public void setOnFinishListener(OnFinishListener mOnFinishListener) {
        this.mOnFinishListener = mOnFinishListener;
    }
}
