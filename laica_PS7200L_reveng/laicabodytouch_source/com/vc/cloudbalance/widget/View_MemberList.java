package com.vc.cloudbalance.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Scroller;
import android.widget.TextView;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.whb.loease.activity.MemberInfoActivity_;
import com.whb.loease.bodytouch.C0181R;
import java.util.List;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903075)
public class View_MemberList extends LinearLayout {
    View SolidDot;
    boolean isLoad;
    @ViewById
    LinearLayout llMemberList;
    @ViewById
    LinearLayout llNotMemberViews;
    private Context mContext;
    public OnClickMemberListener mOnClickMemberListener = new C02701();
    private Scroller mScroller;
    List<MemberMDL> members;
    int offset_x = 0;
    @ViewById
    RelativeLayout rlGuestMode;
    MemberMDL thisMember;
    @ViewById
    TextView tvName;

    public interface OnClickMemberListener {
        void memberSelected(MemberMDL memberMDL);
    }

    class C02701 implements OnClickMemberListener {
        C02701() {
        }

        public void memberSelected(MemberMDL member) {
            View_MemberList.this.thisMember = member;
        }
    }

    public View_MemberList(Context context) {
        super(context);
        this.mContext = context;
    }

    public View_MemberList(Context context, MemberMDL member) {
        super(context);
        this.mContext = context;
        this.thisMember = member;
    }

    public View_MemberList(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setOnClickMemberListener(OnClickMemberListener mOnClickMemberListener) {
        this.mOnClickMemberListener = mOnClickMemberListener;
    }

    @Click({2131493096})
    void showSettingActivity() {
        this.mContext.startActivity(MemberInfoActivity_.intent(this.mContext).get());
    }

    @Click({2131493094})
    void enterGuestMode() {
        if (!this.thisMember.isGuest()) {
            drawGuestModeSolidDot();
            this.thisMember = App.guestMember();
        }
        this.mOnClickMemberListener.memberSelected(this.thisMember);
    }

    @AfterViews
    void init() {
    }

    public void removeNotMemberViews() {
        this.llNotMemberViews.setVisibility(8);
    }

    public MemberMDL getMember() {
        return this.thisMember;
    }

    public void setMember(MemberMDL member) {
        this.thisMember = member;
    }

    public void loadAllMembers() {
        if (App.getApp(this.mContext).getUser() != null) {
            this.members = new MemberDAL(this.mContext).Select(App.getApp(this.mContext).getUser().getUserid());
        } else {
            this.members = new MemberDAL(this.mContext).SelectGuest();
        }
        this.llMemberList.removeAllViews();
        if (this.members != null && this.members.size() > 0) {
            int i = 0;
            while (i < this.members.size()) {
                View_Item_Member v = View_Item_Member_.build(this.mContext, (MemberMDL) this.members.get(i));
                v.setOnClickMemberListener(this.mOnClickMemberListener);
                if (this.thisMember != null && ((MemberMDL) this.members.get(i)).getClientid().equals(this.thisMember.getClientid())) {
                    v.drawSolidDot();
                }
                this.llMemberList.addView(v);
                i++;
            }
        }
        if (this.thisMember.isGuest()) {
            drawGuestModeSolidDot();
        } else if (this.rlGuestMode.getChildCount() >= 2) {
            this.rlGuestMode.removeViews(1, this.rlGuestMode.getChildCount() - 1);
        }
    }

    public void loadChildMembers() {
        this.members = new MemberDAL(this.mContext).SelectNotAdult();
        this.llMemberList.removeAllViews();
        if (this.members != null && this.members.size() > 0) {
            int i = 0;
            while (i < this.members.size()) {
                View_Item_Member v = View_Item_Member_.build(this.mContext, (MemberMDL) this.members.get(i));
                v.setOnClickMemberListener(this.mOnClickMemberListener);
                if (this.thisMember != null && ((MemberMDL) this.members.get(i)).getClientid().equals(this.thisMember.getClientid())) {
                    v.drawSolidDot();
                }
                if (i == this.members.size() - 1) {
                    v.findViewById(C0181R.id.LineView).setVisibility(8);
                }
                this.llMemberList.addView(v);
                i++;
            }
        }
    }

    void drawGuestModeSolidDot() {
        this.SolidDot = new View(this.mContext) {
            protected void onDraw(Canvas canvas) {
                Paint p = new Paint();
                p.setColor(Common.getThemeColor(View_MemberList.this.mContext));
                canvas.drawCircle(10.0f, 10.0f, 10.0f, p);
                p.setAntiAlias(true);
                super.onDraw(canvas);
            }
        };
        LayoutParams lParams = new LayoutParams(30, 30);
        lParams.setMargins(50, 0, 0, 0);
        lParams.addRule(5);
        lParams.addRule(15, -1);
        this.SolidDot.setLayoutParams(lParams);
        this.rlGuestMode.addView(this.SolidDot);
    }
}
