package com.vc.cloudbalance.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.DateHelper;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup
public class View_DataChart_Adult extends LinearLayout {
    public static final int BMI = 6;
    public static final int BMR = 8;
    public static final int Bone = 4;
    public static final int Fat = 2;
    public static final int Height = 7;
    public static final int Month = 2;
    public static final int Muscle = 3;
    public static final int Season = 3;
    public static final int Water = 5;
    public static final int Week = 1;
    public static final int Weight = 1;
    public static final int Year = 4;
    List<BalanceDataMDL> balanceDatas;
    @ViewById
    Button btnNext;
    @ViewById
    Button btnPre;
    BalanceChartView chartView;
    BalanceDataDAL dal;
    Date lastdataDate = new Date();
    @ViewById
    RelativeLayout llPanel;
    Context mContext;
    MemberMDL member;
    int pageIndex = 0;
    @ViewById
    RadioButton rbBMI;
    @ViewById
    RadioButton rbBMR;
    @ViewById
    RadioButton rbBone;
    OnClickListener rbClickListener = new C01071();
    @ViewById
    RadioButton rbFat;
    @ViewById
    RadioButton rbMonth;
    @ViewById
    RadioButton rbMuscle;
    @ViewById
    RadioButton rbSeason;
    @ViewById
    RadioButton rbWater;
    @ViewById
    RadioButton rbWeek;
    @ViewById
    RadioButton rbWeight;
    @ViewById
    RadioButton rbYear;
    @ViewById
    TextView tvChartTip;
    @ViewById
    TextView tvChartTip2;

    class C01071 implements OnClickListener {
        C01071() {
        }

        public void onClick(View v) {
            View_DataChart_Adult.this.pageIndex = 0;
            if (v.getId() == C0181R.id.rbWeek || v.getId() == C0181R.id.rbMonth || v.getId() == C0181R.id.rbSeason || v.getId() == C0181R.id.rbYear) {
                View_DataChart_Adult.this.rbWeek.setChecked(false);
                View_DataChart_Adult.this.rbMonth.setChecked(false);
                View_DataChart_Adult.this.rbSeason.setChecked(false);
                View_DataChart_Adult.this.rbYear.setChecked(false);
                if (v.getId() == C0181R.id.rbWeek) {
                    View_DataChart_Adult.this.rbWeek.setChecked(true);
                } else if (v.getId() == C0181R.id.rbMonth) {
                    View_DataChart_Adult.this.rbMonth.setChecked(true);
                } else if (v.getId() == C0181R.id.rbSeason) {
                    View_DataChart_Adult.this.rbSeason.setChecked(true);
                } else if (v.getId() == C0181R.id.rbYear) {
                    View_DataChart_Adult.this.rbYear.setChecked(true);
                }
                View_DataChart_Adult.this.refreshChart();
            } else if (v.getId() == C0181R.id.rbWeight || v.getId() == C0181R.id.rbFat || v.getId() == C0181R.id.rbBMI || v.getId() == C0181R.id.rbMuscle || v.getId() == C0181R.id.rbBone || v.getId() == C0181R.id.rbWater || v.getId() == C0181R.id.rbBMR) {
                View_DataChart_Adult.this.rbWeight.setChecked(false);
                View_DataChart_Adult.this.rbFat.setChecked(false);
                View_DataChart_Adult.this.rbBMI.setChecked(false);
                View_DataChart_Adult.this.rbMuscle.setChecked(false);
                View_DataChart_Adult.this.rbBone.setChecked(false);
                View_DataChart_Adult.this.rbWater.setChecked(false);
                View_DataChart_Adult.this.rbBMR.setChecked(false);
                if (v.getId() == C0181R.id.rbWeight) {
                    View_DataChart_Adult.this.rbWeight.setChecked(true);
                } else if (v.getId() == C0181R.id.rbFat) {
                    View_DataChart_Adult.this.rbFat.setChecked(true);
                } else if (v.getId() == C0181R.id.rbBMI) {
                    View_DataChart_Adult.this.rbBMI.setChecked(true);
                } else if (v.getId() == C0181R.id.rbMuscle) {
                    View_DataChart_Adult.this.rbMuscle.setChecked(true);
                } else if (v.getId() == C0181R.id.rbBone) {
                    View_DataChart_Adult.this.rbBone.setChecked(true);
                } else if (v.getId() == C0181R.id.rbWater) {
                    View_DataChart_Adult.this.rbWater.setChecked(true);
                } else if (v.getId() == C0181R.id.rbBMR) {
                    View_DataChart_Adult.this.rbBMR.setChecked(true);
                }
                View_DataChart_Adult.this.refreshChart();
            }
        }
    }

    public View_DataChart_Adult(Context context, MemberMDL member) {
        super(context);
        this.mContext = context;
        this.member = member;
        initThemeView();
    }

    public View_DataChart_Adult(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                inflate(getContext(), C0181R.layout.view_datachart_adult, this);
                return;
            case 2:
                inflate(getContext(), C0181R.layout.view_datachart_adult_greentheme, this);
                return;
            case 4:
                inflate(getContext(), C0181R.layout.view_datachart_adult_greytheme, this);
                return;
            default:
                inflate(getContext(), C0181R.layout.view_datachart_adult_bluetheme, this);
                return;
        }
    }

    @AfterViews
    void init() {
        if (getResources().getConfiguration().locale.getLanguage().endsWith("ja")) {
            this.rbSeason.setTextSize(1, 12.0f);
        }
        this.dal = new BalanceDataDAL(this.mContext);
        this.chartView = new BalanceChartView(this.mContext);
        this.chartView.setLayoutParams(new LayoutParams(-1, -1));
        this.llPanel.addView(this.chartView);
        if (TextUtils.isEmpty(this.member.getMemberid())) {
            this.balanceDatas = new BalanceDataDAL(this.mContext).SelectByClientMemberId(this.member.getClientid());
        } else {
            this.balanceDatas = new BalanceDataDAL(this.mContext).SelectByMemberId(this.member.getMemberid());
        }
        if (this.balanceDatas.size() != 0) {
            this.lastdataDate = ((BalanceDataMDL) this.balanceDatas.get(0)).getWeidate();
        }
        this.rbWeek.setOnClickListener(this.rbClickListener);
        this.rbMonth.setOnClickListener(this.rbClickListener);
        this.rbSeason.setOnClickListener(this.rbClickListener);
        this.rbYear.setOnClickListener(this.rbClickListener);
        this.rbWeight.setOnClickListener(this.rbClickListener);
        this.rbFat.setOnClickListener(this.rbClickListener);
        this.rbBMI.setOnClickListener(this.rbClickListener);
        this.rbMuscle.setOnClickListener(this.rbClickListener);
        this.rbBone.setOnClickListener(this.rbClickListener);
        this.rbWater.setOnClickListener(this.rbClickListener);
        this.rbBMR.setOnClickListener(this.rbClickListener);
        refreshChart();
    }

    @Click({2131493043})
    void pre() {
        this.pageIndex--;
        refreshChart();
    }

    @Click({2131493044})
    void next() {
        this.pageIndex++;
        refreshChart();
    }

    public void reloadLayoutSize() {
        this.rbWeek.setTextSize((float) 8);
        this.rbMonth.setTextSize((float) 8);
        this.rbSeason.setTextSize((float) 8);
        this.rbYear.setTextSize((float) 8);
        this.rbWeek.setMinLines(2);
        this.rbWeek.setMaxLines(2);
    }

    public void refreshChart() {
        int datatype = 0;
        if (this.rbWeight.isChecked()) {
            datatype = 1;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.weight));
            this.tvChartTip2.setText("(" + WeightUnitHelper.getWeightUnitString() + ")");
        } else if (this.rbFat.isChecked()) {
            datatype = 2;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.fat));
            this.tvChartTip2.setText("(%)");
        } else if (this.rbBMI.isChecked()) {
            datatype = 6;
            this.tvChartTip.setText(C0181R.string.BMI);
            this.tvChartTip2.setText("");
        } else if (this.rbMuscle.isChecked()) {
            datatype = 3;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.muscle));
            this.tvChartTip2.setText("(%)");
        } else if (this.rbBone.isChecked()) {
            datatype = 4;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.bone));
            this.tvChartTip2.setText("(" + WeightUnitHelper.getWeightUnitString() + ")");
        } else if (this.rbWater.isChecked()) {
            datatype = 5;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.water));
            this.tvChartTip2.setText("(%)");
        } else if (this.rbBMR.isChecked()) {
            datatype = 8;
            this.tvChartTip.setText(this.mContext.getString(C0181R.string.calorie));
            this.tvChartTip2.setText("(Kcal)");
        }
        if (this.rbWeek.isChecked()) {
            drawWeek(this.pageIndex, datatype);
        } else if (this.rbMonth.isChecked()) {
            drawMonth(this.pageIndex, datatype);
        } else if (this.rbSeason.isChecked()) {
            drawSeason(this.pageIndex, datatype);
        } else if (this.rbYear.isChecked()) {
            drawYear(this.pageIndex, datatype);
        }
    }

    private void drawChart(List<Date> nowDate, String[] x_txt, String[] x_top_txt, int[] x_val, int datatype) {
        int i;
        float split;
        float y_max = 0.0f;
        float y_min = 0.0f;
        List<Float> val_list = new LinkedList();
        this.balanceDatas = this.dal.SelectDayDataByTime(this.member.getMemberid(), this.member.getClientid(), ObjectHelper.Convert2String((Date) nowDate.get(0), "yyyy-MM-dd 00:00:00"), ObjectHelper.Convert2String((Date) nowDate.get(nowDate.size() - 1), "yyyy-MM-dd 23:59:59"));
        for (i = 0; i < nowDate.size(); i++) {
            val_list.add(Float.valueOf(0.0f));
        }
        for (int ii = 0; ii < this.balanceDatas.size(); ii++) {
            float value;
            if (datatype == 1) {
                try {
                    value = ObjectHelper.Convert2Float(WeightUnitHelper.getConvertedWtVal(true, 1, ((BalanceDataMDL) this.balanceDatas.get(ii)).getVal(datatype)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (datatype == 4) {
                value = ObjectHelper.Convert2Float(WeightUnitHelper.getConvertedWtVal(1, ((BalanceDataMDL) this.balanceDatas.get(ii)).getVal(datatype)));
            } else {
                value = ObjectHelper.Convert2Float(((BalanceDataMDL) this.balanceDatas.get(ii)).getVal(datatype));
            }
            Date xlableDate = (Date) nowDate.get(0);
            Date date = xlableDate;
            List<Float> list = val_list;
            list.set(ObjectHelper.daysBetween(ObjectHelper.Convert2String(date, "yyyy-MM-dd"), ObjectHelper.Convert2String(((BalanceDataMDL) this.balanceDatas.get(ii)).getWeidate(), "yyyy-MM-dd")), Float.valueOf(value));
        }
        if (datatype == 1) {
            float targetWt = ObjectHelper.Convert2Float(WeightUnitHelper.getConvertedWtVal(true, 1, this.member.getTargetweight()));
            this.chartView.setTargetWtValue(targetWt);
            val_list.add(Float.valueOf(targetWt));
        } else {
            this.chartView.setTargetWtValue(0.0f);
        }
        float[] val = new float[(val_list.size() - 1)];
        i = 0;
        while (i < val_list.size()) {
            if (y_max == 0.0f && ((Float) val_list.get(i)).floatValue() != 0.0f) {
                y_max = ((Float) val_list.get(i)).floatValue();
            }
            if (y_min == 0.0f && ((Float) val_list.get(i)).floatValue() != 0.0f) {
                y_min = ((Float) val_list.get(i)).floatValue();
            }
            if (i != val_list.size() - 1) {
                val[i] = ((Float) val_list.get(i)).floatValue();
            }
            if (y_max <= ((Float) val_list.get(i)).floatValue()) {
                y_max = ((Float) val_list.get(i)).floatValue();
            }
            if (y_min >= ((Float) val_list.get(i)).floatValue() && ((Float) val_list.get(i)).floatValue() != 0.0f) {
                y_min = ((Float) val_list.get(i)).floatValue();
            }
            i++;
        }
        if (y_max == 0.0f) {
            y_max = 5.0f;
            y_min = 0.0f;
        }
        float cha = y_max - y_min;
        if (cha <= 2.0f) {
            split = 1.0f;
        } else if (cha > 2.0f && cha <= 7.0f) {
            split = 5.0f;
        } else if (cha > 7.0f && cha <= 12.0f) {
            split = 10.0f;
        } else if (cha > 12.0f && cha <= 17.0f) {
            split = 15.0f;
        } else if (cha > 18.0f && cha <= 23.0f) {
            split = 20.0f;
        } else if (cha / 20.0f > 15.0f) {
            split = cha / 15.0f;
        } else {
            split = 20.0f;
        }
        y_max = (float) ((int) (y_max + split));
        y_min = (float) ((int) (y_min - (2.0f * split)));
        if (y_min < 0.0f) {
            y_min = 0.0f;
        }
        List<Integer> y_list = new LinkedList();
        int y_count = ((int) ((y_max - y_min) / split)) + 1;
        if (y_count > 11) {
            y_count = 11;
            split = (y_max - y_min) / 10.0f;
        }
        for (i = 0; i < y_count; i++) {
            int v = (int) (y_max - (((float) i) * split));
            if (v > 0) {
                y_list.add(0, Integer.valueOf(v));
            } else if (v <= 0) {
                y_list.add(0, Integer.valueOf(0));
                break;
            }
        }
        Integer[] y_tmp_val = (Integer[]) y_list.toArray(new Integer[0]);
        float[] y_val = new float[y_tmp_val.length];
        String[] y_txt = new String[y_tmp_val.length];
        for (i = 0; i < y_tmp_val.length; i++) {
            y_val[i] = (float) y_tmp_val[i].intValue();
            y_txt[i] = new StringBuilder(String.valueOf((int) y_val[i])).toString();
        }
        this.chartView.setXY(x_val, y_val, x_txt, x_top_txt, y_txt, val);
        this.chartView.invalidate();
    }

    private void drawWeek(int index, int datatype) {
        drawChart(DateHelper.dateToWeek(this.lastdataDate, index), this.mContext.getResources().getStringArray(C0181R.array.weekday_array), new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowWeek.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowWeek.get(0), "MM/dd")).toString(), "", "", new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowWeek.get(3), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowWeek.get(3), "MM/dd")).toString(), "", "", new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowWeek.get(6), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowWeek.get(6), "MM/dd")).toString()}, new int[]{1, 2, 3, 4, 5, 6, 7}, datatype);
    }

    private void drawMonth(int index, int datatype) {
        drawChart(DateHelper.dateToMonth(this.lastdataDate, index), new String[]{"1", "7", "13", "19", "25", new StringBuilder(String.valueOf(((Date) nowMonth.get(DateHelper.dateToMonth(this.lastdataDate, index).size() - 1)).getDate())).toString()}, new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowMonth.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowMonth.get(0), "MM/01")).toString(), "", new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowMonth.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowMonth.get(2), "MM/13")).toString(), "", new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowMonth.get(4), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) DateHelper.dateToMonth(this.lastdataDate, index).get(4), "MM/25")).toString(), ""}, new int[]{1, 7, 13, 19, 25, ((Date) nowMonth.get(DateHelper.dateToMonth(this.lastdataDate, index).size() - 1)).getDate()}, datatype);
    }

    private void drawSeason(int index, int datatype) {
        String[] x_txt;
        int[] x_val;
        String[] x_top_txt;
        List<Date> nowSeason = DateHelper.dateToSeason(this.lastdataDate, index);
        Calendar cal = Calendar.getInstance();
        String[] months = this.mContext.getResources().getStringArray(C0181R.array.month_array);
        int day1;
        int day2;
        int day3;
        if (((Date) nowSeason.get(0)).getMonth() == 0) {
            x_txt = new String[]{months[0], months[1], months[2], months[3]};
            cal.set(2, 0);
            day1 = cal.getActualMaximum(5);
            cal.set(2, 1);
            day2 = cal.getActualMaximum(5);
            cal.set(2, 2);
            day3 = cal.getActualMaximum(5);
            x_val = new int[]{1, day1 + 1, (day1 + day2) + 1, ((day1 + day2) + day3) + 1};
            x_top_txt = new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(0), "01/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(1), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(1), "02/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(2), "03/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(3), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(3), "04/01")).toString()};
        } else if (((Date) nowSeason.get(0)).getMonth() == 3) {
            x_txt = new String[]{months[3], months[4], months[5], months[6]};
            cal.set(2, 3);
            day1 = cal.getActualMaximum(5);
            cal.set(2, 4);
            day2 = cal.getActualMaximum(5);
            cal.set(2, 5);
            day3 = cal.getActualMaximum(5);
            x_val = new int[]{1, day1 + 1, (day1 + day2) + 1, ((day1 + day2) + day3) + 1};
            x_top_txt = new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(0), "04/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(1), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(1), "05/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(2), "06/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(3), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(3), "07/01")).toString()};
        } else if (((Date) nowSeason.get(0)).getMonth() == 6) {
            x_txt = new String[]{months[6], months[7], months[8], months[9]};
            cal.set(2, 6);
            day1 = cal.getActualMaximum(5);
            cal.set(2, 7);
            day2 = cal.getActualMaximum(5);
            cal.set(2, 8);
            day3 = cal.getActualMaximum(5);
            x_val = new int[]{1, day1 + 1, (day1 + day2) + 1, ((day1 + day2) + day3) + 1};
            x_top_txt = new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(0), "07/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(1), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(1), "08/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(2), "09/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(3), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(3), "10/01")).toString()};
        } else {
            x_txt = new String[]{months[9], months[10], months[11], months[0]};
            cal.set(2, 9);
            day1 = cal.getActualMaximum(5);
            cal.set(2, 10);
            day2 = cal.getActualMaximum(5);
            cal.set(2, 11);
            day3 = cal.getActualMaximum(5);
            x_val = new int[]{1, day1 + 1, (day1 + day2) + 1, ((day1 + day2) + day3) + 1};
            x_top_txt = new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(0), "10/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(1), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(1), "11/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowSeason.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(2), "12/01")).toString(), new StringBuilder(String.valueOf(ObjectHelper.Convert2Int(ObjectHelper.Convert2String((Date) nowSeason.get(3), "yyyy")) + 1)).append(";").append(ObjectHelper.Convert2String((Date) nowSeason.get(3), "01/01")).toString()};
        }
        drawChart(nowSeason, x_txt, x_top_txt, x_val, datatype);
    }

    private void drawYear(int index, int datatype) {
        List<Date> nowYear = DateHelper.dateToYear(this.lastdataDate, index);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) nowYear.get(0));
        String[] months = this.mContext.getResources().getStringArray(C0181R.array.month_array);
        String[] x_txt = new String[]{months[0], months[3], months[6], months[9], months[0]};
        String[] x_top_txt = new String[]{new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowYear.get(0), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowYear.get(0), "01/01")).toString(), "", new StringBuilder(String.valueOf(ObjectHelper.Convert2String((Date) nowYear.get(2), "yyyy"))).append(";").append(ObjectHelper.Convert2String((Date) nowYear.get(2), "07/01")).toString(), "", new StringBuilder(String.valueOf(ObjectHelper.Convert2Int(ObjectHelper.Convert2String((Date) nowYear.get(4), "yyyy")) + 1)).append(";").append(ObjectHelper.Convert2String((Date) nowYear.get(4), "01/01")).toString()};
        int day1 = DateHelper.getDayYear(((Date) nowYear.get(0)).getYear(), 0, 1);
        int day4 = DateHelper.getDayYear(((Date) nowYear.get(0)).getYear(), 3, 1);
        int day7 = DateHelper.getDayYear(((Date) nowYear.get(0)).getYear(), 6, 1);
        int day10 = DateHelper.getDayYear(((Date) nowYear.get(0)).getYear(), 9, 1);
        int day12 = calendar.getActualMaximum(6);
        drawChart(nowYear, x_txt, x_top_txt, new int[]{1, 91, 182, 273, 365}, datatype);
    }
}
