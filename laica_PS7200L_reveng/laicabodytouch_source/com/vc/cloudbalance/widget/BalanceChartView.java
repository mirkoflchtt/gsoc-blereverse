package com.vc.cloudbalance.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.vc.cloudbalance.common.Common;
import com.whb.loease.bodytouch.C0181R;

public class BalanceChartView extends View {
    private boolean isWtValue = false;
    Context mContext;
    private float[] vals;
    private float wtValue = 0.0f;
    private String[] x_top_txt;
    private String[] x_txt;
    private int[] x_val;
    private String[] y_txt;
    private float[] y_val;

    public void setXY(int[] _x_val, float[] _y_val, String[] _x_txt, String[] _x_top_txt, String[] _y_txt, float[] _val) {
        this.x_val = _x_val;
        this.y_val = _y_val;
        this.x_txt = _x_txt;
        this.y_txt = _y_txt;
        this.x_top_txt = _x_top_txt;
        this.vals = _val;
    }

    public BalanceChartView(Context context) {
        super(context);
        this.mContext = context;
    }

    public BalanceChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public BalanceChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
    }

    public void setTargetWtValue(float wtValue) {
        this.wtValue = wtValue;
        if (wtValue > 0.0f) {
            this.isWtValue = true;
        } else {
            this.isWtValue = false;
        }
    }

    protected void onDraw(Canvas canvas) {
        if (this.x_val != null && this.y_val != null) {
            int i;
            float y_item;
            Path path;
            float x_item;
            int lableTextSize = getResources().getDimensionPixelSize(C0181R.dimen.datachart_labletextsize);
            int smallerlabelPaintTextSize = (lableTextSize * 3) / 4;
            int linePaintColor = Common.getThemeColor(this.mContext);
            int parsedColor = Common.getThemeLightColor(this.mContext);
            Paint LinePaint = new Paint(1);
            LinePaint.setColor(linePaintColor);
            LinePaint.setStrokeWidth(1.0f);
            LinePaint.setStyle(Style.STROKE);
            Paint DottedPaint = new Paint(1);
            DottedPaint.setAntiAlias(true);
            DottedPaint.setStyle(Style.STROKE);
            DottedPaint.setColor(linePaintColor);
            DottedPaint.setPathEffect(new DashPathEffect(new float[]{5.0f, 5.0f, 5.0f, 5.0f}, 1.0f));
            Paint labelPaint = new Paint();
            labelPaint.setAlpha(MotionEventCompat.ACTION_MASK);
            labelPaint.setTextSize((float) lableTextSize);
            labelPaint.setTextAlign(Align.RIGHT);
            labelPaint.setColor(Color.parseColor("#fff100"));
            Paint smallerlabelPaint = new Paint();
            smallerlabelPaint.setTextSize((float) smallerlabelPaintTextSize);
            smallerlabelPaint.setTextAlign(Align.CENTER);
            smallerlabelPaint.setColor(linePaintColor);
            Paint CirclePaint = new Paint(1);
            CirclePaint.setColor(parsedColor);
            CirclePaint.setStrokeWidth(2.0f);
            CirclePaint.setStyle(Style.STROKE);
            Paint CircleStrokePaint = new Paint(1);
            CircleStrokePaint.setColor(linePaintColor);
            CircleStrokePaint.setStrokeWidth(0.0f);
            CircleStrokePaint.setStyle(Style.FILL);
            int width = getWidth();
            int height = getHeight() - 10;
            FontMetrics fontMetrics = labelPaint.getFontMetrics();
            float fontTotalHeight = fontMetrics.bottom - fontMetrics.top;
            int y_lable_width = getResources().getDimensionPixelSize(C0181R.dimen.datachart_lablewith);
            int max_y = (int) this.y_val[this.y_val.length - 1];
            int min_y = (int) this.y_val[0];
            int max_x = this.x_val[this.x_val.length - 1];
            int min_x = this.x_val[0];
            int padding_top = (int) (((double) y_lable_width) * 1.2d);
            float chart_width = (float) width;
            float chart_val_line_height = (((float) height) - 40.0f) - ((float) padding_top);
            float chart_val_line_width = chart_width;
            float y_offet = (float) padding_top;
            float yy_padding_top = 40.0f / 2.0f;
            canvas.drawLine((float) y_lable_width, 0.0f, (float) y_lable_width, chart_val_line_height + y_offet, LinePaint);
            canvas.drawLine((float) y_lable_width, chart_val_line_height + y_offet, chart_width, chart_val_line_height + y_offet, LinePaint);
            if (this.isWtValue) {
                LinePaint.setColor(SupportMenu.CATEGORY_MASK);
                canvas.drawLine((float) y_lable_width, (((((float) max_y) - this.wtValue) * chart_val_line_height) / ((float) (max_y - min_y))) + y_offet, chart_width, (((((float) max_y) - this.wtValue) * chart_val_line_height) / ((float) (max_y - min_y))) + y_offet, LinePaint);
            }
            labelPaint.setColor(linePaintColor);
            for (i = 0; i < this.y_val.length; i++) {
                y_item = ((((float) (this.y_val.length - i)) * chart_val_line_height) / ((float) this.y_val.length)) + y_offet;
                y_item = (((((float) max_y) - this.y_val[i]) * chart_val_line_height) / ((float) (max_y - min_y))) + y_offet;
                if (i != 0) {
                    path = new Path();
                    path.moveTo((float) y_lable_width, y_item);
                    path.lineTo((float) width, y_item);
                    canvas.drawPath(path, DottedPaint);
                }
                canvas.drawText(this.y_txt[i], (float) (y_lable_width - 5), y_item, labelPaint);
            }
            for (i = 0; i < this.x_val.length; i++) {
                labelPaint.setTextAlign(Align.CENTER);
                float x_unit_widtd = (chart_val_line_width - ((float) y_lable_width)) / ((float) ((max_x - min_x) + 1));
                float x_space_width = (chart_val_line_width - ((float) y_lable_width)) / ((float) (this.x_val.length + 1));
                x_item = (((chart_val_line_width - ((float) y_lable_width)) - (2.0f * 60.0f)) * ((float) (this.x_val[i] - 1))) / ((float) (max_x - min_x));
                path = new Path();
                path.moveTo((((float) y_lable_width) + x_item) + 60.0f, chart_val_line_height + y_offet);
                path.lineTo((((float) y_lable_width) + x_item) + 60.0f, 0.0f + (y_offet / 2.0f));
                canvas.drawPath(path, DottedPaint);
                Canvas canvas2 = canvas;
                canvas2.drawText(this.x_txt[i], (((float) y_lable_width) + x_item) + 60.0f, (float) height, labelPaint);
                if (!TextUtils.isEmpty(this.x_top_txt[i])) {
                    String[] dateStrings = this.x_top_txt[i].split(";");
                    canvas.drawText(dateStrings[0], (((float) y_lable_width) + x_item) + 60.0f, (float) (lableTextSize - 5), labelPaint);
                    canvas.drawText(dateStrings[1], (((float) y_lable_width) + x_item) + 60.0f, (float) ((lableTextSize + smallerlabelPaintTextSize) - 5), smallerlabelPaint);
                }
            }
            Path polygonPath = new Path();
            Path polygonLinePath = new Path();
            Paint paint = new Paint(1);
            paint.setStrokeWidth(0.0f);
            paint.setStyle(Style.FILL);
            paint.setColor(parsedColor);
            int newPathFlag = 0;
            float lastX = 0.0f;
            float lastY = 0.0f;
            for (i = 0; i < this.vals.length; i++) {
                if (i == 27) {
                }
                if (this.vals[i] > 0.0f) {
                    if (this.vals[i] < ((float) min_y)) {
                        this.vals[i] = (float) min_y;
                    }
                    y_item = (((((float) max_y) - this.vals[i]) * chart_val_line_height) / ((float) (max_y - min_y))) + y_offet;
                    x_item = (((chart_val_line_width - ((float) y_lable_width)) - (2.0f * 60.0f)) * ((float) i)) / ((float) (max_x - min_x));
                    Log.e("vals", new StringBuilder(String.valueOf(i)).append("   ").append(this.vals[i]).append("    ").append(x_item).toString());
                    if (newPathFlag == 0) {
                        polygonPath.moveTo((((float) y_lable_width) + x_item) + 60.0f, chart_val_line_height + y_offet);
                        Log.e("polygonPath-1", new StringBuilder(String.valueOf(i)).append("   ").append(this.vals[i]).append("    ").append((((float) y_lable_width) + x_item) + 60.0f).append("    ").append(chart_val_line_height + y_offet).toString());
                        polygonLinePath.moveTo((((float) y_lable_width) + x_item) + 60.0f, y_item);
                    } else {
                        polygonLinePath.lineTo((((float) y_lable_width) + x_item) + 60.0f, y_item);
                    }
                    polygonPath.lineTo((((float) y_lable_width) + x_item) + 60.0f, y_item);
                    lastX = (((float) y_lable_width) + x_item) + 60.0f;
                    lastY = chart_val_line_height + y_offet;
                    Log.e("polygonPath-2", new StringBuilder(String.valueOf(i)).append("   ").append(this.vals[i]).append("    ").append((((float) y_lable_width) + x_item) + 60.0f).append("    ").append(y_item).toString());
                    newPathFlag++;
                }
            }
            polygonPath.lineTo(lastX, lastY);
            canvas.drawPath(polygonPath, paint);
            canvas.drawPath(polygonLinePath, CirclePaint);
            polygonPath.setFillType(FillType.INVERSE_EVEN_ODD);
            polygonPath = new Path();
            for (i = 0; i < this.vals.length; i++) {
                if (this.vals[i] > 0.0f) {
                    y_item = (((((float) max_y) - this.vals[i]) * chart_val_line_height) / ((float) (max_y - min_y))) + y_offet;
                    x_item = (((chart_val_line_width - ((float) y_lable_width)) - (2.0f * 60.0f)) * ((float) i)) / ((float) (max_x - min_x));
                    canvas.drawCircle((((float) y_lable_width) + x_item) + 60.0f, y_item, 4.0f, CircleStrokePaint);
                    canvas.drawCircle((((float) y_lable_width) + x_item) + 60.0f, y_item, 4.0f, CirclePaint);
                    int length = this.vals.length;
                }
            }
            super.onDraw(canvas);
        }
    }
}
