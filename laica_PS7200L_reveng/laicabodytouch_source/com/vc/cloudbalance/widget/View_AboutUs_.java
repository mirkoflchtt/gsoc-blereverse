package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_AboutUs_ extends View_AboutUs implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    public View_AboutUs_(Context context) {
        super(context);
        init_();
    }

    public View_AboutUs_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_AboutUs build(Context context) {
        View_AboutUs_ instance = new View_AboutUs_(context);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_aboutus, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_AboutUs build(Context context, AttributeSet attrs) {
        View_AboutUs_ instance = new View_AboutUs_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvVersion2 = (TextView) hasViews.findViewById(C0181R.id.tvVersion2);
        this.tvVersion1 = (TextView) hasViews.findViewById(C0181R.id.tvVersion1);
        init();
    }
}
