package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_DataChart_Adult_ extends View_DataChart_Adult implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01081 implements OnClickListener {
        C01081() {
        }

        public void onClick(View view) {
            View_DataChart_Adult_.this.next();
        }
    }

    class C01092 implements OnClickListener {
        C01092() {
        }

        public void onClick(View view) {
            View_DataChart_Adult_.this.pre();
        }
    }

    public View_DataChart_Adult_(Context context, MemberMDL member) {
        super(context, member);
        init_();
    }

    public View_DataChart_Adult_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_DataChart_Adult build(Context context, MemberMDL member) {
        View_DataChart_Adult_ instance = new View_DataChart_Adult_(context, member);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_DataChart_Adult build(Context context, AttributeSet attrs) {
        View_DataChart_Adult_ instance = new View_DataChart_Adult_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.btnPre = (Button) hasViews.findViewById(C0181R.id.btnPre);
        this.btnNext = (Button) hasViews.findViewById(C0181R.id.btnNext);
        this.llPanel = (RelativeLayout) hasViews.findViewById(C0181R.id.llPanel);
        this.rbWater = (RadioButton) hasViews.findViewById(C0181R.id.rbWater);
        this.rbBMR = (RadioButton) hasViews.findViewById(C0181R.id.rbBMR);
        this.rbMuscle = (RadioButton) hasViews.findViewById(C0181R.id.rbMuscle);
        this.rbSeason = (RadioButton) hasViews.findViewById(C0181R.id.rbSeason);
        this.rbBMI = (RadioButton) hasViews.findViewById(C0181R.id.rbBMI);
        this.rbWeek = (RadioButton) hasViews.findViewById(C0181R.id.rbWeek);
        this.rbMonth = (RadioButton) hasViews.findViewById(C0181R.id.rbMonth);
        this.tvChartTip2 = (TextView) hasViews.findViewById(C0181R.id.tvChartTip2);
        this.rbYear = (RadioButton) hasViews.findViewById(C0181R.id.rbYear);
        this.tvChartTip = (TextView) hasViews.findViewById(C0181R.id.tvChartTip);
        this.rbFat = (RadioButton) hasViews.findViewById(C0181R.id.rbFat);
        this.rbWeight = (RadioButton) hasViews.findViewById(C0181R.id.rbWeight);
        this.rbBone = (RadioButton) hasViews.findViewById(C0181R.id.rbBone);
        if (this.btnNext != null) {
            this.btnNext.setOnClickListener(new C01081());
        }
        if (this.btnPre != null) {
            this.btnPre.setOnClickListener(new C01092());
        }
        init();
    }
}
