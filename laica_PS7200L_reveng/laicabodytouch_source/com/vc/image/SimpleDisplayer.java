package com.vc.image;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import p000m.framework.ui.widget.asyncview.AsyncImageView;

public class SimpleDisplayer implements Displayer {
    public void loadCompletedisplay(ImageView imageView, Bitmap bitmap, BitmapDisplayConfig config) {
        loadCompletedisplay(imageView, bitmap, config, config.getHasAnimation());
    }

    public void loadCompletedisplay(ImageView imageView, Bitmap bitmap, BitmapDisplayConfig config, boolean loadAnim) {
        if (loadAnim) {
            switch (config.getAnimationType()) {
                case 0:
                    animationDisplay(imageView, bitmap, config.getAnimation());
                    return;
                case 1:
                    fadeInDisplay(imageView, bitmap);
                    return;
                default:
                    return;
            }
        }
        Display(imageView, bitmap);
    }

    public void loadFailDisplay(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    private void fadeInDisplay(ImageView imageView, Bitmap bitmap) {
        TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(AsyncImageView.DEFAULT_TRANSPARENT), new BitmapDrawable(imageView.getResources(), bitmap)});
        imageView.setImageDrawable(td);
        td.startTransition(300);
    }

    private void Display(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    private void animationDisplay(ImageView imageView, Bitmap bitmap, Animation animation) {
        animation.setStartTime(AnimationUtils.currentAnimationTimeMillis());
        imageView.setImageBitmap(bitmap);
        imageView.startAnimation(animation);
    }
}
