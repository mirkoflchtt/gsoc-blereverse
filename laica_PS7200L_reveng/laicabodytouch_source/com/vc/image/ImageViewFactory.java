package com.vc.image;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import com.vc.image.BitmapCache.ImageCacheParams;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageViewFactory {
    private static ExecutorService bitmapLoadAndDisplayExecutor;
    private static BitmapCache mImageCache;
    private static ImageViewFactory mVCBitmapFactory;
    private VCBitmapFactoryConfig mConfig;
    private Context mContext;
    private boolean mExitTasksEarly = false;
    private boolean mPauseWork = false;
    private final Object mPauseWorkLock = new Object();

    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapLoadAndDisplayTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapLoadAndDisplayTask bitmapWorkerTask) {
            super(res, bitmap);
            this.bitmapWorkerTaskReference = new WeakReference(bitmapWorkerTask);
        }

        public BitmapLoadAndDisplayTask getBitmapWorkerTask() {
            return (BitmapLoadAndDisplayTask) this.bitmapWorkerTaskReference.get();
        }
    }

    private class VCBitmapFactoryConfig {
        public BitmapProcess bitmapProcess;
        public String cachePath;
        public BitmapDisplayConfig defaultDisplayConfig = new BitmapDisplayConfig();
        public int diskCacheSize;
        public Displayer displayer;
        public Downloader downloader;
        public int memCacheSize;
        public float memCacheSizePercent;
        public int originalDiskCacheSize = 31457280;
        public int poolSize = 10;

        public VCBitmapFactoryConfig(Context context) {
            this.defaultDisplayConfig.setAnimation(null);
            this.defaultDisplayConfig.setAnimationType(1);
        }

        public void init() {
            if (this.downloader == null) {
                this.downloader = new SimpleHttpDownloader();
            }
            if (this.displayer == null) {
                this.displayer = new SimpleDisplayer();
            }
            this.bitmapProcess = new BitmapProcess(this.downloader, this.cachePath, this.originalDiskCacheSize);
        }
    }

    private class BitmapLoadAndDisplayTask extends ImageAsyncTask<Object, Void, Bitmap> {
        private Object data;
        private final BitmapDisplayConfig displayConfig;
        private final WeakReference<ImageView> imageViewReference;
        private UroadImageView uImageView;

        public BitmapLoadAndDisplayTask(UroadImageView imageView, BitmapDisplayConfig config) {
            this.imageViewReference = new WeakReference(imageView.getImageView());
            this.displayConfig = config;
            this.uImageView = imageView;
        }

        protected void onPreExecute() {
            if (this.displayConfig.getShowLoading()) {
                this.uImageView.setLoading();
            }
            super.onPreExecute();
        }

        protected Bitmap doInBackground(Object... params) {
            Log.e("doInBackground", (String) params[0]);
            this.data = params[0];
            String dataString = String.valueOf(this.data);
            Bitmap bitmap = null;
            synchronized (ImageViewFactory.this.mPauseWorkLock) {
                while (ImageViewFactory.this.mPauseWork && !isCancelled()) {
                    try {
                        ImageViewFactory.this.mPauseWorkLock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
            if (!(null != null || isCancelled() || ImageViewFactory.this.mExitTasksEarly)) {
                bitmap = ImageViewFactory.this.processBitmap(dataString, this.displayConfig);
            }
            if (!(bitmap == null || ImageViewFactory.mImageCache == null || !this.displayConfig.getIsUseCache())) {
                if (this.displayConfig.getIsUseMemoryCache()) {
                    ImageViewFactory.mImageCache.addBitmapToMemoryCache(dataString, bitmap);
                } else {
                    ImageViewFactory.mImageCache.addBitmapToDiskCache(dataString, bitmap);
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled() || ImageViewFactory.this.mExitTasksEarly) {
                bitmap = null;
            }
            if (this.displayConfig.getShowLoading()) {
                this.uImageView.setEndLoading();
            }
            ImageView imageView = getAttachedImageView();
            if (bitmap != null && imageView != null) {
                ImageViewFactory.this.mConfig.displayer.loadCompletedisplay(imageView, bitmap, this.displayConfig);
            } else if (bitmap == null && imageView != null) {
                ImageViewFactory.this.mConfig.displayer.loadFailDisplay(imageView, this.displayConfig.getLoadfailBitmap());
            }
        }

        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            if (this.displayConfig.getShowLoading()) {
                this.uImageView.setEndLoading();
            }
            synchronized (ImageViewFactory.this.mPauseWorkLock) {
                ImageViewFactory.this.mPauseWorkLock.notifyAll();
            }
        }

        private ImageView getAttachedImageView() {
            ImageView imageView = (ImageView) this.imageViewReference.get();
            return this == ImageViewFactory.getBitmapTaskFromImageView(imageView) ? imageView : null;
        }
    }

    private class BitmapLoadAssetAndDisplayTask extends ImageAsyncTask<Object, Void, Bitmap> {
        private Object data;
        private final BitmapDisplayConfig displayConfig;
        private final WeakReference<ImageView> imageViewReference;

        public BitmapLoadAssetAndDisplayTask(ImageView imageView, BitmapDisplayConfig config) {
            this.imageViewReference = new WeakReference(imageView);
            this.displayConfig = config;
        }

        protected Bitmap doInBackground(Object... params) {
            this.data = params[0];
            Bitmap bitmap = null;
            AssetManager am = ImageViewFactory.this.mContext.getResources().getAssets();
            try {
                Options options = new Options();
                InputStream is = am.open(String.valueOf(this.data));
                bitmap = ImageUtils.getUnErrorBitmap(is, options);
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (this.displayConfig.getIsUseCache()) {
                if (this.displayConfig.getIsUseMemoryCache()) {
                    ImageViewFactory.mImageCache.addBitmapToMemoryCache(String.valueOf(this.data), bitmap);
                } else {
                    ImageViewFactory.mImageCache.addBitmapToDiskCache(String.valueOf(this.data), bitmap);
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled() || ImageViewFactory.this.mExitTasksEarly) {
                bitmap = null;
            }
            ImageView imageView = getAttachedImageView();
            if (bitmap != null && imageView != null) {
                ImageViewFactory.this.mConfig.displayer.loadCompletedisplay(imageView, bitmap, this.displayConfig);
            } else if (bitmap == null && imageView != null) {
                ImageViewFactory.this.mConfig.displayer.loadFailDisplay(imageView, this.displayConfig.getLoadfailBitmap());
            }
        }

        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            synchronized (ImageViewFactory.this.mPauseWorkLock) {
                ImageViewFactory.this.mPauseWorkLock.notifyAll();
            }
        }

        private ImageView getAttachedImageView() {
            return (ImageView) this.imageViewReference.get();
        }
    }

    private class BitmapLoadSDAndDisplayTask extends ImageAsyncTask<Object, Void, Bitmap> {
        private Object data;
        private final BitmapDisplayConfig displayConfig;
        private final WeakReference<ImageView> imageViewReference;
        private final boolean loadAnim;
        private int scale = 1;

        public BitmapLoadSDAndDisplayTask(ImageView imageView, BitmapDisplayConfig config, int scale, boolean loadAnim) {
            this.imageViewReference = new WeakReference(imageView);
            this.displayConfig = config;
            this.scale = scale;
            this.loadAnim = loadAnim;
        }

        protected Bitmap doInBackground(Object... params) {
            Throwable th;
            this.data = params[0];
            Bitmap bitmap = null;
            try {
                Options options = new Options();
                options.inPreferredConfig = Config.ARGB_8888;
                options.inSampleSize = this.scale;
                InputStream stream = null;
                try {
                    InputStream stream2 = new FileInputStream(String.valueOf(this.data));
                    try {
                        bitmap = ImageUtils.getUnErrorBitmap(stream2, options);
                        if (stream2 != null) {
                            try {
                                stream2.close();
                            } catch (IOException e) {
                            }
                        }
                    } catch (Exception e2) {
                        stream = stream2;
                        if (stream != null) {
                            try {
                                stream.close();
                            } catch (IOException e3) {
                            }
                        }
                        if (this.displayConfig.getIsUseCache()) {
                            if (this.displayConfig.getIsUseMemoryCache()) {
                                ImageViewFactory.mImageCache.addBitmapToMemoryCache(String.valueOf(this.data), bitmap);
                            } else {
                                ImageViewFactory.mImageCache.addBitmapToDiskCache(String.valueOf(this.data), bitmap);
                            }
                        }
                        return bitmap;
                    } catch (Throwable th2) {
                        th = th2;
                        stream = stream2;
                        if (stream != null) {
                            try {
                                stream.close();
                            } catch (IOException e4) {
                            }
                        }
                        throw th;
                    }
                } catch (Exception e5) {
                    if (stream != null) {
                        stream.close();
                    }
                    if (this.displayConfig.getIsUseCache()) {
                        if (this.displayConfig.getIsUseMemoryCache()) {
                            ImageViewFactory.mImageCache.addBitmapToDiskCache(String.valueOf(this.data), bitmap);
                        } else {
                            ImageViewFactory.mImageCache.addBitmapToMemoryCache(String.valueOf(this.data), bitmap);
                        }
                    }
                    return bitmap;
                } catch (Throwable th3) {
                    th = th3;
                    if (stream != null) {
                        stream.close();
                    }
                    throw th;
                }
            } catch (Exception e6) {
                e6.printStackTrace();
            }
            if (this.displayConfig.getIsUseCache()) {
                if (this.displayConfig.getIsUseMemoryCache()) {
                    ImageViewFactory.mImageCache.addBitmapToMemoryCache(String.valueOf(this.data), bitmap);
                } else {
                    ImageViewFactory.mImageCache.addBitmapToDiskCache(String.valueOf(this.data), bitmap);
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled() || ImageViewFactory.this.mExitTasksEarly) {
                bitmap = null;
            }
            ImageView imageView = getAttachedImageView();
            if (bitmap != null && imageView != null) {
                ImageViewFactory.this.recycle(imageView);
                ImageViewFactory.this.mConfig.displayer.loadCompletedisplay(imageView, bitmap, this.displayConfig, this.loadAnim);
            } else if (bitmap == null && imageView != null) {
                ImageViewFactory.this.mConfig.displayer.loadFailDisplay(imageView, this.displayConfig.getLoadfailBitmap());
            }
        }

        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            synchronized (ImageViewFactory.this.mPauseWorkLock) {
                ImageViewFactory.this.mPauseWorkLock.notifyAll();
            }
        }

        private ImageView getAttachedImageView() {
            return (ImageView) this.imageViewReference.get();
        }
    }

    private class CacheExecutecTask extends ImageAsyncTask<Object, Void, Void> {
        public static final int MESSAGE_CLEAR = 0;
        public static final int MESSAGE_CLEAR_DISK = 5;
        public static final int MESSAGE_CLEAR_KEY = 6;
        public static final int MESSAGE_CLEAR_KEY_IN_DISK = 8;
        public static final int MESSAGE_CLEAR_KEY_IN_MEMORY = 7;
        public static final int MESSAGE_CLEAR_MEMORY = 4;
        public static final int MESSAGE_CLOSE = 3;
        public static final int MESSAGE_FLUSH = 2;
        public static final int MESSAGE_INIT_DISK_CACHE = 1;

        private CacheExecutecTask() {
        }

        protected Void doInBackground(Object... params) {
            switch (((Integer) params[0]).intValue()) {
                case 0:
                    ImageViewFactory.this.clearCacheInternalInBackgroud();
                    break;
                case 1:
                    ImageViewFactory.this.initDiskCacheInternalInBackgroud();
                    break;
                case 2:
                    ImageViewFactory.this.clearMemoryCacheInBackgroud();
                    ImageViewFactory.this.flushCacheInternalInBackgroud();
                    break;
                case 3:
                    ImageViewFactory.this.clearMemoryCacheInBackgroud();
                    ImageViewFactory.this.closeCacheInternalInBackgroud();
                    break;
                case 4:
                    break;
                case 5:
                    ImageViewFactory.this.clearDiskCacheInBackgroud();
                    break;
                case 6:
                    ImageViewFactory.this.clearCacheInBackgroud(String.valueOf(params[1]));
                    break;
                case 7:
                    ImageViewFactory.this.clearMemoryCacheInBackgroud(String.valueOf(params[1]));
                    break;
                case 8:
                    ImageViewFactory.this.clearDiskCacheInBackgroud(String.valueOf(params[1]));
                    break;
            }
            ImageViewFactory.this.clearMemoryCacheInBackgroud();
            return null;
        }
    }

    private ImageViewFactory(Context context) {
        this.mContext = context;
        this.mConfig = new VCBitmapFactoryConfig(context);
        configDiskCachePath(ImageUtils.getDiskCacheDir(context, "bitmapCache").getAbsolutePath());
        configDisplayer(new SimpleDisplayer());
        configDownlader(new SimpleHttpDownloader());
    }

    public static ImageViewFactory create(Context ctx) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, float memoryCacheSizePercent) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configMemoryCachePercent(memoryCacheSizePercent);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, int memoryCacheSize) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configMemoryCacheSize(memoryCacheSize);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, float memoryCacheSizePercent, int threadSize) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configBitmapLoadThreadSize(threadSize);
            mVCBitmapFactory.configMemoryCachePercent(memoryCacheSizePercent);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, int memoryCacheSize, int threadSize) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configBitmapLoadThreadSize(threadSize);
            mVCBitmapFactory.configMemoryCacheSize(memoryCacheSize);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, float memoryCacheSizePercent, int diskCacheSize, int threadSize) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configBitmapLoadThreadSize(threadSize);
            mVCBitmapFactory.configMemoryCachePercent(memoryCacheSizePercent);
            mVCBitmapFactory.configDiskCacheSize(diskCacheSize);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public static ImageViewFactory create(Context ctx, String diskCachePath, int memoryCacheSize, int diskCacheSize, int threadSize) {
        if (mVCBitmapFactory == null) {
            mVCBitmapFactory = new ImageViewFactory(ctx.getApplicationContext());
            mVCBitmapFactory.configDiskCachePath(diskCachePath);
            mVCBitmapFactory.configBitmapLoadThreadSize(threadSize);
            mVCBitmapFactory.configMemoryCacheSize(memoryCacheSize);
            mVCBitmapFactory.configDiskCacheSize(diskCacheSize);
            mVCBitmapFactory.init();
        }
        return mVCBitmapFactory;
    }

    public ImageViewFactory configLoadingImage(Bitmap bitmap) {
        this.mConfig.defaultDisplayConfig.setLoadingBitmap(bitmap);
        return this;
    }

    public ImageViewFactory configLoadingImage(int resId) {
        this.mConfig.defaultDisplayConfig.setLoadingBitmap(BitmapFactory.decodeResource(this.mContext.getResources(), resId));
        return this;
    }

    public ImageViewFactory configLoadfailImage(Bitmap bitmap) {
        this.mConfig.defaultDisplayConfig.setLoadfailBitmap(bitmap);
        return this;
    }

    public ImageViewFactory configLoadAnim(boolean load) {
        this.mConfig.defaultDisplayConfig.setHasAnimation(load);
        return this;
    }

    public ImageViewFactory configLoadfailImage(int resId) {
        this.mConfig.defaultDisplayConfig.setLoadfailBitmap(BitmapFactory.decodeResource(this.mContext.getResources(), resId));
        return this;
    }

    public ImageViewFactory configBitmapMaxHeight(int bitmapHeight) {
        this.mConfig.defaultDisplayConfig.setBitmapHeight(bitmapHeight);
        return this;
    }

    public ImageViewFactory configBitmapMaxWidth(int bitmapWidth) {
        this.mConfig.defaultDisplayConfig.setBitmapWidth(bitmapWidth);
        return this;
    }

    public ImageViewFactory configDownlader(Downloader downlader) {
        this.mConfig.downloader = downlader;
        return this;
    }

    public ImageViewFactory configDisplayer(Displayer displayer) {
        this.mConfig.displayer = displayer;
        return this;
    }

    public ImageViewFactory configCalculateBitmapSizeWhenDecode(boolean neverCalculate) {
        if (!(this.mConfig == null || this.mConfig.bitmapProcess == null)) {
            this.mConfig.bitmapProcess.configCalculateBitmap(neverCalculate);
        }
        return this;
    }

    private ImageViewFactory configDiskCachePath(String strPath) {
        if (!TextUtils.isEmpty(strPath)) {
            this.mConfig.cachePath = strPath;
        }
        return this;
    }

    private ImageViewFactory configMemoryCacheSize(int size) {
        this.mConfig.memCacheSize = size;
        return this;
    }

    private ImageViewFactory configMemoryCachePercent(float percent) {
        this.mConfig.memCacheSizePercent = percent;
        return this;
    }

    private ImageViewFactory configDiskCacheSize(int size) {
        this.mConfig.diskCacheSize = size;
        return this;
    }

    private ImageViewFactory configBitmapLoadThreadSize(int size) {
        if (size >= 1) {
            this.mConfig.poolSize = size;
        }
        return this;
    }

    private ImageViewFactory init() {
        this.mConfig.init();
        ImageCacheParams imageCacheParams = new ImageCacheParams(this.mConfig.cachePath);
        if (((double) this.mConfig.memCacheSizePercent) > 0.05d && ((double) this.mConfig.memCacheSizePercent) < 0.8d) {
            imageCacheParams.setMemCacheSizePercent(this.mContext, this.mConfig.memCacheSizePercent);
        } else if (this.mConfig.memCacheSize > AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_END) {
            imageCacheParams.setMemCacheSize(this.mConfig.memCacheSize);
        } else {
            imageCacheParams.setMemCacheSizePercent(this.mContext, 0.3f);
        }
        if (this.mConfig.diskCacheSize > 5242880) {
            imageCacheParams.setDiskCacheSize(this.mConfig.diskCacheSize);
        }
        mImageCache = new BitmapCache(imageCacheParams);
        bitmapLoadAndDisplayExecutor = Executors.newFixedThreadPool(this.mConfig.poolSize);
        new CacheExecutecTask().execute(Integer.valueOf(1));
        return this;
    }

    public BitmapCache getBitmapCache() {
        return mImageCache;
    }

    public void display(UroadImageView imageView, String uri, BitmapDisplayConfig config) {
        doDisplay(imageView, uri, config);
    }

    public void doDisplayByAsset(ImageView imageView, String uri) {
        if (checkImageTask(uri, imageView)) {
            new BitmapLoadAssetAndDisplayTask(imageView, this.mConfig.defaultDisplayConfig).execute(uri);
        }
    }

    public void doDisplayBySD(ImageView imageView, String uri, int scale, boolean loadAnim) {
        Bitmap bitmap = null;
        if (mImageCache != null && scale > 1) {
            bitmap = mImageCache.getBitmapFromCache(uri);
        }
        if (bitmap != null && !bitmap.isRecycled()) {
            imageView.setImageBitmap(bitmap);
        } else if (checkImageTask(uri, imageView)) {
            new BitmapLoadSDAndDisplayTask(imageView, this.mConfig.defaultDisplayConfig, scale, loadAnim).execute(uri);
        }
    }

    private void doDisplay(UroadImageView uv, String uri, BitmapDisplayConfig displayConfig) {
        if (!TextUtils.isEmpty(uri) && uv != null) {
            ImageView imageView = uv.getImageView();
            if (displayConfig == null) {
                displayConfig = this.mConfig.defaultDisplayConfig;
            }
            Bitmap bitmap = null;
            if (mImageCache != null) {
                bitmap = mImageCache.getBitmapFromCache(uri);
            }
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else if (checkImageTask(uri, imageView)) {
                BitmapLoadAndDisplayTask task = new BitmapLoadAndDisplayTask(uv, displayConfig);
                imageView.setImageDrawable(new AsyncDrawable(this.mContext.getResources(), displayConfig.getLoadingBitmap(), task));
                task.executeOnExecutor(bitmapLoadAndDisplayExecutor, uri);
            }
        }
    }

    public BitmapDisplayConfig getDisplayConfig() {
        BitmapDisplayConfig config = new BitmapDisplayConfig();
        config.setAnimation(this.mConfig.defaultDisplayConfig.getAnimation());
        config.setAnimationType(this.mConfig.defaultDisplayConfig.getAnimationType());
        config.setBitmapHeight(this.mConfig.defaultDisplayConfig.getBitmapHeight());
        config.setBitmapWidth(this.mConfig.defaultDisplayConfig.getBitmapWidth());
        config.setLoadfailBitmap(this.mConfig.defaultDisplayConfig.getLoadfailBitmap());
        config.setLoadingBitmap(this.mConfig.defaultDisplayConfig.getLoadingBitmap());
        return config;
    }

    private void initDiskCacheInternalInBackgroud() {
        if (mImageCache != null) {
            mImageCache.initDiskCache();
        }
        if (this.mConfig != null && this.mConfig.bitmapProcess != null) {
            this.mConfig.bitmapProcess.initHttpDiskCache();
        }
    }

    private void clearCacheInternalInBackgroud() {
        if (mImageCache != null) {
            mImageCache.clearCache();
        }
        if (this.mConfig != null && this.mConfig.bitmapProcess != null) {
            this.mConfig.bitmapProcess.clearCacheInternal();
        }
    }

    private void clearMemoryCacheInBackgroud() {
        if (mImageCache != null) {
            mImageCache.clearMemoryCache();
        }
    }

    private void clearDiskCacheInBackgroud() {
        if (mImageCache != null) {
            mImageCache.clearDiskCache();
        }
        if (this.mConfig != null && this.mConfig.bitmapProcess != null) {
            this.mConfig.bitmapProcess.clearCacheInternal();
        }
    }

    private void clearCacheInBackgroud(String key) {
        if (mImageCache != null) {
            mImageCache.clearCache(key);
        }
    }

    private void clearDiskCacheInBackgroud(String key) {
        if (mImageCache != null) {
            mImageCache.clearDiskCache(key);
        }
    }

    private void clearMemoryCacheInBackgroud(String key) {
        if (mImageCache != null) {
            mImageCache.clearMemoryCache(key);
        }
    }

    private void flushCacheInternalInBackgroud() {
        if (mImageCache != null) {
            mImageCache.flush();
        }
        if (this.mConfig != null && this.mConfig.bitmapProcess != null) {
            this.mConfig.bitmapProcess.flushCacheInternal();
        }
    }

    private void closeCacheInternalInBackgroud() {
        if (mImageCache != null) {
            mImageCache.close();
            mImageCache = null;
        }
        if (this.mConfig != null && this.mConfig.bitmapProcess != null) {
            this.mConfig.bitmapProcess.clearCacheInternal();
        }
    }

    private Bitmap processBitmap(String uri, BitmapDisplayConfig config) {
        if (this.mConfig == null || this.mConfig.bitmapProcess == null) {
            return null;
        }
        return this.mConfig.bitmapProcess.processBitmap(uri, config);
    }

    public void setExitTasksEarly(boolean exitTasksEarly) {
        this.mExitTasksEarly = exitTasksEarly;
    }

    public void onResume() {
        setExitTasksEarly(false);
    }

    public void onPause() {
        setExitTasksEarly(true);
    }

    public void onDestroy() {
        closeCache();
    }

    public Bitmap getByCache(String url) {
        if (mImageCache != null) {
            return mImageCache.getBitmapFromCache(url);
        }
        return null;
    }

    public void clearCache() {
        new CacheExecutecTask().execute(Integer.valueOf(0));
    }

    public void clearCache(String key) {
        new CacheExecutecTask().execute(Integer.valueOf(6), key);
    }

    public void clearMemoryCache() {
        new CacheExecutecTask().execute(Integer.valueOf(4));
    }

    public void clearMemoryCache(String key) {
        new CacheExecutecTask().execute(Integer.valueOf(7), key);
    }

    public void clearDiskCache() {
        new CacheExecutecTask().execute(Integer.valueOf(5));
    }

    public void clearDiskCache(String key) {
        new CacheExecutecTask().execute(Integer.valueOf(8), key);
    }

    public void flushCache() {
        new CacheExecutecTask().execute(Integer.valueOf(2));
    }

    public void closeCache() {
        new CacheExecutecTask().execute(Integer.valueOf(3));
    }

    public void exitTasksEarly(boolean exitTasksEarly) {
        this.mExitTasksEarly = exitTasksEarly;
        if (exitTasksEarly) {
            pauseWork(false);
        }
    }

    public void pauseWork(boolean pauseWork) {
        synchronized (this.mPauseWorkLock) {
            this.mPauseWork = pauseWork;
            if (!this.mPauseWork) {
                this.mPauseWorkLock.notifyAll();
            }
        }
    }

    private static BitmapLoadAndDisplayTask getBitmapTaskFromImageView(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                return ((AsyncDrawable) drawable).getBitmapWorkerTask();
            }
        }
        return null;
    }

    public boolean checkImageTask(Object data, ImageView imageView) {
        BitmapLoadAndDisplayTask bitmapWorkerTask = getBitmapTaskFromImageView(imageView);
        if (bitmapWorkerTask == null) {
            return true;
        }
        Object bitmapData = bitmapWorkerTask.data;
        if (bitmapData != null && bitmapData.equals(data)) {
            return false;
        }
        bitmapWorkerTask.cancel(true);
        return true;
    }

    public static int computeSampleSize(Options options, int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
        if (initialSize > 8) {
            return ((initialSize + 7) / 8) * 8;
        }
        int roundedSize = 1;
        while (roundedSize < initialSize) {
            roundedSize <<= 1;
        }
        return roundedSize;
    }

    private static int computeInitialSampleSize(Options options, int minSideLength, int maxNumOfPixels) {
        double w = (double) options.outWidth;
        double h = (double) options.outHeight;
        int lowerBound = maxNumOfPixels == -1 ? 1 : (int) Math.ceil(Math.sqrt((w * h) / ((double) maxNumOfPixels)));
        int upperBound = minSideLength == -1 ? 128 : (int) Math.min(Math.floor(w / ((double) minSideLength)), Math.floor(h / ((double) minSideLength)));
        if (upperBound < lowerBound) {
            return lowerBound;
        }
        if (maxNumOfPixels == -1 && minSideLength == -1) {
            return 1;
        }
        if (minSideLength != -1) {
            return upperBound;
        }
        return lowerBound;
    }

    public void recycle(ImageView vcImageView) {
        if (vcImageView != null) {
            try {
                Drawable drawable = vcImageView.getDrawable();
                BitmapDrawable bd = null;
                if (drawable != null) {
                    if (drawable instanceof TransitionDrawable) {
                        bd = (BitmapDrawable) ((TransitionDrawable) vcImageView.getDrawable()).getDrawable(1);
                    } else if (drawable instanceof BitmapDrawable) {
                        bd = (BitmapDrawable) vcImageView.getDrawable();
                    } else if (drawable instanceof FastBitmapDrawable) {
                        rceycleBitmap(((FastBitmapDrawable) vcImageView.getDrawable()).getBitmap(), vcImageView);
                        return;
                    }
                }
                if (bd != null) {
                    vcImageView.setImageBitmap(null);
                    rceycleBitmap(bd.getBitmap(), vcImageView);
                }
            } catch (Exception e) {
            }
        }
    }

    private void rceycleBitmap(Bitmap bitmap, ImageView vImageView) {
        if (bitmap != null && !bitmap.isRecycled()) {
            Log.e("rceycleBitmap", "rceycleBitmap");
            vImageView.setImageBitmap(null);
            bitmap.recycle();
        }
    }
}
