package com.vc.image;

import android.net.Uri;

public interface IImageView {
    void cancelLoadingTask();

    void clientRender(int i);

    void dispose(boolean z);

    void loadRes(int i);

    void loadUri(Uri uri);

    void loadUri(String str);

    void setImageUrl(String str);

    void setImageUrl(String str, int i);

    void setImageUrlMemoryCache(String str);

    void setImageUrlNotCache(String str);

    void setScaleEnable(boolean z);

    void toggleFillScreen();
}
