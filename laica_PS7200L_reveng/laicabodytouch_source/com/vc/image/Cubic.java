package com.vc.image;

public class Cubic implements Easing {
    public double easeOut(double time, double start, double end, double duration) {
        time = (time / duration) - 1.0d;
        return ((((time * time) * time) + 1.0d) * end) + start;
    }

    public double easeIn(double time, double start, double end, double duration) {
        time /= duration;
        return (((end * time) * time) * time) + start;
    }

    public double easeInOut(double time, double start, double end, double duration) {
        time /= duration / 2.0d;
        if (time < 1.0d) {
            return ((((end / 2.0d) * time) * time) * time) + start;
        }
        time -= 2.0d;
        return ((end / 2.0d) * (((time * time) * time) + 2.0d)) + start;
    }
}
