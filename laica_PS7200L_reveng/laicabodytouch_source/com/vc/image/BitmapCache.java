package com.vc.image;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import com.vc.image.LruDiskCache.Editor;
import com.vc.image.LruDiskCache.Snapshot;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BitmapCache {
    private static final boolean DEFAULT_CLEAR_DISK_CACHE_ON_START = false;
    private static final CompressFormat DEFAULT_COMPRESS_FORMAT = CompressFormat.JPEG;
    private static final int DEFAULT_COMPRESS_QUALITY = 70;
    private static final boolean DEFAULT_DISK_CACHE_ENABLED = true;
    private static final int DEFAULT_DISK_CACHE_SIZE = 20971520;
    private static final boolean DEFAULT_INIT_DISK_CACHE_ON_CREATE = false;
    private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
    private static final int DEFAULT_MEM_CACHE_SIZE = 8388608;
    private static final int DISK_CACHE_INDEX = 0;
    private static final String TAG = "ImageCache";
    private ImageCacheParams mCacheParams;
    private final Object mDiskCacheLock = new Object();
    private boolean mDiskCacheStarting = true;
    private LruDiskCache mDiskLruCache;
    private LruMemoryCache<String, Bitmap> mMemoryCache;

    public static class ImageCacheParams {
        public boolean clearDiskCacheOnStart = false;
        public CompressFormat compressFormat = BitmapCache.DEFAULT_COMPRESS_FORMAT;
        public int compressQuality = BitmapCache.DEFAULT_COMPRESS_QUALITY;
        public File diskCacheDir;
        public boolean diskCacheEnabled = true;
        public int diskCacheSize = BitmapCache.DEFAULT_DISK_CACHE_SIZE;
        public boolean initDiskCacheOnCreate = false;
        public int memCacheSize = 8388608;
        public boolean memoryCacheEnabled = true;

        public ImageCacheParams(File diskCacheDir) {
            this.diskCacheDir = diskCacheDir;
        }

        public ImageCacheParams(String diskCacheDir) {
            this.diskCacheDir = new File(diskCacheDir);
        }

        public void setMemCacheSizePercent(Context context, float percent) {
            if (percent < 0.05f || percent > 0.8f) {
                throw new IllegalArgumentException("setMemCacheSizePercent - percent must be between 0.05 and 0.8 (inclusive)");
            }
            this.memCacheSize = Math.round(((((float) getMemoryClass(context)) * percent) * 1024.0f) * 1024.0f);
        }

        public void setMemCacheSize(int memCacheSize) {
            this.memCacheSize = memCacheSize;
        }

        public void setDiskCacheSize(int diskCacheSize) {
            this.diskCacheSize = diskCacheSize;
        }

        private static int getMemoryClass(Context context) {
            return ((ActivityManager) context.getSystemService("activity")).getMemoryClass();
        }
    }

    public BitmapCache(ImageCacheParams cacheParams) {
        init(cacheParams);
    }

    private void init(ImageCacheParams cacheParams) {
        this.mCacheParams = cacheParams;
        if (this.mCacheParams.memoryCacheEnabled) {
            this.mMemoryCache = new LruMemoryCache<String, Bitmap>(this.mCacheParams.memCacheSize) {
                protected int sizeOf(String key, Bitmap bitmap) {
                    return ImageUtils.getBitmapSize(bitmap);
                }
            };
        }
        if (cacheParams.initDiskCacheOnCreate) {
            initDiskCache();
        }
    }

    public void initDiskCache() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache == null || this.mDiskLruCache.isClosed()) {
                File diskCacheDir = this.mCacheParams.diskCacheDir;
                if (this.mCacheParams.diskCacheEnabled && diskCacheDir != null) {
                    if (!diskCacheDir.exists()) {
                        diskCacheDir.mkdirs();
                    }
                    if (ImageUtils.getUsableSpace(diskCacheDir) > ((long) this.mCacheParams.diskCacheSize)) {
                        try {
                            this.mDiskLruCache = LruDiskCache.open(diskCacheDir, 1, 1, (long) this.mCacheParams.diskCacheSize);
                        } catch (IOException e) {
                            this.mCacheParams.diskCacheDir = null;
                            Log.e(TAG, "initDiskCache - " + e);
                        }
                    }
                }
            }
            this.mDiskCacheStarting = false;
            this.mDiskCacheLock.notifyAll();
        }
    }

    public void addBitmapToDiskCache(String data, Bitmap bitmap) {
        synchronized (this.mDiskCacheLock) {
            if (!(this.mDiskLruCache == null || this.mDiskLruCache.getDirectory() == null)) {
                if (!this.mDiskLruCache.getDirectory().exists()) {
                    this.mDiskLruCache.getDirectory().mkdirs();
                }
                String key = FileNameGenerator.generator(data);
                OutputStream out = null;
                try {
                    Snapshot snapshot = this.mDiskLruCache.get(key);
                    if (snapshot == null) {
                        Editor editor = this.mDiskLruCache.edit(key);
                        if (editor != null) {
                            out = editor.newOutputStream(0);
                            bitmap.compress(this.mCacheParams.compressFormat, this.mCacheParams.compressQuality, out);
                            editor.commit();
                            out.close();
                        }
                    } else {
                        snapshot.getInputStream(0).close();
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IOException e2) {
                    Log.e(TAG, "addBitmapToCache - " + e2);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (Exception e4) {
                    Log.e(TAG, "addBitmapToCache - " + e4);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (Throwable th) {
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e6) {
                        }
                    }
                }
            }
        }
    }

    public void addBitmapToDiskCacheByPng(String data, Bitmap bitmap) {
        synchronized (this.mDiskCacheLock) {
            if (!(this.mDiskLruCache == null || this.mDiskLruCache.getDirectory() == null)) {
                if (!this.mDiskLruCache.getDirectory().exists()) {
                    this.mDiskLruCache.getDirectory().mkdirs();
                }
                String key = FileNameGenerator.generator(data);
                OutputStream out = null;
                try {
                    Snapshot snapshot = this.mDiskLruCache.get(key);
                    if (snapshot == null) {
                        Editor editor = this.mDiskLruCache.edit(key);
                        if (editor != null) {
                            out = editor.newOutputStream(0);
                            bitmap.compress(CompressFormat.PNG, 100, out);
                            editor.commit();
                            out.close();
                        }
                    } else {
                        snapshot.getInputStream(0).close();
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IOException e2) {
                    Log.e(TAG, "addBitmapToCache - " + e2);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (Exception e4) {
                    Log.e(TAG, "addBitmapToCache - " + e4);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (Throwable th) {
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e6) {
                        }
                    }
                }
            }
        }
    }

    public void addBitmapToMemoryCache(String data, Bitmap bitmap) {
        if (data != null && bitmap != null && this.mMemoryCache != null && this.mMemoryCache.get(data) == null) {
            this.mMemoryCache.put(data, bitmap);
        }
    }

    public void addBitmapToCache(String data, Bitmap bitmap) {
        if (data != null && bitmap != null) {
            if (this.mMemoryCache != null && this.mMemoryCache.get(data) == null) {
                this.mMemoryCache.put(data, bitmap);
            }
            synchronized (this.mDiskCacheLock) {
                if (!(this.mDiskLruCache == null || this.mDiskLruCache.getDirectory() == null)) {
                    if (!this.mDiskLruCache.getDirectory().exists()) {
                        this.mDiskLruCache.getDirectory().mkdirs();
                    }
                    String key = FileNameGenerator.generator(data);
                    OutputStream out = null;
                    try {
                        Snapshot snapshot = this.mDiskLruCache.get(key);
                        if (snapshot == null) {
                            Editor editor = this.mDiskLruCache.edit(key);
                            if (editor != null) {
                                out = editor.newOutputStream(0);
                                bitmap.compress(this.mCacheParams.compressFormat, this.mCacheParams.compressQuality, out);
                                editor.commit();
                                out.close();
                            }
                        } else {
                            snapshot.getInputStream(0).close();
                        }
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {
                            }
                        }
                    } catch (IOException e2) {
                        Log.e(TAG, "addBitmapToCache - " + e2);
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e3) {
                            }
                        }
                    } catch (Exception e4) {
                        Log.e(TAG, "addBitmapToCache - " + e4);
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e5) {
                            }
                        }
                    } catch (Throwable th) {
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e6) {
                            }
                        }
                    }
                }
            }
            return;
        }
        return;
    }

    public Bitmap getBitmapFromMemCache(String data) {
        if (this.mMemoryCache != null) {
            Bitmap memBitmap = (Bitmap) this.mMemoryCache.get(data);
            if (memBitmap != null) {
                return memBitmap;
            }
        }
        return null;
    }

    public Bitmap getBitmapFromCache(String data) {
        Bitmap bitmap = getBitmapFromMemCache(data);
        if (bitmap != null) {
            return bitmap;
        }
        return getBitmapFromDiskCache(data);
    }

    public Bitmap getBitmapFromDiskCache(String data) {
        String key = FileNameGenerator.generator(data);
        synchronized (this.mDiskCacheLock) {
            while (this.mDiskCacheStarting) {
                try {
                    this.mDiskCacheLock.wait();
                } catch (InterruptedException e) {
                }
            }
            if (this.mDiskLruCache != null) {
                InputStream inputStream = null;
                try {
                    Snapshot snapshot = this.mDiskLruCache.get(key);
                    if (snapshot != null) {
                        inputStream = snapshot.getInputStream(0);
                        if (inputStream != null) {
                            Bitmap bitmap = BitmapDecoder.decodeSampledBitmapFromInputStream(inputStream);
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e2) {
                                }
                            }
                            return bitmap;
                        }
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (IOException e4) {
                    Log.e(TAG, "getBitmapFromDiskCache - " + e4);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e6) {
                        }
                    }
                }
            }
            return null;
        }
    }

    public void clearCache() {
        clearMemoryCache();
        clearDiskCache();
    }

    public void clearDiskCache() {
        synchronized (this.mDiskCacheLock) {
            this.mDiskCacheStarting = true;
            if (!(this.mDiskLruCache == null || this.mDiskLruCache.isClosed())) {
                try {
                    this.mDiskLruCache.delete();
                } catch (IOException e) {
                    Log.e(TAG, "clearCache - " + e);
                }
                this.mDiskLruCache = null;
                initDiskCache();
            }
        }
    }

    public void clearMemoryCache() {
        if (this.mMemoryCache != null) {
            this.mMemoryCache.evictAll();
        }
    }

    public void clearCache(String key) {
        clearMemoryCache(key);
        clearDiskCache(key);
    }

    public void clearDiskCache(String data) {
        String key = FileNameGenerator.generator(data);
        synchronized (this.mDiskCacheLock) {
            if (!(this.mDiskLruCache == null || this.mDiskLruCache.isClosed())) {
                try {
                    this.mDiskLruCache.remove(key);
                } catch (IOException e) {
                    Log.e(TAG, "clearCache - " + e);
                }
            }
        }
    }

    public void clearMemoryCache(String key) {
        if (this.mMemoryCache != null) {
            this.mMemoryCache.remove(key);
        }
    }

    public void flush() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache != null) {
                try {
                    this.mDiskLruCache.flush();
                } catch (IOException e) {
                    Log.e(TAG, "flush - " + e);
                }
            }
        }
    }

    public void close() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache != null) {
                try {
                    if (!this.mDiskLruCache.isClosed()) {
                        this.mDiskLruCache.close();
                        this.mDiskLruCache = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "close - " + e);
                }
            }
        }
    }
}
