package com.vc.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

public class ImageUtils {
    private static final String TAG = "BitmapCommonUtils";

    public static Bitmap getUnErrorBitmap(InputStream is, Options options) {
        try {
            return BitmapFactory.decodeStream(is, null, options);
        } catch (OutOfMemoryError e) {
            options.inSampleSize++;
            return getUnErrorBitmap(is, options);
        }
    }

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static String getBitmapWH(Bitmap bitmap) {
        byte[] datas = Bitmap2Bytes(bitmap);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(datas, 0, datas.length, options);
        return options.outWidth + "X" + options.outHeight;
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        return new File(new StringBuilder(String.valueOf("mounted".equals(Environment.getExternalStorageState()) ? getExternalCacheDir(context).getPath() : context.getCacheDir().getPath())).append(File.separator).append(uniqueName).toString());
    }

    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public static File getExternalCacheDir(Context context) {
        return new File(new StringBuilder(String.valueOf(Environment.getExternalStorageDirectory().getPath())).append("/Android/data/" + context.getPackageName() + "/cache/").toString());
    }

    public static long getUsableSpace(File path) {
        try {
            StatFs stats = new StatFs(path.getPath());
            return ((long) stats.getBlockSize()) * ((long) stats.getAvailableBlocks());
        } catch (Exception e) {
            Log.e(TAG, "获取 sdcard 缓存大小 出错，请查看AndroidManifest.xml 是否添加了sdcard的访问权限");
            e.printStackTrace();
            return -1;
        }
    }
}
