package com.vc.net;

import android.content.Context;
import android.os.Message;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

public class SyncHttpClient extends AsyncHttpClient {
    private int responseCode;
    protected AsyncHttpResponseHandler responseHandler = new C02731();
    protected String result;

    class C02731 extends AsyncHttpResponseHandler {
        C02731() {
        }

        protected void sendResponseMessage(HttpResponse response) {
            SyncHttpClient.this.responseCode = response.getStatusLine().getStatusCode();
            super.sendResponseMessage(response);
        }

        protected void sendMessage(Message msg) {
            handleMessage(msg);
        }

        public void onSuccess(String content) {
            SyncHttpClient.this.result = content;
        }

        public void onFailure(Throwable error, String content) {
            SyncHttpClient.this.result = SyncHttpClient.this.onRequestFailed(error, content);
        }
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    protected void sendRequest(DefaultHttpClient client, HttpContext httpContext, HttpUriRequest uriRequest, String contentType, AsyncHttpResponseHandler responseHandler, Context context) {
        if (contentType != null) {
            uriRequest.addHeader("Content-Type", contentType);
        }
        new AsyncHttpRequest(client, httpContext, uriRequest, responseHandler).run();
    }

    public String onRequestFailed(Throwable error, String content) {
        return "";
    }

    public void delete(String url, RequestParams queryParams, AsyncHttpResponseHandler responseHandler) {
        delete(url, responseHandler);
    }

    public String get(String url, RequestParams params) {
        get(url, params, this.responseHandler);
        return this.result;
    }

    public String get(String url) {
        get(url, null, this.responseHandler);
        return this.result;
    }

    public String put(String url, RequestParams params) {
        put(url, params, this.responseHandler);
        return this.result;
    }

    public String put(String url) {
        put(url, null, this.responseHandler);
        return this.result;
    }

    public String post(String url, RequestParams params) {
        post(url, params, this.responseHandler);
        return this.result;
    }

    public JSONObject postToJson(String url, RequestParams params) {
        post(url, params, this.responseHandler);
        try {
            return new JSONObject(this.result);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String post(String url) {
        post(url, null, this.responseHandler);
        return this.result;
    }

    public JSONObject postToJson(String url) {
        post(url, null, this.responseHandler);
        try {
            return new JSONObject(this.result);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String delete(String url, RequestParams params) {
        delete(url, params, this.responseHandler);
        return this.result;
    }

    public String delete(String url) {
        delete(url, null, this.responseHandler);
        return this.result;
    }
}
