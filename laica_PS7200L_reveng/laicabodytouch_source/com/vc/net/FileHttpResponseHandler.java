package com.vc.net;

import android.os.Message;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;

public class FileHttpResponseHandler extends AsyncHttpResponseHandler {
    private static final int BUFFER_SIZE = 8192;
    private static final String TAG = "FileHttpResponseHandler";
    private static final String TEMP_SUFFIX = ".download";
    private static final int TIMERSLEEPTIME = 100;
    public static final int TIME_OUT = 30000;
    private File baseDirFile;
    private long downloadSize;
    private File file;
    private boolean interrupt = false;
    private long networkSpeed;
    private RandomAccessFile outputStream;
    private long previousFileSize;
    private long previousTime;
    private File tempFile;
    private Timer timer = new Timer();
    private boolean timerInterrupt = false;
    private long totalSize;
    private long totalTime;
    private String url;

    class C01391 extends TimerTask {
        C01391() {
        }

        public void run() {
            while (!FileHttpResponseHandler.this.timerInterrupt) {
                FileHttpResponseHandler.this.sendProgressMessage(FileHttpResponseHandler.this.totalSize, FileHttpResponseHandler.this.getDownloadSize(), FileHttpResponseHandler.this.networkSpeed);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class ProgressReportingRandomAccessFile extends RandomAccessFile {
        private int progress = 0;

        public ProgressReportingRandomAccessFile(File file, String mode) throws FileNotFoundException {
            super(file, mode);
        }

        public void write(byte[] buffer, int offset, int count) throws IOException {
            super.write(buffer, offset, count);
            this.progress += count;
            FileHttpResponseHandler.this.totalTime = System.currentTimeMillis() - FileHttpResponseHandler.this.previousTime;
            FileHttpResponseHandler.this.downloadSize = ((long) this.progress) + FileHttpResponseHandler.this.previousFileSize;
            if (FileHttpResponseHandler.this.totalTime > 0) {
                FileHttpResponseHandler.this.networkSpeed = (long) (((double) (((long) this.progress) / FileHttpResponseHandler.this.totalTime)) / 1.024d);
            }
        }
    }

    public FileHttpResponseHandler(String url, String rootFile, String fileName) {
        this.url = url;
        this.baseDirFile = new File(rootFile);
        this.file = new File(rootFile, fileName);
        this.tempFile = new File(rootFile, new StringBuilder(String.valueOf(fileName)).append(TEMP_SUFFIX).toString());
        init();
    }

    public FileHttpResponseHandler(String rootFile, String fileName) {
        this.baseDirFile = new File(rootFile);
        this.file = new File(rootFile, fileName);
        this.tempFile = new File(rootFile, new StringBuilder(String.valueOf(fileName)).append(TEMP_SUFFIX).toString());
        init();
    }

    public FileHttpResponseHandler(String filePath) {
        this.file = new File(filePath);
        this.baseDirFile = new File(this.file.getParent());
        this.tempFile = new File(new StringBuilder(String.valueOf(filePath)).append(TEMP_SUFFIX).toString());
        init();
    }

    private void init() {
        if (!this.baseDirFile.exists()) {
            this.baseDirFile.mkdirs();
        }
    }

    private void startTimer() {
        this.timer.schedule(new C01391(), 0, 1000);
    }

    private void stopTimer() {
        this.timerInterrupt = true;
    }

    public File getFile() {
        return this.file;
    }

    public String getUrl() {
        return this.url;
    }

    public boolean isInterrupt() {
        return this.interrupt;
    }

    public void setInterrupt(boolean interrupt) {
        this.interrupt = interrupt;
    }

    public long getDownloadSize() {
        return this.downloadSize;
    }

    public long getTotalSize() {
        return this.totalSize;
    }

    public double getDownloadSpeed() {
        return (double) this.networkSpeed;
    }

    public void setPreviousFileSize(long previousFileSize) {
        this.previousFileSize = previousFileSize;
    }

    public long getTotalTime() {
        return this.totalTime;
    }

    public void onSuccess(byte[] binaryData) {
        onSuccess(new String(binaryData));
    }

    public void onSuccess(int statusCode, byte[] binaryData) {
        onSuccess(binaryData);
    }

    public void onFailure(Throwable error, byte[] binaryData) {
        onFailure(error);
    }

    protected void sendSuccessMessage(int statusCode, byte[] responseBody) {
        sendMessage(obtainMessage(4, new Object[]{Integer.valueOf(statusCode), responseBody}));
    }

    protected void sendFailureMessage(Throwable e, byte[] responseBody) {
        sendMessage(obtainMessage(1, new Object[]{e, responseBody}));
    }

    protected void sendProgressMessage(long totalSize, long currentSize, long speed) {
        sendMessage(obtainMessage(0, new Object[]{Long.valueOf(totalSize), Long.valueOf(currentSize), Long.valueOf(speed)}));
    }

    protected void handleSuccessMessage(int statusCode, byte[] responseBody) {
        onSuccess(statusCode, responseBody);
    }

    protected void handleFailureMessage(Throwable e, byte[] responseBody) {
        onFailure(e, responseBody);
    }

    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case 4:
                Object[] response = msg.obj;
                handleSuccessMessage(((Integer) response[0]).intValue(), (byte[]) response[1]);
                return;
            default:
                super.handleMessage(msg);
                return;
        }
    }

    protected void sendResponseMessage(HttpResponse response) {
        Throwable error = null;
        long result = -1;
        int statusCode = 0;
        try {
            statusCode = response.getStatusLine().getStatusCode();
            long contentLenght = response.getEntity().getContentLength();
            if (contentLenght == -1) {
                contentLenght = (long) response.getEntity().getContent().available();
            }
            this.totalSize = this.previousFileSize + contentLenght;
            if (this.file.exists() && this.totalSize == this.file.length()) {
                throw new FileAlreadyExistException("Output file already exists. Skipping download.");
            }
            if (this.tempFile.exists()) {
                this.previousFileSize = this.tempFile.length();
            }
            this.outputStream = new ProgressReportingRandomAccessFile(this.tempFile, "rw");
            InputStream input = response.getEntity().getContent();
            startTimer();
            int bytesCopied = copy(input, this.outputStream);
            if (this.previousFileSize + ((long) bytesCopied) == this.totalSize || this.totalSize == -1 || this.interrupt) {
                result = (long) bytesCopied;
                stopTimer();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (result != -1 && !this.interrupt && error == null) {
                    this.tempFile.renameTo(this.file);
                    sendSuccessMessage(statusCode, "下载成功！".getBytes());
                    return;
                } else if (error != null) {
                    Log.v(TAG, "Download failed." + error.getMessage());
                    if (error instanceof FileAlreadyExistException) {
                        sendSuccessMessage(statusCode, "下载成功！".getBytes());
                        return;
                    } else {
                        sendFailureMessage(error, null);
                        return;
                    }
                } else {
                    return;
                }
            }
            throw new IOException("Download incomplete: " + bytesCopied + " != " + this.totalSize);
        } catch (Throwable e2) {
            sendFailureMessage(e2, null);
            error = e2;
        } catch (Throwable e22) {
            error = e22;
        } catch (Throwable e222) {
            error = e222;
        } catch (Throwable e2222) {
            error = e2222;
        }
    }

    public int copy(InputStream input, RandomAccessFile out) throws IOException {
        if (input == null || out == null) {
            return -1;
        }
        byte[] buffer = new byte[8192];
        BufferedInputStream in = new BufferedInputStream(input, 8192);
        int count = 0;
        long errorBlockTimePreviousTime = -1;
        out.seek(out.length());
        this.previousTime = System.currentTimeMillis();
        while (!this.interrupt) {
            try {
                int n = in.read(buffer, 0, 8192);
                if (n != -1) {
                    out.write(buffer, 0, n);
                    count += n;
                    if (this.networkSpeed != 0) {
                        errorBlockTimePreviousTime = -1;
                    } else if (errorBlockTimePreviousTime <= 0) {
                        errorBlockTimePreviousTime = System.currentTimeMillis();
                    } else if (System.currentTimeMillis() - errorBlockTimePreviousTime > 30000) {
                        throw new ConnectTimeoutException("connection time out.");
                    }
                }
            } finally {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
        return count;
    }

    public File getTempFile() {
        return this.tempFile;
    }
}
