#!/bin/bash

# Prints the weight in Kg.
# From the moment the script is started or the password is entered (if required)
# you have 15 seconds to weigh yourself. (editable by changing the value of $available_time)

. ./basic_functions.sh

hcidump_file="hcidump_output"
available_time=15

check_bt_status
MAC_address=$(scan_address YoHealth $hcidump_file $available_time)

if [[ $MAC_address = "-1" ]]; then
    echo "get_weight.sh error: MAC address not found"
else
    find_weight $MAC_address $hcidump_file
fi

rm $hcidump_file
